#include "main.h"
#include "readfile.h"
#include "config.h"

#include "charge.h"
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//


void chargeplots(){
  cout<<"CHARGE"<<endl;

  string ectext;
 ectext.append("Charge Distribution: ");
    ectext+=runname;
	 ectext.append("; Electron Peak Charge (pC); Entries");
   
 const char* ecText=ectext.c_str();
 
ec=new TH1F("ec",ecText,ecbin,0,eclast);

 
  string tfulltext;
 tfulltext.append("Time Resolution: ");
    tfulltext+=runname;
	 tfulltext.append(";  SAT_{Pico}-SAT_{PD} (ns); Entries ");
   
 const char* tfullText=tfulltext.c_str();
 
 tfull=new TH1F("time",tfullText,trbin,trfirst,trlast);
 
 for(int i=0; i<entries;i++){

   if(mm_st[i]>tcutstart&&mm_st[i]<tcutfin) {
     ec->Fill(mm_ec[i]);
     tfull->Fill(mm_st[i]-mcp_nt[i]);
   }
   
 }





 //Fit gaus
double parg[3];
 double parerrg[3];
 
 TF1 *G1g = new TF1 ("G1g","gaus",trfirst,trlast);

 
 tfull->Fit(G1g,"R0");
 G1g->GetParameters(&parg[0]);
    
 TF1 *F1g = new TF1 ("F1g","gaus",trfirst,trlast);
 F1g->SetParameters(&parg[0]);
 F1g->SetLineColor(kRed);
        
 double chi2g=G1g->GetChisquare();
 double nofg=G1g->GetNDF();
 
 

 //Fit polya

 double par[3];
 double parerr[3];
 
 TF1 *G1 = new TF1 ("G1","([0]/[1])*((([2]+1)^([2]+1)*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",ecfirst,eclastfit);
 G1->SetParameter(0,1000); // amplitude
 G1->SetParameter(1,30); //Charge
 //  G2->SetParameter(2,10000); //Charge
 
 ec->Fit(G1,"R0");
 ec->Fit(G1,"R0");
 ec->Fit(G1,"R0");
 ec->Fit(G1,"R0");


 
 G1->GetParameters(&par[0]);
parerr[0]=G1->GetParError(0);
parerr[1]=G1->GetParError(1);
 parerr[2]=G1->GetParError(2);

 TF1 *F1 = new TF1 ("F1","([0]/[1])*(((([2]+1)^([2]+1))*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",0.1,400);
 F1->SetParameters(&par[0]);
 F1->SetLineColor(kRed);
        
 double chi2=G1->GetChisquare();
 double nof=G1->GetNDF();

 TCanvas *c1 = new TCanvas("c1","Charge Plots",200,10,700,500);
 ///c1->Divide(2);
 
 

 //c1->cd(1);
 ec->Draw();
c1->SetLogy();
    F1->Draw("same");
   ec->SetStats(0);

 TLegend* legec = new TLegend(0.42,0.8,0.89,0.89);

    string LegecResultTime;
    //LegecResultTime.append("Mean Charge: ");
   
        LegecResultTime+=to_string(par[1]);
	   LegecResultTime.append(" +/- ");
    LegecResultTime+=to_string(parerr[1]);
    LegecResultTime.append(" pC ");

 const char* legecres=LegecResultTime.c_str();
legec->SetTextSize(.03);
 legec->AddEntry((TObject*)0,"Mean Charge:","");

    legec->AddEntry((TObject*)0,legecres,"");
   legec->SetBorderSize(0);

    legec->Draw();

    // c1->cd(2);
    // tfull->Draw();
    //F1g->Draw("same");

  c1->Update();
     
 c1->Modified();


  runname.append("C.pdf");
  
  const char* filename=runname.c_str();
 c1 -> SaveAs(filename);

   runname.erase(runname.end()-5,runname.end());

 // Find Slewing steps
 int i=0;
 int j=1;
 int stepcount=0;
 double xstep[100];
 double binstep;
 
 xstep[0]=0;
 while(binstep<eclast){
   
 double sumintegral=0;
 while(sumintegral<integration){
   binstep=i*binning;
   sumintegral+=(par[0]/par[1])*pow((par[2]+1),(par[2]+1))*pow((binstep/par[1]),par[2])/(TMath::Gamma(par[2]+1))*exp(-(par[2]+1)*binstep/par[1]);
   i++;
   if(binstep>eclast)break;
 }
 xstep[j]=binstep;
 stepcount=j;
 j++;
 cout<<binstep<<" "<<stepcount<<endl;
 }

 
 //create plots for each step

   TH1F* sat[stepcount];
   TH1F* tres[stepcount];

    char histoName[64];
    char histoName2[64];

    for (int i=0; i<stepcount; i++) {
        sprintf(histoName, "SAT%d", i);
	sprintf(histoName2, "Tres%d", i);
        sat[i] = new TH1F(histoName, histoName, 100,100,150);
	tres[i] = new TH1F(histoName2, histoName2,trbin,trfirst,trlast);

	for(int j=0; j<entries;j++){
	  if(mm_ec[j]>xstep[i]&&mm_ec[j]<xstep[i+1]&&mm_st[j]>tcutstart&&mm_st[j]<tcutfin){
	    sat[i]->Fill(mm_st[j]);
	    tres[i]->Fill(mm_st[j]-mcp_nt[j]);
	  }
	  
	}
	
    }


    double entry_sat[stepcount],entry_tres[stepcount],mean_sat[stepcount],mean_tres[stepcount],meane_sat[stepcount],meane_tres[stepcount],sig_sat[stepcount],sig_tres[stepcount],sige_sat[stepcount],sige_tres[stepcount],slewx[stepcount],slewxe[stepcount];

    TF1 *g;
 TCanvas *c2 = new TCanvas("c2","Slew Plots",200,10,700,500);
 c2->Divide(2,stepcount);
 
 for (int i=0; i<stepcount; i++) {
   slewx[i]=(xstep[i+1]+xstep[i])/2;
      slewxe[i]=(xstep[i+1]-xstep[i])/2;

   c2->cd(2*i+1);
   sat[i]->Draw();
   // if(sat[i]->GetEntries()>1)sat[i]->Fit("gaus");
   //g = (TF1*) sat[i]->GetListOfFunctions()->FindObject("gaus");
   // entry_sat[i]= sat[i]->GetEntries();
   mean_sat[i]=  sat[i]->GetMean(1);
   //sig_sat[i]= g->GetParameter(2);
   meane_sat[i]=  sat[i]->GetMeanError(1);
   //sige_sat[i]= g->GetParError(2);

    cout<<"Fitstep "<<i<<endl;
   
   c2->cd(2*i+2);
   tres[i]->Draw();
   tres[i]->Fit("gaus","q");
   g = (TF1*) tres[i]->GetListOfFunctions()->FindObject("gaus");
   entry_tres[i]= tres[i]->GetEntries();
   mean_tres[i]= g->GetParameter(1);
   sig_tres[i]= g->GetParameter(2)*1000;
   meane_tres[i]= g->GetParError(1);
    sige_tres[i]= g->GetParError(2)*1000;

 }

 double weight[stepcount];
 double fullweight=0;

 for(int i=0; i<stepcount;i++){

   binstep=xstep[i];
   weight[i]=0;
   
   for(int j=0;j<(xstep[i+1]-xstep[i])*100;j++){
     binstep=xstep[i]+0.01;
     fullweight+= (par[0]/par[1])*pow((par[2]+1),(par[2]+1))*pow((binstep/par[1]),par[2])/(TMath::Gamma(par[2]+1))*exp(-(par[2]+1)*binstep/par[1]);
     weight[i]+= (par[0]/par[1])*pow((par[2]+1),(par[2]+1))*pow((binstep/par[1]),par[2])/(TMath::Gamma(par[2]+1))*exp(-(par[2]+1)*binstep/par[1]);
   }
 }

for(int i=0; i<stepcount;i++){
 weight[i]= weight[i]/fullweight;
 cout<<weight[i]<<endl;
 }
 
  runname.append("S.pdf");
  
  const char* filename2=runname.c_str();
  // c2 -> SaveAs(filename2);

   runname.erase(runname.end()-5,runname.end());

   //Calc Slewing Time Resolution

 double TimeRes=0;
 double TimeResErr=0;
 
 for(int i=0; i<stepcount-1;i++){
   TimeRes+=pow(weight[i],1)*pow(sig_tres[i],2);
   // cout<<TimeRes<<" "<<pow(weight[i],2)*pow(sig_tres[i],2)<<" "<<weight[i]<<" "<<sig_tres[i]<<endl;
 }

 for(int i=0; i<stepcount;i++){
   for(int j=0; j<stepcount;j++){ 
     if(!i==j)TimeRes+=weight[i]*weight[j]*(pow(sig_tres[i],2)+pow(sig_tres[j],2)+pow(mean_tres[i]-mean_tres[j],2));
   }
 }
 
 
 TimeRes=sqrt(TimeRes);


 for(int i=0; i<stepcount-1;i++){
   TimeResErr+=pow(2*weight[i]*sig_tres[i]*sige_tres[i]/TimeRes,2);
   cout<<sige_tres[i]<<endl;
 }

 for(int i=0; i<stepcount;i++){
   for(int j=0; j<stepcount;j++){ 

   }
 }

 
 TimeResErr=sqrt(TimeResErr);

 cout<<"TIMERES: "<<TimeRes<<" "<<TimeResErr<<endl;
 
 



  TGraphErrors*  slewsat = new TGraphErrors(stepcount-1,slewx,mean_sat,slewxe,meane_sat);
      TGraphErrors*  slewtres = new TGraphErrors(stepcount-1,slewx,sig_tres,slewxe,sige_tres);

  TCanvas *c3 = new TCanvas("c3","Slew Plots",200,10,700,500);
 c3->Divide(2,1);
 
 c3->cd(1);
 slewsat->Draw("AP");
 slewsat->SetTitle("Slewing SAT");
 slewsat->GetXaxis()->SetTitle("Signal Charge (pC)");
 slewsat->GetYaxis()->SetTitle("SAT (ns)");
 TF1 *sl1 = new TF1 ("sl1","[1]/(pow(x,[2]))+[0]",0,slewlast);
 sl1->SetParameter(0,52);
  sl1->SetParameter(1,170);
  sl1->SetParameter(2,1.5);

  // slewsat->Fit(sl1,"R");


 c3->cd(2);
 slewtres->Draw("AP");
 slewtres->SetTitle("Slewing Time Res.");
 slewtres->GetXaxis()->SetTitle("Signal Charge (pC)");
 slewtres->GetYaxis()->SetTitle("Time Resolution (ps)");
 TF1 *sl2 = new TF1 ("sl12","[1]/(pow(x,[2]))+[0]",1,slewlast);
sl1->SetParameter(0,40);
  sl1->SetParameter(1,170);
  sl1->SetParameter(2,1.5);

  // slewtres->Fit(sl2,"R");
 

  runname.append("Sl.pdf");
  
  const char* filename3=runname.c_str();
 c3 -> SaveAs(filename2);

   runname.erase(runname.end()-6,runname.end());



   //Overlap all Gaussians
   double fitpar0[stepcount],fitpar0b[stepcount],fitpar1[stepcount],fitpar2[stepcount];

   for(int i=0; i<stepcount;i++){
     fitpar0[i]=parg[0]*weight[i];
     fitpar0b[i]=tfull->GetEntries()*weight[i];
     fitpar1[i]=mean_tres[i];
     fitpar2[i]=sig_tres[i]/1000;
}



   

   TF1 *Gaus;
   string Gaussum;
   TCanvas *c4 = new TCanvas("c4","AllGaus",200,10,700,500);
   tfull->Draw();
   tfull->SetStats(0);

   for(int i=0; i<stepcount;i++){
        char histoNameG[64];
 
	 sprintf(histoNameG, "Gaus%d", i);
	 Gaus= new TF1 ("Gaus","gaus",trfirst,trlast);
	Gaus->SetParameter(0,parg[0]*weight[i]);
	Gaus->SetParameter(0,(tfull->GetMaximum()+parg[0])/2*weight[i]);

	//	Gaus->SetParameter(0,8575*weight[i]);

	Gaus->SetParameter(1, mean_tres[i]);
	Gaus->SetParameter(2,sig_tres[i]/1000);
	//	cout<<parg[0]*weight[i]<<" "<< mean_tres[i]<<" "<<sig_tres[i]<<endl;
	Gaus->SetLineColor(kBlue);
	Gaus->Draw("same");


	
	Gaussum.append("gaus(");
	Gaussum+=to_string(3*(i));
		Gaussum.append(")+");
		//	cout<<Gaussum<<endl;
   }

      Gaussum.erase(Gaussum.end()-1,Gaussum.end());
      //	cout<<Gaussum<<endl;

 const char* gausfunc=Gaussum.c_str();
 TF1* CombinedGaus= new TF1 ("CombinedGaus",gausfunc,trfirst,trlast);

   for(int i=0; i<stepcount;i++){ 
     CombinedGaus->SetParameter(3*(i),tfull->GetMaximum()*weight[i]);
     CombinedGaus->SetParameter(3*(i)+1,  mean_tres[i]);
     CombinedGaus->SetParameter(3*(i)+2, sig_tres[i]/1000);
   }

   
 CombinedGaus->SetLineColor(kRed);

CombinedGaus->Draw("same");

    TLegend* legGaus = new TLegend(0.55,0.8,0.89,0.89);

    string LegResultTime;
    // LegResultTime.append("Time Res.: ");
        LegResultTime+=to_string(TimeRes);
	   LegResultTime.append(" +/- ");
    LegResultTime+=to_string(TimeResErr);
    LegResultTime.append(" ps ");

 const char* legres=LegResultTime.c_str();
legGaus->SetTextSize(.03);
     legGaus->AddEntry((TObject*)0,"Time Res.:","");

    legGaus->AddEntry((TObject*)0,legres,"");
   legGaus->SetBorderSize(0);

    legGaus->Draw();
 
 runname.append("Gaus.pdf");
 
 const char* filename4=runname.c_str();
 c4 -> SaveAs(filename4);
 
   runname.erase(runname.end()-8,runname.end());
 /*
   for (int i=0;i<5;i++)  parerr[i]=f->GetParError(i);
   
   double con1=par[0];
   double mean1=par[1];
   double sigma1=par[2];
   double con2=par[3];
   double slope2=par[4];
    
    double conerr1=parerr[0];
    double meanerr1=parerr[1];
    double sigmaerr1=parerr[2];
    double conerr2=parerr[3];
    double slopeerr2=parerr[4];
    
    
    
    gStyle->SetOptStat(10);
    //    //textsize
    double txtsize=0.035;
    
    TPaveStats *ps = (TPaveStats*)c1->GetPrimitive("stats");
    ps->SetName("mystats");
    TList *list = ps->GetListOfLines();
    //TPaveStats->Clear();
    //
    //    // Remove the RMS line
    TText *tconst = ps->GetLineWith("Entries");
    list->Remove(tconst);
    
    
    
    ostringstream m;
    m.precision(3);
    m<<fixed;
    
    m << "#bar{Q} " << mean1<<" #pm "<<meanerr1;
    TLatex *mytm = new TLatex(0,0,m.str().c_str());
    mytm ->SetTextFont(42);
    mytm ->SetTextSize(txtsize);
    mytm ->SetTextColor(kBlack);
    list->Add(mytm);
    
    
    ps->AddLine(.0,.5,1.,.5);
    
    
    //
    //    // the following line is needed to avoid that the automatic redrawing of stats
     htemp__1->SetStats(0);

     c1->Update();
    */
 





 TCanvas *cover = new TCanvas("cover","Overview",200,10,700,500);
 cover->Divide(2,2);
 
 

 cover->cd(1);
 
 ec->Draw();
cover->cd(1)->SetLogy();
    F1->Draw("same");
   ec->SetStats(0);
  legec->Draw();

   cover->cd(2);
    tfull->Draw();
   tfull->SetStats(0);
   CombinedGaus->Draw("same");
    legGaus->Draw();

    cover->cd(3);
 slewsat->Draw("AP");
 slewsat->SetTitle("Slewing SAT");
 slewsat->GetXaxis()->SetTitle("Signal Charge (pC)");
 slewsat->GetYaxis()->SetTitle("SAT (ns)");

 cover->cd(4);
 slewtres->Draw("AP");
 slewtres->SetTitle("Slewing Time Res.");
 slewtres->GetXaxis()->SetTitle("Signal Charge (pC)");
 slewtres->GetYaxis()->SetTitle("Time Resolution (ps)");
  c1->Update();
     
 c1->Modified();


  
 cover -> SaveAs("overview.pdf");

  
 runname.append("F.pdf");
 
 const char* filename5=runname.c_str();
 cover -> SaveAs(filename5);
 
   runname.erase(runname.end()-5,runname.end());
   
   

 
  /*
gmy=new TH1F("gmy","gmy",sgmy,fgmy,lgmy);

ec=new TH1F("ec","ec",sec,fec,lec);
ac=new TH1F("ac","ac",sac,fac,lac);
 sc=new TH1F("sc","sc",ssc,fsc,lsc);

 aec=new TH2F("aec","aec",sec,fec,lec,sgmy,fgmy,lgmy);
aac=new TH2F("aac","aac",sac,fac,lac,sgmy,fgmy,lgmy);
asc=new TH2F("asc","asc",ssc,fsc,lsc,sgmy,fgmy,lgmy);

 for(int i=0; i<entries;i++){
   
   aec->Fill(mm_ec[i],mm_gmy[i]);
   aac->Fill(mm_ac[i],mm_gmy[i]);
   asc->Fill(mm_sc[i],mm_gmy[i]);

   if(mm_gmy[i]>ampcut){
     gmy->Fill(mm_gmy[i]);
     
     ec->Fill(mm_ec[i]);
     ac->Fill(mm_ac[i]);
     sc->Fill(mm_sc[i]);
     
   }
 }

 
 gmy->SetTitle("Peak Amplitude");
 gmy->GetXaxis()->SetTitle("Voltage (V)");
 gmy->GetYaxis()->SetTitle("Entries");

 ec->SetTitle("Electron Charge");
 ec->GetXaxis()->SetTitle("Charge (pC)");
 ec->GetYaxis()->SetTitle("Entries");
 
 ac->SetTitle("Full Charge");
 ac->GetXaxis()->SetTitle("Charge (pC)");
 ac->GetYaxis()->SetTitle("Entries");

 sc->SetTitle("Sigmoid Charge");
 sc->GetXaxis()->SetTitle("Charge (pC)");
 sc->GetYaxis()->SetTitle("Entries");

 aec->SetTitle("Amplitude vs. Electron Charge");
 aec->GetXaxis()->SetTitle("Charge (pC)");
 aec->GetYaxis()->SetTitle("Amplitude (V)");
 
 aac->SetTitle("Amplitude vs. Full Charge");
 aac->GetXaxis()->SetTitle("Charge (pC)");
 aac->GetYaxis()->SetTitle("Amplitude (V)");

 asc->SetTitle("Amplitude vs. Sigmoid Charge");
 asc->GetXaxis()->SetTitle("Charge (pC)");
 asc->GetYaxis()->SetTitle("Amplitude (V)");

 TCanvas *c1 = new TCanvas("c1","Charge Plots",200,10,700,500);
 c1->Divide(3,3);
 
 c1->cd(2);
 gmy->Draw();
 
 c1->cd(4);
 ec->Draw();
 
 c1->cd(5);
 ac->Draw();
 
 c1->cd(6);
 sc->Draw();
 
 c1->cd(7);
 aec->Draw();
 
 c1->cd(8);
 aac->Draw();
 
 c1->cd(9);
 asc->Draw();

 c1->Update();
     
 c1->Modified();
     
 c1 -> SaveAs("charge.pdf");

}

void chargeplotspolya(){
  cout<<"CHARGEPOLYA"<<endl;

  gmyp=new TH1F("gmyp","gmyp",sgmy,fgmy,lgmy);

  ecp=new TH1F("ecp","ecp",sec,fec,lec);
  acp=new TH1F("acp","acp",sac,fac,lac);
  scp=new TH1F("scp","scp",ssc,fsc,lsc);


  for(int i=0; i<entries;i++){
   
   if(mm_gmy[i]>ampcut){
     if(R_mm_gmy[i]<radcut)gmyp->Fill(mm_gmy[i]);
     
     if(R_mm_ec[i]<radcut) ecp->Fill(mm_ec[i]);
     if(R_mm_ac[i]<radcut) acp->Fill(mm_ac[i]);
     if(R_mm_sc[i]<radcut) scp->Fill(mm_sc[i]);  
   }
 }


  
  TF1 *Pgmy = new TF1 ("Pgmy","([0]/[1])*((([2]+1)^([2]+1)*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",pcgmy,lgmy);
  Pgmy->SetParameter(1,gmyp->GetMean()); //Charge
  Pgmy->SetParameter(0,2*gmyp->GetEntries()/gmyp->GetMean());
  gmyp->Fit(Pgmy,"qR");
  gmyp->Fit(Pgmy,"qR");
  gmyp->Fit(Pgmy,"qR");
  gmyp->Fit(Pgmy,"qR");
  
  TF1 *Pec = new TF1 ("Pec","([0]/[1])*((([2]+1)^([2]+1)*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",pcec,lec);
  Pec->SetParameter(1,ecp->GetMean()); //Charge
  Pec->SetParameter(0,2*ecp->GetEntries()/ecp->GetMean());
  ecp->Fit(Pec,"qR");
  ecp->Fit(Pec,"qR");
  ecp->Fit(Pec,"qR");
  ecp->Fit(Pec,"qR");
  
  TF1 *Pac = new TF1 ("Pac","([0]/[1])*((([2]+1)^([2]+1)*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",pcac,lac);
  Pac->SetParameter(1,acp->GetMean()); //Charge
  Pac->SetParameter(0,2*acp->GetEntries()/acp->GetMean());
  acp->Fit(Pac,"qR");
  acp->Fit(Pac,"qR");
  acp->Fit(Pac,"qR");
  acp->Fit(Pac,"qR");

  TF1 *Psc = new TF1 ("Psc","([0]/[1])*((([2]+1)^([2]+1)*(x/[1])^[2])/(TMath::Gamma([2]+1)))*exp(-([2]+1)*x/[1])",pcsc,lsc);
  Psc->SetParameter(1,scp->GetMean()); //Charge
  Psc->SetParameter(0,2*scp->GetEntries()/scp->GetMean());
  scp->Fit(Psc,"qR");
  scp->Fit(Psc,"qR");
  scp->Fit(Psc,"qR");
  scp->Fit(Psc,"qR");
  
 gmyp->SetTitle("Peak Amplitude");
 gmyp->GetXaxis()->SetTitle("Voltage (V)");
 gmyp->GetYaxis()->SetTitle("Entries");

 ecp->SetTitle("Electron Charge");
 ecp->GetXaxis()->SetTitle("Charge (pC)");
 ecp->GetYaxis()->SetTitle("Entries");
 
 acp->SetTitle("Full Charge");
 acp->GetXaxis()->SetTitle("Charge (pC)");
 acp->GetYaxis()->SetTitle("Entries");

 scp->SetTitle("Sigmoid Charge");
 scp->GetXaxis()->SetTitle("Charge (pC)");
 scp->GetYaxis()->SetTitle("Entries");

 
 TCanvas *c2 = new TCanvas("c2","Charge Polya Plots",200,10,700,500);
 gStyle->SetOptFit(1);
 c2->Divide(3,3);
 
 c2->cd(2);
 gmyp->Draw();
 
 c2->cd(4);
 ecp->Draw();
 
 c2->cd(5);
 acp->Draw();
 
 c2->cd(6);
 scp->Draw();
 
 c2->cd(7);
 aec->Draw();
 
 c2->cd(8);
 aac->Draw();
 
 c2->cd(9);
 asc->Draw();

 c2->Update();
     
 c2->Modified();
     
 c2 -> SaveAs("charge_polya.pdf");

  */  
}
