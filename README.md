# PicosecAnalysis

==============================================================
		README FILE
==============================================================

This analysis code was developped from CEA/IRFU//DEDIP Group 
for the TestBeam data analysis of PICOSEC Micromegas Detector

-------------------------------------------------------------

The main code is contained in the code/ folder, 
while on the data/ you will find, organised by TestBeam period, 
the data used and produced in the analysis procedure. 

-------------------------------------------------------------

==============================================================
		HOW TO RUN 
==============================================================


----In the /paht_to/code/directory you will find the header MyFunctions.h. 

------MyFunctions.h-----------------

First you need to define the directories of the raw-binary data, tracking data
and all the other directories you may need for your analysis. 
In this version we use the following structure:  

const char *CODEDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/code";
const char *BASEDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4";
const char *WORKDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4/wdir";
const char *PLOTDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4/plots";
const char *DATADIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4/dataTrees";
const char *TRACKDIRNAME="/sw/akallits/PicoAnalysis/TestBeams/2022_October_h4/tracking/";
const char *OUTDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4/processedTrees";
const char *PARAMDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/data/2022_October_h4/processedTrees/ParameterTrees";
const char *DATA_PATH_NAME="/sw/akallits/PicoAnalysis/TestBeams/2022_October_h4";


All those directories will be created by simply running: 

        % root -l
	% .x ConstructDirTree.C


----Having your environment set up, you need to move to the Bin2Tree/ folder 

- bin2tree.cxx is tha main code which together with the makefile
will read the LEcroy software and binary files 

	 % make 
	 % ./bin2tree runNo poolNo 

!!!!!!!!!!!!!!!!!!!!!!!!ATTENTION for GDD scope you use poolNo == 0 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

This will result to a root file containing all trc waveforms in a tree format. 
You can find this in the directory 

/path_to/data/2022_October_h4/dataTrees/

with the name RUNNO-POOLNO_TESTBEAMraw_tree.root 
this contatins all the waveform points for each channel and all the oscilloscope information during data taking
i.e, gain, offset, npoints, dt, etc. 

--------------------------------------------------------------


----For the next step you will start to process the tree with the RAW data.

First you have to create a logbook txt file (see the example) with RUN parameter information, 
named OsciloscopeSetup.txt in the data/TESTBEAMPERIODDATA/ directory 
 
In the code/ directory, you will find the codes: makeTree.cxx and MakeTreefromRawTreePicosec.C

In the MakeTreefromRawTreePicosec.C you read both the RUNNO-POOLNO_TESTBEAMraw_tree.root raw dataTree file
and the OsciloscopeSetup information of the individual run.

Those txt run informations will then be stored to the output root file of the data.

At this point the tracking information is added and the SRS decoding is processed. 

	% root -l  
	% .x MakeTreefromRawTreePicosec.C++(RUNNO,POOLNO)

This will result to the outputfile /path_to/data/2022_October_h4/processedTrees/Run224-Pool2_TESTBEAM_tree.root
Which contains to trees : 
	OsciloscopeSetup with all the information read from the txt file for the run parameters
	RawDataTree with all waveforms per channel, the tracking information and the SRS number from decoding

------------------------------------------------------------

----Last step is to follow the offline analysis procedure based on the CFD method 

This will use the AnalyseTreePicosec.C and.....to be continued

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.cern.ch/akallits/picosecanalysis.git
git branch -M master
git push -uf origin master
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.cern.ch/akallits/picosecanalysis/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
