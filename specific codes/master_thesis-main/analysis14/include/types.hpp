#ifndef TYPES_HPP
#define TYPES_HPP
#include <stdint.h>

// Unsigned
using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

// Signed
using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;

// Char
using c8 = char;

// Floating point
using f32 = float;
using f64 = double;

#endif
