#ifndef HEADER_H_
#define HEADER_H_

#include "./types.hpp"
#include <cstdio>
#include <inttypes.h>

struct __attribute__((packed)) wavedesc {
  u8 descriptor_name[16];
  u8 template_name[16];
  i8 comm_type1;
  i8 comm_type2;
  i8 comm_order1;
  i8 comm_order2;
  i32 wave_descriptor;
  i32 user_text;
  i32 res_desc1;
  i32 trigtime_array;
  i32 ris_time_array;
  i32 res_array1;
  i32 wave_array_1;
  i32 wave_array_2;
  i32 res_array2;
  i32 res_array3;
  u8 instrument_name[16];
  i32 instrument_number;
  u8 trace_label[16];
  i16 reserved1;
  i16 reserved2;
  i32 wave_array_count;
  i32 pnts_per_screen;
  i32 first_valid_pnt;
  i32 last_valid_pnt;
  i32 first_point;
  i32 sparsing_factor;
  i32 segment_index;
  i32 subarray_count;
  i32 sweeps_per_acq;
  i16 points_per_pair;
  i16 pair_offset;
  f32 vertical_gain;
  f32 vertical_offset;
  f32 max_value;
  f32 min_value;
  i16 nominal_bits;
  i16 nom_subarray_count;
  f32 horizontal_interval;
  f64 horizontal_offset;
  f64 pixel_offset;
  u8 vertunit[48];
  u8 horunit[48];
  f32 horiz_uncertainty;
  f64 trigger_time_s;
  i8 trigger_time_m;
  i8 trigger_time_h;
  i8 trigger_time_D;
  i8 trigger_time_M;
  i16 trigger_time_Y;
  i16 trigger_time_r;
  f32 acq_duration;
  i16 record_type;
  i16 processing_done;
  i16 reserved5;
  i16 ris_sweeps;
  i16 timebase;
  i16 vert_coupling;
  f32 probe_att;
  i16 fixed_vert_gain;
  i16 bandwidth_limit;
  f32 vertical_vernier;
  f32 acq_vert_offset;
  i16 wave_source;
};

static void dump_header(FILE* stream, wavedesc* WaveDesc) {
  fprintf(stream, "Desciptor           = %.*s\n", 16,
          WaveDesc->descriptor_name);
  fprintf(stream, "Template            = %.*s\n", 16, WaveDesc->template_name);
  fprintf(stream, "commtype1           = %" PRIu8 "\n", WaveDesc->comm_type1);
  fprintf(stream, "commtype2           = %" PRIu8 "\n", WaveDesc->comm_type2);
  fprintf(stream, "commorder1          = %" PRIu8 "\n", WaveDesc->comm_order1);
  fprintf(stream, "commorder2          = %" PRIu8 "\n", WaveDesc->comm_order2);
  fprintf(stream, "wave_desciptor      = %d\n", WaveDesc->wave_descriptor);
  fprintf(stream, "user_text           = %d\n", WaveDesc->user_text);
  fprintf(stream, "res_desc1           = %d\n", WaveDesc->res_desc1);
  fprintf(stream, "trigtime_array      = %d\n", WaveDesc->trigtime_array);
  fprintf(stream, "ris_time_array      = %d\n", WaveDesc->ris_time_array);
  fprintf(stream, "res_array1          = %d\n", WaveDesc->res_array1);
  fprintf(stream, "wave_array_1        = %d\n", WaveDesc->wave_array_1);
  fprintf(stream, "wave_array_2        = %d\n", WaveDesc->wave_array_2);
  fprintf(stream, "res_array2          = %d\n", WaveDesc->res_array2);
  fprintf(stream, "res_array3          = %d\n", WaveDesc->res_array3);
  fprintf(stream, "instrument_name     = %.*s\n", 16,
          (c8*)WaveDesc->instrument_name);
  fprintf(stream, "instrument_number   = %d\n", WaveDesc->instrument_number);
  fprintf(stream, "trace_label         = %.*s\n", 16,
          (c8*)WaveDesc->trace_label);
  fprintf(stream, "reserved1           = %" PRIu16 "\n", WaveDesc->reserved1);
  fprintf(stream, "reserved2           = %" PRIu16 "\n", WaveDesc->reserved2);
  fprintf(stream, "wave_array_count    = %d\n", WaveDesc->wave_array_count);
  fprintf(stream, "pnts_per_screen     = %d\n", WaveDesc->pnts_per_screen);
  fprintf(stream, "first_valid_pnt     = %d\n", WaveDesc->first_valid_pnt);
  fprintf(stream, "last_valid_pnt      = %d\n", WaveDesc->last_valid_pnt);
  fprintf(stream, "first_point         = %d\n", WaveDesc->first_point);
  fprintf(stream, "sparsing_factor     = %d\n", WaveDesc->sparsing_factor);
  fprintf(stream, "segment_index       = %d\n", WaveDesc->segment_index);
  fprintf(stream, "subarray_count      = %d\n", WaveDesc->subarray_count);
  fprintf(stream, "sweeps_per_acq      = %d\n", WaveDesc->sweeps_per_acq);
  fprintf(stream, "points_per_pair     = %" PRIu16 "\n",
          WaveDesc->points_per_pair);
  fprintf(stream, "pair_offset         = %" PRIu16 "\n", WaveDesc->pair_offset);
  fprintf(stream, "vertical_gain       = %f\n", WaveDesc->vertical_gain);
  fprintf(stream, "vertical_offset     = %f\n", WaveDesc->vertical_offset);
  fprintf(stream, "max_value           = %f\n", WaveDesc->max_value);
  fprintf(stream, "min_value           = %f\n", WaveDesc->min_value);
  fprintf(stream, "nominal_bits        = %" PRIu16 "\n",
          WaveDesc->nominal_bits);
  fprintf(stream, "nom_subarray_count  = %" PRIu16 "\n",
          WaveDesc->nom_subarray_count);
  fprintf(stream, "horizontal_interval = %e\n", WaveDesc->horizontal_interval);
  fprintf(stream, "horizontal_offset   = %e\n", WaveDesc->horizontal_offset);
  fprintf(stream, "pixel_offset        = %e\n", WaveDesc->pixel_offset);
  fprintf(stream, "vertunit            = %.*s\n", 1, WaveDesc->vertunit);
  fprintf(stream, "horunit             = %.*s\n", 1, WaveDesc->horunit);
  fprintf(stream, "horiz_uncertainty   = %f\n", WaveDesc->horiz_uncertainty);
  fprintf(stream, "trigger_time_s      = %f\n", WaveDesc->trigger_time_s);
  fprintf(stream, "trigger_time_m      = %" PRIu8 "\n",
          WaveDesc->trigger_time_m);
  fprintf(stream, "trigger_time_h      = %" PRIu8 "\n",
          WaveDesc->trigger_time_h);
  fprintf(stream, "trigger_time_D      = %" PRIu8 "\n",
          WaveDesc->trigger_time_D);
  fprintf(stream, "trigger_time_M      = %" PRIu8 "\n",
          WaveDesc->trigger_time_M);
  fprintf(stream, "trigger_time_Y      = %" PRIu16 "\n",
          WaveDesc->trigger_time_Y);
  fprintf(stream, "trigger_time_r      = %" PRIu16 "\n",
          WaveDesc->trigger_time_r);
  fprintf(stream, "acq_duration        = %f\n", WaveDesc->acq_duration);
  fprintf(stream, "record_type         = %" PRIu16 "\n", WaveDesc->record_type);
  fprintf(stream, "processing_done     = %" PRIu16 "\n",
          WaveDesc->processing_done);
  fprintf(stream, "reserved5           = %" PRIu16 "\n", WaveDesc->reserved5);
  fprintf(stream, "ris_sweeps          = %" PRIu16 "\n", WaveDesc->ris_sweeps);
  fprintf(stream, "timebase            = %" PRIu16 "\n", WaveDesc->timebase);
  fprintf(stream, "vert_coupling       = %" PRIu16 "\n",
          WaveDesc->vert_coupling);
  fprintf(stream, "probe_att           = %f\n", WaveDesc->probe_att);
  fprintf(stream, "fixed_vert_gain     = %" PRIu16 "\n",
          WaveDesc->fixed_vert_gain);
  fprintf(stream, "bandwidth_limit     = %" PRIu16 "\n",
          WaveDesc->bandwidth_limit);
  fprintf(stream, "vertical_vernier    = %f\n", WaveDesc->vertical_vernier);
  fprintf(stream, "acq_vert_offset     = %1.10f\n", WaveDesc->acq_vert_offset);
  fprintf(stream, "wave_source         = %" PRIu16 "\n", WaveDesc->wave_source);
}
#endif // HEADER_H_
