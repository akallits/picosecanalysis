#ifndef CHANNELS_H_
#define CHANNELS_H_

#include "./header.hpp"
#include "./types.hpp"

#include <string>
#include <vector>

struct channel {
  wavedesc Header;
  std::vector<f64> TriggerTime;
  std::vector<f64> TriggerOffset;
  std::vector<i32> Wave;

  std::vector<f64> WaveformX;
  std::vector<f64> WaveformY;

  std::string DirPath;
  std::string
      FilenameHeader; // i.e. 'C2Seq100traces' of 'C2Seq100traces00000.trc'
  i32 EventMumber;
  i32 FileCounter;
  i32 MaxNumberOfFiles;
  i32 NumberOfEventsInFile;
};

#endif // CHANNELS_H_
