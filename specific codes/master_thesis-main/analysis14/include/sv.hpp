#ifndef SV_H_
#define SV_H_

#include "types.hpp"
#include <string>
#include <vector>

#include <ostream>
#include <iostream>

struct string_view {
  const c8* data;
  u64 size;
};

#define SV_FMT "%.*s"

void chop_left(string_view& sv, u64 n);
void chop_by_delim(string_view& sv, c8 delim);
auto find_char(const string_view& sv, c8 delim) -> u64;
auto split_by(const string_view& str, c8 delim) -> std::vector<string_view>;
auto read_file_as_string_view(const c8* filename) -> string_view;
auto read_file_as_string(const c8* filename) -> std::string;

#endif // SV_H_

#ifdef SV_IMPLEMENTATION
void chop_left(string_view& sv, u64 n) {
  if (n > sv.size) {
    n = sv.size;
  }

  sv.data += n;
  sv.size -= n;
}

void chop_by_delim(string_view& sv, c8 delim) {
  while (0 < sv.size && sv.data[0] != delim) {
    chop_left(sv, 1);
  }
  // another one to clear the delim left
  chop_left(sv, 1);
}

auto find_char(const string_view& sv, c8 delim) -> u64 {
  u64 res = 0;
  while (res < sv.size && sv.data[res] != delim) {
    ++res;
  }
  return res;
}
// Split a string_view accorind to a delimiter
auto split_by(const string_view& str, c8 delim) -> std::vector<string_view> {
  std::vector<string_view> vec;
  // preallocate memory for 1024 lines
  vec.reserve(1024);

  // create a string_view from the input sv
  string_view temp{str.data, str.size};
  // while the size of the sv > 0
  // chop a until we find a newline
  // add push the result a vector
  while (temp.size > 0) {
    auto end = find_char(temp, delim);
    vec.emplace_back(string_view{temp.data, end});
    chop_by_delim(temp, delim);
  }
  return vec;
}

// Split a string accorind to a delimiter
auto split_by(const std::string& str, c8 delim) -> std::vector<string_view> {
  std::vector<string_view> vec;
  // preallocate memory for 1024 lines
  vec.reserve(1024);

  // create a string_view from the input string
  string_view temp{str.data(), str.size()};
  // while the size of the sv > 0
  // chop a until we find a newline
  // add push the result a vector
  while (temp.size > 0) {
    auto end = find_char(temp, delim);
    vec.emplace_back(string_view{temp.data, end});
    chop_by_delim(temp, delim);
  }
  return vec;
}

auto read_file_as_string_view(const c8* filename) -> string_view {
  // open file in read binary mode (rb)
  FILE* f = fopen(filename, "rb");
  if (f == nullptr) {
    return {};
  }

  // set the cursor at the end of the file
  i32 err = fseek(f, 0, SEEK_END);
  if (err < 0) {
    return {};
  }

  // find the size ot the file
  i64 size = ftell(f);
  if (size < 0) {
    return {};
  }

  // set the cursor at the start of the file again
  err = fseek(f, 0, SEEK_SET);
  if (err < 0) {
    return {};
  }

  // allocate memory to read in the file
  auto* data = static_cast<c8*>(malloc(size));
  if (data == nullptr) {
    return {};
  }

  // read the file content to the data
  // and return the amount of data read
  std::size_t read_size = fread(data, 1, size, f);
  if (read_size != static_cast<std::size_t>(size) && ferror(f) != 0) {
    return {};
  }

  // close the file
  fclose(f);
  // create a string_view and return it
  string_view sv;
  sv.data = data;
  sv.size = static_cast<u64>(size);

  return sv;
}
// This function reads a whole file in memory and return a string with it
auto read_file_as_string(const c8* filename) -> std::string {
  // open file in read binary mode (rb)
  FILE* f = fopen(filename, "rb");
  if (f == nullptr) {
    return {};
  }

  // set the cursor at the end of the file
  i32 err = fseek(f, 0, SEEK_END);
  if (err < 0) {
    return {};
  }

  // find the size ot the file
  i64 size = ftell(f);
  if (size < 0) {
    return {};
  }

  // set the cursor at the start of the file again
  err = fseek(f, 0, SEEK_SET);
  if (err < 0) {
    return {};
  }

  // allocate memory to read in the file
  auto* data = malloc(size);
  if (data == nullptr) {
    return {};
  }

  // read the file content to the data
  // and return the amount of data read
  std::size_t read_size = fread(data, 1, size, f);
  if (read_size != static_cast<std::size_t>(size) && ferror(f) != 0) {
    return {};
  }

  // close the file
  fclose(f);
  // create a string object and return it
  auto ret =
      std::string{static_cast<const c8*>(data), static_cast<std::size_t>(size)};
  free(data);
  return ret;
}
#endif
