#include "include/channels.hpp"
#include "include/header.hpp"
#include "include/types.hpp"

#include "./parsing.cpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>

//root includes
#include <TRint.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include "TH1.h"
#include "TF1.h"
#include <TSystem.h>

// for math functions
#include <TMath.h>
#include <math.h>

using namespace std;

auto main(i32 Argc, c8* Argv[]) -> i32 {
  if (Argc != 5) {
    fprintf(stderr, "Usage:");
    fprintf(stderr, "./main C1--Trace--00000.trc C2--Trace--00000.trc "
                    "C3--Trace--00000.trc C4--Trace--00000.trc");
    fprintf(stderr, "No filenames given!");
    exit(1);
  }
  
  TRint App("app", nullptr, nullptr);
  auto* Canvas = new TCanvas("c1", "", 1366, 768);
  
    
  constexpr u64 nChannels = 4;
  std::vector<channel> Channels(nChannels);
   for (u64 i = 0; i < nChannels; ++i) {
      Channels[i] = create_channel(Argv[i + 1]);
    }
   i32 nEvents   = Channels[1].Header.subarray_count;
   std::cout << nEvents <<std::endl;
   std::vector<std::vector<TGraph*>> Graphs(4, std::vector<TGraph*>(nEvents));
  
   const f64 threshold=0.006; //Volts  = 10 mV
   f64  ch_max=0;
   
 
 //main loop
 for (i32 iEvent = 770; iEvent < 780; ++iEvent) {
        fprintf(stdout,"####### Running event: %d ######## \n", iEvent+1);
       f64 x_pico, x_ref;
   for (i32 iChannel = 0; iChannel < 2; ++iChannel) {
    auto XY = get_next_event(Channels[iChannel], iEvent);
    

    // get mean and rms of noise and eliminate baseline noise of vector y values   
    pdf noise = eliminate_noise(XY.second);
    
    //store in a graph
    Graphs[iChannel][iEvent] =
          new TGraph(XY.first.size(), XY.first.data(), XY.second.data());

      Graphs[iChannel][iEvent]->SetName(Form("gg_%d", iChannel));
      Graphs[iChannel][iEvent]->SetUniqueID(0);
      Graphs[iChannel][iEvent]->SetTitle(Form("Event_%d", iEvent+1));

      Graphs[iChannel][iEvent]->SetMarkerColor(1);
      Graphs[iChannel][iEvent]->SetMarkerStyle(20);
      Graphs[iChannel][iEvent]->SetMarkerSize(0.6);
      
    //find position and value of maximum
    u64 i_max = find_max_pos(XY.second);
    ch_max = XY.second.at(i_max);
    fprintf(stdout,"ch_max = %f V \n", ch_max);
    
    // accept the pulse?
    if (ch_max < threshold|| i_max<20){
       fprintf(stdout,"ch_max = %f V < threshold = %f \n", ch_max, threshold);
       ch_max=0;
       break; // go to next event
    }
    
    // to find the point where pulse starts-----> according to RMS of noise
    u64 i_start = find_pulse_start_pos(XY.second, i_max, noise.RMS);
    
    // find position where pulse ends---> according to RMS of noise
      u64 i_end = distance(XY.second.begin(), min_element(XY.second.begin()+ i_max, XY.second.begin()+ i_max +4*(i_max-i_start) ) );
      
      if(abs(XY.second.at(i_end))>0.45*ch_max){
         cout << "FAKE PULSE ---- DUMBED ---- 1 \n" << endl;
         ch_max = 0;
         break;
      }
      
      for (u64 i = 0; i < 4*(i_max-i_start) ; ++i) {
        if(XY.second.at(i_max + i) < noise.RMS ){
           i_end = i_max+i;
           break;
          }
      }
      fprintf(stdout,"start= %lu  and end = %lu\n", i_max, i_end);
      //----------------------------------------------------
      // now that we have the actual borders of pulse, we check if it is fake, in a range of 2 pulses left and right
      u64 dumb_range = 2*(i_end - i_start); 
      u64 i_dumb;
      f64 dumb_array[4];
      //left side min and max
      i_dumb=distance(XY.second.begin(), min_element(XY.second.begin()+ i_start-dumb_range, XY.second.begin()+ i_start ) );
      dumb_array[0] = abs(XY.second.at(i_dumb));
      
      i_dumb=distance(XY.second.begin(), max_element(XY.second.begin()+ i_start-dumb_range, XY.second.begin()+ i_start ) );
      dumb_array[1] = abs(XY.second.at(i_dumb));
      
      //right side min and max
      i_dumb=distance(XY.second.begin(), min_element(XY.second.begin()+ i_end, XY.second.begin()+ i_end+dumb_range ) );
      dumb_array[2] = abs(XY.second.at(i_dumb));
      
      i_dumb=distance(XY.second.begin(), max_element(XY.second.begin()+ i_end, XY.second.begin()+ i_end + dumb_range ) );
      dumb_array[3] = abs(XY.second.at(i_dumb));
      
      for (u64 i = 0; i < 4 ; ++i) {
         if(dumb_array[i]>0.45*ch_max){
         cout << "FAKE PULSE ---- DUMBED ---- 2 \n" << endl;
         ch_max = 0;
         break;
      }
      }
      if(!ch_max) break;
      //-------------------------------------------------
    fprintf(stdout,"start= %lu  and end = %lu\n", i_max, i_end);
    
    // constant fraction = 20% = 0.2
    const f32 cfd = 0.2;  

    if(iChannel){
    
    // Define the parameter array for fitting function-s
       Double_t par[8];
       //fit range
       u64 range =200;
       if(i_start< range) range = i_start-1;
       if(i_end+range >= XY.first.size()) range = XY.first.size()-i_end-1;  
    
    // ----------------------------------------------------------------------------------
    // fit only leading edge-----> sigmoid ----> for SAT 
    // parametrization follows from: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5774301/
    //------------------------------------------------------------------------------------
    
    // to find approximately max steepness of curve at x_mid(need to help initialize the fit)
    //u64 i_dumb;                     //not important now (ignore) ; helps to pass information for Xpoint_of_linear_interpolation function.
    f64 steepness_left = slope_at_x(XY.first, XY.second, i_start, i_max, 0.5*ch_max, i_dumb)*4/(XY.second.at(i_max)-XY.second.at(i_start));
    
    //to find approximately x where we are at 0.5 of Vmax (aka 0.5*ch_max) (need to help initialize the fit)      
      f64 x_mid_left = Xpoint_of_linear_interpolation( XY.first, XY.second, i_start, i_max, 0.5*ch_max);

              
      
      TF1 *leading_edge_fit = new TF1("leading_edge_fit",leading_edge_function, XY.first.at(i_start-range) , XY.first.at(i_max+range),4);   // IMPORTANT: NUM OF PARS
      leading_edge_fit -> SetParameters(XY.second.at(i_max)-XY.second.at(i_start), x_mid_left, steepness_left, XY.second.at(i_start) );
    
      Graphs[iChannel][iEvent]->Fit("leading_edge_fit","","",XY.first.at(i_start),XY.first.at(i_max+3)); 
      leading_edge_fit->GetParameters(&par[0]);
            
      x_pico = par[1] - 1/par[2]*log(par[0]/(cfd*ch_max-par[3])-1);
      
   
      Canvas ->Clear();
      Canvas->Divide(1, 2); 
           
      Canvas->cd(1);
      Graphs[iChannel][iEvent]->Draw();
      leading_edge_fit->Draw("SAME");
      

        //App.Run(kTRUE);

      
      /*-----------------------------------------------------------------------------------
      fit whole pulse----> double sigmoid ----> for charge/integral
      parametrization follows from: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5774301/
      (eq 14)
      -------------------------------------------------------------------------------------*/
      
      
      // mid point and max slope at x_mid
       u64 i1 =0, i2=0 ;
      for (u64 i = i_max; i< i_end ; ++i) {
          if(XY.second.at(i) < 0.5*ch_max){
           i1 = i;
           i2=i-1;
           break;
          }
      } 
      f64 x1 = XY.first.at(i1);
      f64 x2 = XY.first.at(i2);
      f64 y1 = XY.second.at(i1);
      f64 y2 = XY.second.at(i2);
    
      f64 steepness_right = (y2-y1)/(x2-x1)*4/(XY.second.at(i_max)-XY.second.at(i_end));
      f64 x_mid_right = x1+ (0.5*ch_max-y1)*(x2-x1)/(y2-y1);
      
      //estimate the rest of parameters
      par[4] = XY.second.at(i_max)-XY.second.at(i_end);
      par[5] = x_mid_right;
      par[6] = steepness_right;
      par[7] = XY.second.at(i_end);
      fprintf(stdout,"start= %lu  and range = %lu\n", i_start, range);
      TF1 *impulse_fit = new TF1("impulse_fit",impulse_function, XY.first.at(i_start-range) , XY.first.at(i_end+range),8); //IMPORTANT NUMBER OF PRMTRS
      impulse_fit-> SetParameters (par);
         
      Graphs[iChannel][iEvent]->Fit("impulse_fit","RS");
      //Graphs[iChannel][iEvent]->Fit("impulse_fit","","",XY.first.at(i_start),XY.first.at(i_end));
      //impulse_fit->GetParameters(&par[0]);
      
      Canvas->cd(2);
      Graphs[iChannel][iEvent]->Draw();
      impulse_fit->Draw("SAME");
      
      // limits for plots
      Graphs[iChannel][iEvent]->GetXaxis()->SetLimits(XY.first.at(i_start-range), XY.first.at(i_end+range)); 
      //Canvas->Modified();
      // TO REMOVE ONLY THE FIT impulse_fit FUNCTION
      Graphs[iChannel][iEvent]->GetListOfFunctions()->Remove(Graphs[iChannel][iEvent]->GetFunction("impulse_fit"));
      
      App.Run(kTRUE);
      
      f64 integral = impulse_fit->Integral(XY.first.at(i_start) , XY.first.at(i_end));
      f64 fit_charge = integral/50 *1e12; // R=50 Ohm ; and in pico-Qoulomb
      fprintf(stdout,"integral = %e  and charge= %.3f pC \n", integral, fit_charge);
      
    }
    else{
     // for ref pulse--> difficult to fit so we take linear approx
      x_ref = Xpoint_of_linear_interpolation( XY.first, XY.second, i_start, i_max, cfd*ch_max);
    }
    
    //Graphs[iChannel][iEvent]->Write();
    }
    //get value only if a pulse was identified/ accepted
    if(ch_max){
       f64 SAT = (x_pico - x_ref)*1e09;
       fprintf(stdout,"SAT = %.3f ns \n", SAT);

    }
  }

  //App.Run();
  
  return 0;
}  
