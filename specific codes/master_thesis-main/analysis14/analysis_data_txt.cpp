#include "include/channels.hpp"
#include "include/header.hpp"
#include "include/types.hpp"

#include "./parsing.cpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>

// basic file operations
#include <iostream>
#include <fstream>
#include <iomanip>

//root includes
#include <TGraph.h>
#include <TMultiGraph.h>
#include "TH1.h"
#include "TF1.h"


// for math functions
#include <TMath.h>
#include <math.h>

using namespace std;

//auto main(i32 Argc, c8* Argv[]) -> i32 {
auto main() -> i32 {
  
  // open an (already existing) file to add  time-info for SAT events
  // ATTENTION: for keeping the previous information without overwrite, replace: ios:: out ----> ios:: app
  i32 pad_num =32;
  char filename_format[] = "info_pad_%d.txt";
  char filename[sizeof(filename_format)+2]; // up to 3 %d number digits
  snprintf(filename, sizeof(filename),filename_format, pad_num); // must !!!
  ofstream info_file;
  info_file.open(filename, ios:: app);
  
  //info_file << "gpeak3 , tfit3 , gpeak1 , tfit1 , pl3 , qfit1 , th20 , th80 , th120 , th160 , th200 , th250 , th300 , qh20 , qh80 , qh120 , qh160 , qh200 , qh250 , qh300 \n";
  
  const i32 Argc = 3;
  c8* Argv[Argc];
//i32 ii =0;
for (i32 ii = 40; ii < 58; ++ii) 
{

       string old_str = std::to_string(ii);
       size_t n_zero = 5;
       string new_str = string(n_zero - std::min(n_zero, old_str.length() ) , '0') + old_str;
  
      Argv[1] = (c8*)Form("/media/evridiki/backup/c1-trace-000ii-trc/C1--Trace--%s.trc", new_str.c_str()) ;
      //Argv[1] = (c8*)Form("/home/evridiki/Desktop/analysis14/trcs_pad13/C1--Trace--%s.trc", new_str.c_str()) ;
       
      Argv[2] = (c8*)Form("/media/evridiki/backup/c4-trace-000ii-trc/C4--Trace--%s.trc", new_str.c_str()) ;
      //Argv[2] = (c8*)Form("/home/evridiki/Desktop/analysis14/trcs_pad13/C2--Trace--%s.trc", new_str.c_str()) ;

      if (!exists( Argv[1] ) || !exists( Argv[2] ) ) {
         fprintf(stderr, "Usage:");
         fprintf(stderr, "No filenames given!");
         exit(1);
        }

    
   
   constexpr u64 nChannels = Argc-1;
   std::vector<channel> Channels(nChannels);
   for (u64 i = 0; i < nChannels; ++i) {
      Channels[i] = create_channel(Argv[i + 1]);
    }
   i32 nEvents   = Channels[1].Header.subarray_count;
   const f64 dx = Channels[1].Header.horizontal_interval;
   
   std::vector<std::vector<TGraph*>> Graphs(nChannels , std::vector<TGraph*>(nEvents));
  
   const f64 threshold=0.006; //Volts  
   
   
   //for multiple threshold timing technique
   const size_t M = 7;
   f64 thr[M] = {0.020 , 0.080, 0.120, 0.160, 0.200, 0.250, 0.300};
   f64 t_thr[M][2];
   
   
 //main loop
 for (i32 iEvent = 0; iEvent < nEvents; ++iEvent) {
        fprintf(stdout,"####### Running event: %d ######## \n", iEvent+1);
       
       f64 ch_max , MCP_peak; 
       f64 x_pico , x_ref;
       f64 p2;
       f64 fit_charge;
       
       //for multiple threshold timing technique-----> re-initalize here and always set 0 
       f64 q_thr[M] {};
       
   for (i32 iChannel = 0; iChannel < 2; ++iChannel) {
    auto XY = get_next_event(Channels[iChannel], iEvent);

    // get mean and rms of noise and eliminate baseline noise of vector y values   
    pdf noise = eliminate_noise(XY.second);
    
    //store in a graph
    Graphs[iChannel][iEvent] =
          new TGraph(XY.first.size(), XY.first.data(), XY.second.data());
    
    //find position and value of maximum
    u64 i_max = find_max_pos(XY.second);
    ch_max = XY.second.at(i_max);
    
    
    // accept the pulse?
    if (ch_max < threshold || i_max<20){
       //std::cout << ch_max <<" , " << threshold <<std::endl;
       ch_max=0;
       break; // go to next event
    }
    
    // to find the point where pulse starts-----> according to RMS of noise
    u64 i_start = find_pulse_start_pos(XY.second, i_max, noise.RMS);
    

    // constant fraction = 20% = 0.2
    const f32 cfd = 0.2;  

    if(iChannel){  //1st channel-->is MM
    
        // find position where pulse ends---> according to RMS of noise
      u64 dumb_range =4*(i_max-i_start);
       if(i_max +dumb_range >= XY.first.size()) dumb_range = XY.first.size()-i_max-1; 
      
      u64 i_end = distance(XY.second.begin(), min_element(XY.second.begin()+ i_max, XY.second.begin()+ i_max +dumb_range ) );
      
      if(abs(XY.second.at(i_end))>0.40*ch_max){
         cout << "FAKE PULSE ---- DUMBED ---- 1 \n" << endl;
         ch_max = 0;
         break;
      }
      if(XY.second.at(i_end)<noise.RMS){
         for (u64 i = i_max; i < i_end ; ++i) {
            if(XY.second.at( i) < noise.RMS ){
               i_end = i;
               break;
              }
           }
       }
       else{
           i_end = least_sq_for_ion_tail(XY.first, XY.second, i_max, i_end);
       }
      //----------------------------------------------------
      // now that we have the actual borders of pulse, we check if it is fake, in a range of 2 pulses left and right
      dumb_range = 2*(i_end - i_start); 
      if(i_start<=dumb_range) dumb_range = i_start-1; 
      if(i_end +dumb_range >= XY.first.size()) dumb_range = XY.first.size()-i_end-1; 
      u64 i_dumb;
      f64 dumb_array[4];
      //left side min and max
      i_dumb=distance(XY.second.begin(), min_element(XY.second.begin()+ i_start-dumb_range, XY.second.begin()+ i_start ) );
      dumb_array[0] = abs(XY.second.at(i_dumb));
      
      i_dumb=distance(XY.second.begin(), max_element(XY.second.begin()+ i_start-dumb_range, XY.second.begin()+ i_start ) );
      dumb_array[1] = abs(XY.second.at(i_dumb));
      
      //right side min and max
      i_dumb=distance(XY.second.begin(), min_element(XY.second.begin()+ i_end, XY.second.begin()+ i_end+dumb_range ) );
      dumb_array[2] = abs(XY.second.at(i_dumb));
      
      i_dumb=distance(XY.second.begin(), max_element(XY.second.begin()+ i_end, XY.second.begin()+ i_end + dumb_range ) );
      dumb_array[3] = abs(XY.second.at(i_dumb));
      
      for (u64 i = 0; i < 4 ; ++i) {
         if(dumb_array[i]>0.40*ch_max){
         cout << "FAKE PULSE ---- DUMBED ---- 2 \n" << endl;
         ch_max = 0;
         break;
      }
      }
      if(!ch_max) break;
      //-------------------------------------------------
        
    
    // Define the parameter array for fitting function-s
       Double_t par[8];
       //fit range
       u64 range =200;
       if(i_start<=range) range = i_start-1; 
       if(i_end+range >= XY.first.size()) range = XY.first.size()-i_end-1;   
    
    // ----------------------------------------------------------------------------------
    // fit only leading edge-----> sigmoid ----> for SAT 
    // parametrization follows from: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5774301/
    //------------------------------------------------------------------------------------
    
    // to find approximately max steepness of curve at x_mid(need to help initialize the fit)
    //u64 i_dumb;                      //not important now (ignore) ; helps to pass information for Xpoint_of_linear_interpolation function.
    f64 steepness_left = slope_at_x(XY.first, XY.second, i_start, i_max, 0.5*ch_max, i_dumb)*4/(XY.second.at(i_max)-XY.second.at(i_start));
    
    //to find approximately x where we are at 0.5 of Vmax (aka 0.5*ch_max) (need to help initialize the fit)      
      f64 x_mid_left = Xpoint_of_linear_interpolation( XY.first, XY.second, i_start, i_max, 0.5*ch_max);


      TF1 *leading_edge_fit = new TF1("leading_edge_fit",leading_edge_function, XY.first.at(i_start-range) , XY.first.at(i_max+range),4);   // IMPORTANT: NUM OF PARS
      leading_edge_fit -> SetParameters(XY.second.at(i_max)-XY.second.at(i_start), x_mid_left, steepness_left, XY.second.at(i_start) );
    
      Graphs[iChannel][iEvent]->Fit("leading_edge_fit","","",XY.first.at(i_start-range),XY.first.at(i_max+3)); 
      leading_edge_fit->GetParameters(&par[0]);
            
      x_pico = par[1] - 1/par[2]*log(par[0]/(cfd*ch_max-par[3])-1);
      p2 = par[2];
      
      /*-----------------------------------------------------------------------------------
      fit whole pulse----> double sigmoid ----> for charge/integram
      -------------------------------------------------------------------------------------*/

      // mid point and max slope at x_mid
       u64 i1 =0, i2=0 ;
      for (u64 i = i_max; i< i_end ; ++i) {
          if(XY.second.at(i) < 0.5*ch_max){
           i1 = i;
           i2=i-1;
           break;
          }
      } 
      f64 x1 = XY.first.at(i1);
      f64 x2 = XY.first.at(i2);
      f64 y1 = XY.second.at(i1);
      f64 y2 = XY.second.at(i2);
    
      f64 steepness_right = (y2-y1)/(x2-x1)*4/(XY.second.at(i_max)-XY.second.at(i_end));
      f64 x_mid_right = x1+ (0.5*ch_max-y1)*(x2-x1)/(y2-y1);
      
      //estimate the rest of parameters
      par[4] = XY.second.at(i_max)-XY.second.at(i_end);
      par[5] = x_mid_right;
      par[6] = steepness_right;
      par[7] = XY.second.at(i_end);
      
      TF1 *impulse_fit = new TF1("impulse_fit",impulse_function, XY.first.at(i_start-range) , XY.first.at(i_end+range),8); //IMPORTANT NUMBER OF PRMTRS
      impulse_fit-> SetParameters (par);
         
      Graphs[iChannel][iEvent]->Fit("impulse_fit","RS"); 
      //impulse_fit->GetParameters(&par[0]);
      
      f64 integral = impulse_fit->Integral(XY.first.at(i_start) , XY.first.at(i_end));
      fit_charge = integral/50 *1e12; // R=50 Ohm ; and in pico-Qoulomb
      
      //--------------------------------------------------
      // for multiple threshold technique
      //---------------------------------------------------
      fill(*t_thr, *t_thr + 2*M, 111*1e-09);
 
      size_t N=0;
      while(N<M && ch_max > thr[N] ){
           ++N;
         }
      
      for(u64 j = 0; j< N ; ++j){
          
      //-------------- left thr 
           
           for (u64 i = i_start; i< i_max ; ++i) {
              if(XY.second.at(i) > thr[j]){
                 i1 = i;
                 break;
               }
            } 
            x1 = XY.first.at(i1-1);
            x2 = XY.first.at(i1);
            y1 = XY.second.at(i1-1);
            y2 = XY.second.at(i1);
    
          t_thr[j][0] = x1+ (thr[j]-y1)*(x2-x1)/(y2-y1);
      
      //-------------- right thr
           
           for (u64 i = i_max; i< i_end ; ++i) {
              if(XY.second.at(i) < thr[j]){
                 i2 = i-1;
                 break;
               }
            } 
            x1 = XY.first.at(i2);
            x2 = XY.first.at(i2+1);
            y1 = XY.second.at(i2);
            y2 = XY.second.at(i2+1);
    
          t_thr[j][1] = x1+ (thr[j]-y1)*(x2-x1)/(y2-y1);
                 
      //------------------- charge over thr
           for (u64 i = i1; i<= i2 ; ++i) {
              q_thr[j] += dx*(XY.second.at(i) - thr[j] );
           }
           
           q_thr[j] = q_thr[j]*1e12/50; // Ohm
           }
    }
      
    else{
     // for ref pulse--> not possible to fit so we take linear approx
      x_ref = Xpoint_of_linear_interpolation( XY.first, XY.second, i_start, i_max, cfd*ch_max);
      MCP_peak = XY.second.at(i_max);
    }   
    }
    //get value only if a pulse was identified/ accepted
    if(ch_max){
       info_file <<setprecision(6);
       //MCP parameters 
       info_file  <<MCP_peak << " ";
       info_file.setf( std::ios::fixed);
       info_file  << x_ref*1e09 <<" ";
       info_file.unsetf( std::ios::fixed);
       
       //PICOSEC parameters of fit
       info_file << ch_max << " ";
       
       info_file.setf( std::ios::fixed);
       info_file << x_pico*1e09 <<" ";
       info_file.unsetf( std::ios::fixed);
       
       info_file << p2 << " "<< fit_charge << " ";
       
       //MULTI THR parameters
       for(u64 j = 0; j< M ; ++j){
        if (t_thr[j][0] != 111*1e-09 ) info_file.setf( std::ios::fixed);
       info_file << (t_thr[j][0] )*1e09 <<" " ;
       info_file.unsetf( std::ios::fixed);
       }
       for(u64 j = 0; j< M ; ++j)
       info_file << q_thr[j] <<" " ;
       
       info_file <<endl; // next event - next line
    }
  }

}  
  info_file.close();
  return 0;
} 
