#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>
#include <sstream>

// basic file operations
#include <iostream>
#include <fstream>

//root includes
#include <TApplication.h>
#include <TCanvas.h>
#include <TColor.h>
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TH2.h"
#include "TFile.h"
#include <TSystem.h>
#include <TTree.h>
#include <TStyle.h>
#include <TCut.h>
#include "TRootCanvas.h"
#include <TGraph.h>
#include <TFitResult.h>
#include "TH1.h"

// for math functions
#include <TMath.h>
#include <math.h>

using namespace std;
using u64 = uint64_t;

#include "./functions_pad.cpp"
//########################## MAIN ################################
int main(int Argc, char *Argv[]){
   
   if (Argc >3) {
    fprintf(stderr, "ERROR\n");
    fprintf(stderr, "AVAILIABLE RUNS: 627, 632 OR BOTH OF THEM\n");
    exit(1);
    }
  else if(Argc==1){
    fprintf(stderr, "No filenames given!\n");
    exit(1);
  }


  //for initial values of slew and resol fit OF EACH MCP 
  double fit_par_s[5]={-3.6911687, 8.04848308, -0.53114533, 5.57098107, 10.2080784}; // param array for slew
  double fit_par_r[5]= {-0.1, -5., -0.5, 5., 15}; // parameter array for resolution
  
  // for delta time range OF EACH PAD
  double range[2][4]={ {-7800,-7800,-9100,-8300},
                      {-7300,-7300,-8500, -7800} };
  double cable_delay[4] = {-7577.852, -7606.115, -8940.529, -8163.508};
  
  
  // root app
   TApplication App("app", nullptr, nullptr);
   auto* canvas = new TCanvas("canvas", "", 900, 900);
      gStyle->SetOptFit(1111);
   
   
   //read given files
   int num_runs = Argc-1;
   const char* run_for_title = (num_runs==1) ? Argv[1]:"S COMBINED";
   
   vector<TTree*> data_tree(num_runs);
   vector<TFile*> rootfile(num_runs);
   vector<unsigned int> run(num_runs);
   
   for (int k=0;k<num_runs;k++){
   	rootfile[k] = new TFile(Form("/home/evridiki/Downloads/test_run%s.root",Argv[k+1]),"read");
   	data_tree[k] = (TTree*) rootfile[k]->Get("Pico");
   	data_tree[k]->SetBranchStatus("*", 0);
   	fprintf(stdout,"read file\n");
   	
   	//convert to int
   	stringstream str_run;
	str_run << Argv[k+1];
	str_run >> run[k];
   }
   
   vector<info> vec_pico;
   vec_pico.reserve(1.5e5);
   
   //read file and retrieve the tree
for(int i =0;i<4;++i){
  int pad = i+1;
    
  for(int k=0;k<num_runs;++k){
  	info pico, mcp2;
  	vector<info> vec_pico_k, vec_mcp2;
  	double t2, t;
  	double x[3];
  	double chi2;
  	double p2;
  	int ntrk;
  
  	data_tree[k]->SetBranchStatus("xy", 1);
  	data_tree[k]->SetBranchStatus("chi2", 1);
  	data_tree[k]->SetBranchStatus("ntracks", 1);
  	data_tree[k]->SetBranchAddress("xy",x);
  	data_tree[k]->SetBranchAddress("chi2",&chi2);
  	data_tree[k]->SetBranchAddress("ntracks",&ntrk);
  	
  	
  	const char* which_mcp = pad<3? "":"2";
  	data_tree[k]->SetBranchStatus(Form("mcp%s_gpeak",which_mcp), 1);
  	data_tree[k]->SetBranchStatus(Form("mcp%s_qall",which_mcp), 1);
  	data_tree[k]->SetBranchStatus(Form("mcp%s_t",which_mcp), 1);
  	data_tree[k]->SetBranchAddress(Form("mcp%s_gpeak",which_mcp),&mcp2.v);
  	data_tree[k]->SetBranchAddress(Form("mcp%s_qall",which_mcp),&mcp2.q); 
  	data_tree[k]->SetBranchAddress(Form("mcp%s_t",which_mcp),&t2); 
  
  	data_tree[k]->SetBranchStatus(Form("mm%d_gpeak",pad), 1);
  	data_tree[k]->SetBranchAddress(Form("mm%d_gpeak",pad),&pico.v);
	data_tree[k]->SetBranchStatus(Form("mm%d_qe",pad), 1);
  	data_tree[k]->SetBranchAddress(Form("mm%d_qe",pad),&pico.q);
  	
  	data_tree[k]->SetBranchStatus(Form("mm%d_t",pad), 1);
  	data_tree[k]->SetBranchStatus(Form("mm%d_pl3",pad), 1);
  	data_tree[k]->SetBranchAddress(Form("mm%d_t",pad), &t); 
  	data_tree[k]->SetBranchAddress(Form("mm%d_pl3",pad), &p2); 
  
  	Int_t treentries = data_tree[k]->GetEntries();
  	cout <<treentries<<endl;
  	
  	for (int j=0;j<treentries;j++) { 
       	data_tree[k]->GetEntry(j); 
       
       	//define only "global" cuts
       	bool chi2_cut = (chi2<40.) && (ntrk==1);
       	//bool xy_cut = abs(x[0]-xc[i])<radius && abs(x[1]-yc[i])<radius;
       	bool mcp2_cut = mcp2.v>0.3 && mcp2.v<0.74;
       	bool line_cut = pico.q>=30.*pico.v;

       	if(chi2_cut && /*xy_cut &&*/ mcp2_cut && line_cut){
       	//if(mcp2_cut){
          		pico.t = (t-t2)*1000;
          		pico.rp2 = 1/p2;

          		vec_pico_k.push_back(pico);   
       	}   
  	}

  	overflow_cut(vec_pico_k,0.355,0.03); // cut overflow 
  	charge_cut(vec_pico_k,20,0.1); // cut overflow
  	dt_cut(vec_pico_k, range[0][i],range[1][i]); // cut times out of range
  	
  	//to remove cable delay
  	for(u64 j=0; j<vec_pico_k.size();j++){
  		vec_pico_k[j].t = vec_pico_k[j].t -cable_delay[i] +10;
  	}
  	
  	//now that is ready..
  	vec_pico.insert(vec_pico.end(), vec_pico_k.begin(), vec_pico_k.end());

  	cout <<"data after all the cuts:  "<< vec_pico_k.size() <<endl;
  	//disable again all branches
  	data_tree[k]->SetBranchStatus("*",0);
  }
}
cout <<"data final:  "<< vec_pico.size() <<endl;

  q_vector_sort(vec_pico);    // first RE-arrange the elements according to charges

  // fill the slewing and resol graphs according to
  //gauss fits of q-bins 
  string str =Form("./slew_RUN%s.txt",run_for_title);

  auto graphs = slew_resol_gr(vec_pico,"q",str);
  TGraphAsymmErrors *slewing = graphs.first;
  TGraphAsymmErrors *resol = graphs.second;

  slewing->SetTitle( Form("RUN%s: SAT for all pads ; E-peak Row charge (pC)",run_for_title) );
  resol->SetTitle("; E-peak Row charge (pC)" );
  
  //get fitting parameters
  //FIT
  int first_fit_point =0;
  int last_fit_point = slewing->GetN()-1;
  auto q_fit_range = fit_range(slewing, first_fit_point, last_fit_point);

  //fit(slewing, q_fit_range.first, q_fit_range.second, fit_par_s);
  
  canvas->Clear(); 
  canvas->Divide(1,2);
  canvas->cd(1);
  gPad->SetLogy();
  slewing->Draw("ap");
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  
  canvas->cd(2);
  resol->Draw("ap");
    
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore();
  
  vec_pico = correct_for_slew(vec_pico,fit_par_s); 
  
  str =Form("./res_RUN%s.txt",run_for_title);
   graphs = slew_resol_gr(vec_pico, "q",str );
   slewing = graphs.first;
   resol = graphs.second;
   
  slewing->SetTitle(Form("RUN%s: SAT for all pads ; E-peak Row charge (pC); Corrected slewing (ps)",run_for_title ));
  resol->SetTitle("; E-peak Row charge (pC)");
   
  //FIT
  first_fit_point=0;
  last_fit_point= resol->GetN()-1;
  q_fit_range = fit_range(resol, first_fit_point, last_fit_point);
   
  fit(resol, q_fit_range.first, q_fit_range.second, fit_par_r);

  canvas->Clear(); 
  canvas->Divide(1,2);
  canvas->cd(1);
  slewing->Draw("ap");
  canvas->cd(2);
  resol->Draw("ap");
    
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore(); 
    
  ///////////////////////////
  cout <<"END OF PROGRAM, CLOSE GRAPH WINDOW TO EXIT"<<endl;  
        
   TRootCanvas *rc = (TRootCanvas *)canvas->GetCanvasImp();
   rc->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()"); 
   //gApplication->Terminate();
   
  App.Run(kTRUE);
  
   
   canvas->Close();
   for(int k=0;k<num_runs;k++)
   	rootfile[k]->Delete();
   return 0;
}
