# master_thesis
## Time performance of a multi-pad PICOSEC-MM detector

## 1. Download repo to your mashine
'''  
$ git clone https://github.com/evridikiCh/master_thesis.git  
'''  
* you can also add an encryption key and download with ssh, see here: https://www.theserverside.com/blog/Coffee-Talk-Java-News-Stories-and-Opinions/GitHub-SSH-Key-Setup-Config-Ubuntu-Linux

## 2. analysis14 folder

This was the initial analysis I have done on previous runs in order to learn how to do standard signal processing, it actually does the same as "program" was given to me later, but in a more meshy way!!----> Created for single pad events!!!  
contains files:  
* my_main.cpp : displays chosen sequence of waveforms when thay are assigned as signals with and performs fits. 
* main.cpp : displays only a few pulses of four channels of oscilloscope
* parsing.cpp : help functions to read data from osciloscope, create waveform graphs, fit leaidng edge, short vectors etc.
* analysis_data_txt.cpp : store parameters to .txt files (for tzamaria needed)
* make_histos.cpp : displays histograms of MCP and PICO charge, graph of slewing and a double gaussian fit
* include folder : .h files Yiannis gave me once (help functions)

requirements:  
* c++ : version 14
* ROOT CERN (version 24.0.1 or later)

how to run it:  

'''  
$ make file.cpp  
$ ./ file  
'''  
(make executes code and ./ runs the executable file)
* main.cpp :  you need to add in terminal 4 trc files for the 4 osc. channels eg: ./main C1--Trace--00000.trc C2--Trace--00000.trc C3--Trace--00000.trc C4--Trace--00000.trc 
* my_main : SAME // also modify line 54 to chose dispayed events. Program will currently opens a ROOT APPLICATION when you run it, keep pressing '.q' to continue (not the best way: @TODO)
* analysis_data_txt : is supposed to read from a directory many .trc files (a whole run actually) perform standard signal processing and store parameters in .txt file named 'info_pad_%d.txt' where %d is the pad number. Chang to your path from lines 55 and 58.
* make histos : reads this 'info_pad_%d.txt' and make histograms, add you pad number from line 65.

## 3. scaning folder
This folder contains preliminary work I have done on scanning events : run627 & run632. It mostly finds the slewing effect (and another '2nd order slewing correction' by 1/p2 parameter/slope [Tzam idea])  
contains files:  
* functions_pad.cpp : defines functions for crateing slewing graph, sortinf vectors, eliminate overflow etc. (most important function in line 104 I think)
* pad_slew.cpp : creates the slewing graphs for pads 27,28,37,38 in 627 and 632 scanning runs.  You press 'Enter' key to continue
* does the same but not for every pad each time (combines all pads), Tzam asked for it, to see if they are the same?
* .txt files : store all parameters for Tzamaria

requirements:  
* c++ : version 14
* ROOT CERN (version 24.0.1 or later)

how to run it:  

'''  
$ make file.cpp  
$ ./ file  
'''  
(make executes code and ./ runs the executable file)

* functions_pad.cpp : you dont run it on its own (it only has funtions)
* pad_slew.cpp : In terminal you add the number or eun you would like to see eg: ./pad_slew 627 (you can run both toghether ./pad_slew 627 632).You press 'Enter' key to continue. Modify line 90 for your .root file location.
* does the same but not for every pad each time (combines all pads), Tzam asked for it, to see if they are the same?

## 4. py_analysis folder
these are all in python commands. Contains the main plots in my thesis.

Requirements:  

numpy  
matplotlib  
pandas  
scipy  
jupyter notebook  
uproot (to read .root files and translate them into pandas dataframes)  
pytorch (for the ANN)
(these are all python libraries)

contains files:  
* root_to_np.py : function to translate Tree files to pandas dataframes
* constrain_fit.py : the iterative algorithm to find pad centers (chapter 4 in my thesis)
* slew_resol.py : creates slewing and resolution graphs in python
* rest .ipynb : jupyter notebooks files to make plots (@TODO write analytical instructions in README file!)

how to run:  
@TODO write analytical instructions in README file!
