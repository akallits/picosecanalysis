        program main
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
       real h
        PARAMETER (NWPAWC=5000000)
        COMMON/pawc/H(NWPAWC)
       character*80 filename !*80 80 characters sto filename
       common/parameters/ param(3) !common :global variavle, 
       common/param_errors/errors(2,3)
      common/single_polya/polya_m,polya_s

       filename='tracking/track_RR_ALL.txt'

       ifit=2 ! 1 only no pes, 2 only position, 10 results !!!flag





       CALL HLIMIT(NWPAWC) 

       call hropen(1,'OUTFIT','accept.his','n',1024,istatus)

       call init
c read
       open(unit=69,file=filename,status='old')
       call dataread
       close(69)
c

      if (ifit.eq.1) then
         param(1)=14.7
         param(2)=2.125
         param(3)=-2.5075
      call FIT1
      end if
      if(ifit.eq.2) then
         param(1)=14.7
         param(2)=2.125
         param(3)=-2.5075
       call FIT2
      end if
 

      if(ifit.eq.3) call fit3

      if(ifit.eq.10) then
         call hropen(1,'OUTFIT','accept.his','n',1024,istatus)
         param(1)=14.7
         param(2)=2.125
         param(3)=-2.5075
         polya_m= 1.172 !default
         polya_s= 0.7876 !default

         call booking
         call predict
c here you finish, out the histograms
          call hrout(0,icycle, ' ')
         call hrend('OUTFIT')
         close(1)
       end if

c      do i=1,210
c         q=(i)*0.33
c         x=param(2)
c         y=param(3)
c         rm0=param(1)

c      qd=convol(q,x,y,rm0)
c      print*,sngl(q),sngl(qd)
c      end do



      stop
      end

      subroutine booking
      double precision bmin,bmax
      common/bins/nbins,bmin,bmax
      nbins=70
      nbinss=35
      bmin=0.1
      bmax=70.1
      call hbook1(1000,'Data',nbins,sngl(bmin),sngl(bmax),0.)
      call hbook1(1001,'Data In',nbinss,sngl(bmin),sngl(bmax),0.)
      call hbook1(2000,'Prediction',nbins,sngl(bmin),sngl(bmax),0.)
      call hbook1(2001,'Prediction IN',nbinss,sngl(bmin),sngl(bmax),0.)

      call hbook1(1101,'R distribution',16,0.,8.,0.)
      call hbook1(1102,'WG R distribution',16,0.,8.,0.)
      call hbook1(1103,'WG**2 R distribution',16,0.,8.,0.)
c      return
      end


      subroutine init
      double precision dfac,ttt,polya_m,polya_s,xxx,rf,dstep
      common/acc_factor/xxx(1601),rf(1601),dstep
      common/values/dfac(100)
      common/single_polya/polya_m,polya_s
      double precision rad_a,rad_c
      common/geometry/rad_a,rad_c
	 open(unit=61,
     & file='/Users/tzamaria/pawfiles/accept_muon/accept.txt',
     &  status='old')
         do i=1,1601 
            read(61,*) xxx0,rf0
            xxx(i)=xxx0
            rf(i)=rf0
         end do
         dstep=xxx(2)-xxx(1)
c
c  polya parameters
         polya_m= 1.172
         polya_s= 0.7876
c
c     geom calculate factorial 100 till line 130
         rad_a=5
         rad_c=3 
c     dfac log(100)
         dfac(1)=0
         dfac(2)=0
         do ii=3,100
            ttt=float(ii-1)
            dfac(ii)=dfac(ii-1)+dlog(ttt)
         end do

         do i=1,10
            print*,i-1,dfac(i),dexp(dfac(i))
         end do

c         return
         end
c
c
c
       double precision function dpoi(n,rm)
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      common/values/dfac(100)

      denom=dfac(n+1)
      dnum=float(n)*dlog(rm)
      dall=dnum-denom-rm
c      print*,n,rm,denom,dnum,dall
      if(dall.gt.-18) then
      dpoi=dexp(dall)
      else
         dpoi=0
      end if
c      return
      end

c
	double precision function polya(xx,npe,rmg,rmsg)
	double precision polya0,th,qm,a,b,over,x,gammaln
	double precision ds,xx,rmg,rmsg,thhh,thh
	polya=0
c        print*,'Polya 1',xx,npe,rmg,rmsg
	if(xx.le.0.) return
        if(npe.lt.1) then ! noise
           polya=0
c           return
        end if
c
      r=1.
      x=xx
      thhh=(rmsg/rmg)**2
      thh=1./thhh
      th=thh-1
      qm=rmg
c
      b=th+1
      a=float(npe)*(th+1)
      over=gammaln(a)

      polya0=a*dlog(b)+(a-1)*dlog(x/qm)-b*x/qm-over

	polya0=dexp(polya0)/qm
	polya=polya0
	
c      print*,'polya 2',b,a,over,polya0,polya	
c       return
       end
c

      double precision FUNCTION GAMMaLN(XX)
      double precision xx
      INTEGER J
      DOUBLE PRECISION SER, STP, TMP, X, Y, COF(6)
      SAVE COF, STP
C
      DATA COF, STP/76.18009172947146D0, -86.50532032941677D0,
     +    24.01409824083091D0, -1.231739572450155D0, 
     +    0.1208650973866179D-2, -0.5395239384953D-5, 
     +    2.5066282746310005D0/
C
      X = XX
      Y = X
      TMP = X + 5.5D0
      TMP = (X + 0.5D0) * LOG(TMP) - TMP
      SER = 1.000000000190015D0
C
      DO 11 J = 1, 6 
          Y = Y + 1.D0
          SER = SER + COF(J)/Y
 11   CONTINUE
C
      GAMMaLN = TMP + LOG(STP * SER / X)
C
c      RETURN
      END


c
      double precision function convol(q,x,y,rm0)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      common/single_polya/polya_m,polya_s
      common/acc_factor/xxx(1601),rf(1601),dstep
      common/geometry/rad_a,rad_c
      rmg=polya_m
      rmsg=polya_s
c
      convol=0
c
      eff=0
      xy=dsqrt(x**2+y**2)
      if(xy.le.rad_a-rad_c) then !
         eff=1.
      else if(xy.gt.rad_a+rad_c) then
         eff=0
      else
      rxy=xy/dstep 
      irxy=rxy+1
          if(irxy.lt.1.or.irxy.gt.1601) then ! 
             eff=0
          else
             eff=rf(irxy)
          end if
      end if
c
      rm_eff=rm0*eff !rm = number of pe 
c      print*,' Convol',rm0,xy,eff,rm_eff

c
      nmax=rm_eff+5*dsqrt(rm_eff)+1
      if(nmax.gt.50) nmax=50
c
c      print*,'*********nmax',nmax,rmg,rmsg,rm_eff
      do npe=0,nmax
        a1= polya(q,npe,rmg,rmsg)
             a2=dpoi(npe,rm_eff)
         convol=convol+ a1*a2
c         print*,'convol------------',npe,a1,a2,convol

      end do
      if(convol.le.0.) convol=1.d-10 !check  convolv to not crush
c
      return
      end




C--   THE FUNCTION TO MINIMIZE
      SUBROUTINE FCNES(NPAR,GRAD,VAL,PARM,IFLAG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
c      DIMENSION PARM(*),GRAD(*)
c
      val=0
c      ph_mn=parm(1)
c      dx=parm(2)
c      dy=parm(3)

c
      return
      end


c
      subroutine dataread
      double precision ximp,yimp,qep
      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)

      ndata=0
      do n=1,6000
      read(69,*,end=1111,err=2222) xi,yi,qi
      ndata=ndata+1
      ximp(n)=xi
      yimp(n)=yi
      qep(n)=qi
      end do
      print*, 'I did not find the end of file'
      return
 1111 continue
      print*,' I found the end of file after ',n-1,'  events'
      return
 2222 continue
      print *,'Error in Reading after',n-1
      stop
c
      end

      subroutine fit1
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)
       common/parameters/ param(3)
       common/param_errors/errors(2,3)
       common/pe_likel/ n_pe,rm_pe(50),vln_lik_pe(50)
c       param(1)=13.6
c       param(2)=0
c       param(3)=0.



         dx=param(2) 
         dy=param(3)

         n_pe=0
         do i=1,50
            rm_pe(i)=0.
            vln_lik_pe(i)=0
         end do
c
       dm=0.3 
       rcut=9
         sstt=2*dm/40
         do i=1,41 
         rm0=param(1)-dm+float(i-1)*sst
         rm_pe(i)=rm0 
c
         do n=1,ndata
            q=qep(n)
            x=ximp(n)-dx !allignment 
            y=yimp(n)-dy
            if(dsqrt(x**2+y**2).le.rcut) then
      qd=convol(q,x,y,rm0) 
      vln_lik_pe(i)=vln_lik_pe(i)-dlog(qd)
            end if
         end do

         end do

         vmin=999999.d+5
         pemin=param(1)
         imin=20
         do i=1,41
            if(vln_lik_pe(i).lt.vmin) then
               vmin=vln_lik_pe(i)
               pemin=rm_pe(i)
               imin=i
            end if
         end do
         param(1)=pemin
c
         v1=999999.D+5
         p1=2
         do i=imin,1,-1
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p1=-(rm_pe(i)-pemin)
               go to 1
            end if
         end do
 1       continue
c
         v2=999999.d+5
         p2=2
         do i=imin,40
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p2=rm_pe(i)-pemin
               go to 2
            end if
         end do
 2       continue
         dm=p1
         if(dm.lt.p2) dm=p2
         print*, 'LIMITS 0 FIT 1', DM,param(1),p1,p2

         print *, 'FIT 0 Likelihood ************'
         do i=1,40
         print*,(rm_pe(i)),(vln_lik_pe(i))
         end do

ccccccccccccccccccccccccccccccccccccccc
cfine tuning

         dx=param(2)
         dy=param(3)

         n_pe=0
         do i=1,50
            rm_pe(i)=0.
            vln_lik_pe(i)=0
         end do
c
       !!!!!!! STOP !!!!!!
         sstt=2*dm/40.
         do i=1,41
         rm0=param(1)-dm+float(i-1)*sstt
         rm_pe(i)=rm0
c
         do n=1,ndata
            q=qep(n)
            x=ximp(n)-dx
            y=yimp(n)-dy
            if(dsqrt(x**2+y**2).le.rcut) then 
      qd=convol(q,x,y,rm0)
      vln_lik_pe(i)=vln_lik_pe(i)-dlog(qd)
            end if
         end do

         end do
c
         print *, 'FIT 1 Likelihood ************'
         do i=1,40
         print*,sngl(rm_pe(i)),sngl(vln_lik_pe(i))
         end do


         vmin=9999999.d+5
         pemin=param(1)
         imin=20
         do i=1,41
            if(vln_lik_pe(i).lt.vmin) then
               vmin=vln_lik_pe(i)
               pemin=rm_pe(i)
               imin=i
            end if
         end do
         param(1)=pemin

c
         v1=999999.
         p1=2
         do i=imin,1,-1
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p1=-(rm_pe(i)-pemin)
               go to 11
            end if
         end do
 11      continue
c
         v2=999999.
         p2=2
         do i=imin,40
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p2=rm_pe(i)-pemin
               go to 12
            end if
         end do
 12       continue
          print*,' FIT 1 Results',param(1),p1,p2
          errors(1,1)=p1
         errors(1,2)=p2
         
       return
       end

c
ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c

      subroutine fit2
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)
       common/parameters/ param(3)
       common/param_errors/errors(2,3)
       common/xy_likel/ n_xy,rm_xy(50,50,2),vln_lik_xy(50,50)
       dimension vminx(50,2),vminy(50,2)
c       param(1)=13.6
c       param(2)=0
c       param(3)=0.




         n_xy=0
         do i=1,50
            do j=1,50
            rm_xy(i,j,1)=0.
            rm_xy(i,j,2)=0.
            vln_lik_xy(i,j)=0
         end do
         end do
c
       dmx=0.25
       dmy=0.25
         sstx=2*dmx/40.
         ssty=2*dmy/40.
         do ix=1,41
         do iy=1,41
         dx=param(2)-dmx+float(ix-1)*sstx
         dy=param(3)-dmy+float(iy-1)*ssty
         rm_xy(ix,iy,1)=dx
         rm_xy(ix,iy,2)=dy


         rm0=param(1)
c
c         print *, 'values *****' ,rm0,dx,dy
         do n=1,ndata
            q=qep(n)
            x=ximp(n)-dx
            y=yimp(n)-dy

      qd=convol(q,x,y,rm0)
      if(qd.le.0) then
         print*,'------------',x,y,sqrt(x**2+y**2),q,rm0
         end if
      vln_lik_xy(ix,iy)=vln_lik_xy(ix,iy)-dlog(qd)
c      print *,'data',q,x,y,rm0,qd,vln_lik_xy(ix,ix)
         end do

         end do
         end do

         vmin=999999.d+5
         xmin=param(2)
         ymin=param(3)
         iminx=20
         iminy=20

         do ix=1,41
            do iy=1,41
            if(vln_lik_xy(ix,iy).lt.vmin) then
               vmin=vln_lik_xy(ix,iy)
               xmin=rm_xy(ix,iy,1)
               ymin=rm_xy(ix,iy,2)
               iminx=ix
               iminy=iy
            end if
         end do
         end do
         param(2)=xmin
         param(3)=ymin

c
         do ix=1,41
            vminx(ix,1)=rm_xy(ix,1,1)!value of displacement for y = 1
            vminx(ix,2)=9999999.d+5
            do iy=1,41
            if(vln_lik_xy(ix,iy).lt.vminx(ix,2)) then
               vminx(ix,2)=vln_lik_xy(ix,iy)
         if(ix.eq.iminx) print*,sngl(vln_lik_xy(ix,iy))
            end if
         end do
         if(ix.eq.iminx) print*,'min',sngl(vminx(ix,2))
         end do

c
         do iy=1,41
            vminy(iy,1)=rm_xy(1,iy,2)
            vminy(iy,2)=9999999.d+5
            do ix=1,41
            if(vln_lik_xy(ix,iy).lt.vminy(iy,2)) then
               vminy(iy,2)=vln_lik_xy(ix,iy)
            end if
         end do
         end do




c

         do i=1,2
            print*,(sngl(rm_xy(ix,ix,i)),ix=1,41)
            end do
            print*,'*******************************************'
            print*,'*******************************************'
            print*,'*******************************************'
         do iy=iminy-10,iminy+10
            do ix=iminx-10,iminx+10
            print*,sngl(rm_xy(ix,iy,1)),sngl(rm_xy(ix,iy,2)), 
     &    sngl(vln_lik_xy(ix,iy))
         end do
         end do
c
         print *,'---------  XPROFILE'
         do ix=1,41
            print*, vminx(ix,1),vminx(ix,2)
         end do
         p1x=11111
         p2x=11111
         do ix=1,iminx
            if(vminx(ix,2).le.vmin+2) then
               p1x=-vminx(ix,1)+xmin
               go to 1
            end if
         end do
 1       continue
         do ix=iminx,41
            if(vminx(ix,2).ge.vmin+2) then
               p2x=vminx(ix,1)-xmin
               go to 2
            end if
         end do
 2       continue
         p1y=11111
         p2y=11111
         do iy=1,iminy
            if(vminy(iy,2).le.vmin+2) then
               p1y=-vminy(iy,1)+ymin
               go to 3
            end if
         end do
 3       continue
         do iy=iminy,41
            if(vminy(iy,2).ge.vmin+2) then
               p2y=vminy(iy,1)-ymin
               go to 4
            end if
         end do
 4       continue


c
         print *,'---------  YPROFILE'
         do iy=1,41
            print*, vminy(iy,1),vminy(iy,2)
         end do




         print*, 'XY RESULTS',iminx,iminy,xmin,ymin,vmin
         print*, 'X ERRORS',p1x,p2x,'.   Y ERRORS',p1y,p2y
       return
       end


c
c
c
      subroutine fit3
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)
       common/parameters/ param(3)
       param(1)=13
       param(2)=0
       param(3)=0.
       return
       end


c
c
c
      subroutine predict
       IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)
       common/parameters/ param(3)
       common/param_errors/errors(2,3)
      common/bins/nbins,bmin,bmax
      dimension qsave(1000)


      rm0=param(1)
      dx=param(2)
      dy=param(3)
c
      nbb=nbins*10
      if(nbb.gt.1000) nbb=1000
      bstep=(bmax-bmin)/float(nbb)
c      print*,nbb,bmax,bmin,bstep
         do n=1,ndata

            q=qep(n)
            x=ximp(n)-dx
            y=yimp(n)-dy
            radi=dsqrt(x**2+y**2)
            call hfill(1101,sngl(radi),1.,1.)
            call hfill(1102,sngl(radi),1.,sngl(q))
            call hfill(1103,sngl(radi),1.,sngl(q**2))
            call hfill(1000,sngl(q),1.,1.)
            if(dsqrt(x**2+y**2).lt.2.) then
            call hfill(1001,sngl(q),1.,1.)
            end if
c
            qsum=0.
            do iq=1,nbb
               qi=(iq-1)*bstep+bstep/2
               qd=convol
               (qi,x,y,rm0)
      qsum=qsum+qd
      qsave(iq)=qd
           end do
c       print*,'******************** Event',n,q,x,y,dsqrt(x**2+y**2),qsum

           do iq=1,nbb
               qi=(iq-1)*bstep+bstep/2
              wg=qsave(iq)/qsum
               call hfill(2000,sngl(qi),1.,sngl(wg))
            if(dsqrt(x**2+y**2).lt.2) then
               call hfill(2001,sngl(qi),1.,sngl(wg))
           end if
           end do
         end do

         return
         end
