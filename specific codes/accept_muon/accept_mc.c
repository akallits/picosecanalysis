//
//  accept_mc.c
//  
//
//  Created by Giannis Maniatis on 07/06/2019.
//

#include "accept_mc.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <TH2.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>

using namespace std;


int nphotons = 100000, npos = 255;

double absor = 0.24,  cone_r = 3, pad_r = 5, step = 0.1, pi2 = 2*TMath::Pi(), phot = 5;

double rndm_n1, rndm_n2, r, phi,x, x0, y, y00, x1, x01, x2, x02, x3, x03, x4, y11, y01, y2, y02, y3, y03, y4, dx, rb, rb1, rb2, rb3, ins_val;

bool cr1;

void accept_mc(){
    

    
    ofstream my;
    my.open("acc.txt");
    
    TRandom3 *rn = new TRandom3();
    rn ->SetSeed(0);
    vector<double> all(npos,0), acc(npos,0), pos, xvec, yvec, acc_grapgh;
    
    for(int i = 0; i < npos; i++){
        pos.push_back((i)*step);
        
    }
    
    
    
    for(int i = 0; i < nphotons; i++){
        rndm_n1 = rn->Uniform(0,1);
        rndm_n2 = rn->Uniform(0,1);
        
        r = cone_r * rndm_n1;
        phi = pi2 * rndm_n2;

        x = r * TMath::Cos(phi);
        y = r * TMath::Sin(phi);
    

        x1 = (r + 2*cone_r)*TMath::Cos(phi);
        y11 = (r + 2*cone_r)*TMath::Sin(phi);
        
        x2 = (r + 4*cone_r)*TMath::Cos(phi);
        y2 = (r + 4*cone_r)*TMath::Sin(phi);
        
        x3 = (r + 6*cone_r)*TMath::Cos(phi);
        y3 = (r + 6*cone_r)*TMath::Sin(phi);
        
        x4 = (r + 8*cone_r)*TMath::Cos(phi);
        y4 = (r + 8*cone_r)*TMath::Sin(phi);
        
        for(int j = 0; j < npos; j++){
            
            cr1 = 0;
            
            dx = j * step;
            x0 = x + dx;
            y00 = y;
            rb = TMath::Sqrt(x0*x0 + y00*y00);
            xvec.push_back(x0);
            yvec.push_back(y00);

            all.at(j) = all.at(j) + 1;


            if(rb <= pad_r){
                acc.at(j) = acc.at(j) + 1;
                cr1 = 1;
//                cout << i << "  pos " << j << endl;
            }
            
            if(rb <= phot){  cr1 = 1;} //on the photocathode and NO reflection
            
            if(cr1 != 1){
                //FIRST REFLECTION
//                cout << i << "  pos " << j << "!!!! " << endl;
                x01 = x1 + dx;
                y01 = y11;
                rb1 = TMath::Sqrt(x01*x01 + y01*y01);

                if (rb1 <= pad_r) {
                    acc.at(j) = acc.at(j) + absor;
                    cr1 = 1;
                }

                if(rb1 <= phot) cr1 = 1;
            
                if(cr1 != 1){
                    
                    //SECOND REFLECTION
                    
                    x02 = x2 + dx;
                    y02 = y2;
                    rb2 = TMath::Sqrt(x02*x02 + y02*y02);
                    if (rb2 <= pad_r) {
                        acc.at(j) = acc.at(j) + absor*absor;
                        cr1 = 1;
                    }

                    if(rb2 <= phot) cr1 = 1;
            
                    if(cr1 != 1){
                        x03 = x3 + dx;
                        y03 = y3;
                        rb3 = TMath::Sqrt(x03*x03 + y03*y03);
                        if(rb3 <= pad_r){
                            acc.at(j) = acc.at(j) + TMath::Power(absor,3);
                        }
                
                    }//Break loop after second reflection
                }  //Break loop after first reflection
            }  // Break loop without reflection

            
        } // end of npos loop

        if(i % 1000 == 0) cout << i << endl;

    } // end of nphotons loop
    
    for (int i = 0; i < npos; i++) {
        my << std::fixed <<std::setprecision(9) << pos.at(i) << "\t" << acc.at(i)/all.at(i) << endl;
        acc_grapgh.push_back(acc.at(i)/all.at(i));
    }
    
    
    
    cout << "size " << all.size() << endl;

    TGraph *gr1 = new TGraph(pos.size(),&pos[0],&acc_grapgh[0]);
    TCanvas *c1 = new TCanvas("c1", "profile", 100, 0, 800, 800);
    gr1->Draw("AP *");


    my.close();
}
