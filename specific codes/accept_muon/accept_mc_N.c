//
//  accept_mc_N.c
//  
//
//  Created by Giannis Maniatis on 09/06/2019.
//

#include "accept_mc_N.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <TH2.h>
#include <TRandom.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>


using namespace std;


int nphotons = 100000, npos = 510;

double refl = 0.22, cone_r = 3, pad_r = 5, step = 0.05, pi2 = 2*TMath::Pi(), phot = 5, p_imp[8], rmax = 0;

double ptot = 1 - refl, x00, y00, rndm_n1, rndm_n2, r, phi, dx, rb;

vector<double> all(npos,0), acc(npos,0), pos, x_imp(8,0), y_imp(8,0), acc_grapgh;


void accept_mc_N(){
    
    
    ofstream my;
    my.open("acc_N.txt");
    
    TRandom3 * rn = new TRandom3();
    rn->SetSeed(0);
    
    
    
    
    p_imp[0] = ptot;
    for(int i = 1; i < 8; i++){
        p_imp[i] = (1 - ptot) * (1 - refl);
        ptot = ptot + p_imp[i];
    }
    
    
    for(int i = 0; i < npos; i++){
        pos.push_back((i)*step);
        
    }
    
    
    for(int i = 0; i < nphotons; i++){
        
        rndm_n1 = rn->Uniform(0,1);
        rndm_n2 = rn->Uniform(0,1);
        
        r = cone_r * rndm_n1;
        phi = pi2 * rndm_n2;
        
        for (int k = 0; k < 8; k++) {
            x_imp.at(k) = (r + k*2*cone_r)*TMath::Cos(phi);
            y_imp.at(k) = (r + k*2*cone_r)*TMath::Sin(phi);
        }
        

        
        for (int j = 0; j < npos; j++) {
            all.at(j) = all.at(j) + 1;
            dx = j*step;
            
            for(int w = 0; w < 8; w++){
                x00 = x_imp.at(w) + dx;
                y00 = y_imp.at(w);
                rb = TMath::Sqrt(x00*x00 + y00*y00);

                if(rb <= pad_r) acc.at(j) = acc.at(j) + p_imp[w];
            
                
            } //end of 8 positions loop
            
        } //end of npos loop
        
        
        
    }  //end of nphotons loop
    
    for(int i = 0; i < npos; i++){
        my << std::fixed <<std::setprecision(9) << pos.at(i) << "\t" << acc.at(i)/all.at(i) << endl;
        acc_grapgh.push_back(acc.at(i)/all.at(i));

    }

    
    cout << "size " << acc.size() << endl;
    TGraph *gr1 = new TGraph(pos.size(),&pos[0],&acc_grapgh[0]);
    TCanvas *c1 = new TCanvas("c1", "profile", 100, 0, 800, 800);
    gr1->Draw("AP *");
    
    my.close();

    
}
