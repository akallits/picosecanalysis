!//
!//  Header.h
!//
!//
!//  Created by sampson on 1/12/19.
!//
!//
!
!c
   subroutine dataread
   double precision ximp,yimp,qep
   COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)

   ndata=0
   do n=1,6000
    read(69,*,end=1111,err=2222) xi,yi,qi
    ndata=ndata+1
    ximp(n)=xi
    yimp(n)=yi
    qep(n)=qi
    end do
    print*, 'I did not find the end of file'
    return
1111 continue
      print*,' I found the end of file after ',n-1,'  events'
     return
2222 continue
     print *,'Error in Reading after',n-1
    stop
!c
     end

!////********************************************************************   fit1

      subroutine fit1
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      COMMON/DATA/ndata,ximp(6000),yimp(6000),qep(6000)
       common/parameters/ param(3)
       common/param_errors/errors(2,3)
       common/pe_likel/ n_pe,rm_pe(50),vln_lik_pe(50)
!c       param(1)=13.6
!c       param(2)=0
!c       param(3)=0.

         dx=param(2)
         dy=param(3)

         n_pe=0
         do i=1,50
            rm_pe(i)=0.
            vln_lik_pe(i)=0
         end do
!c
       dm=0.3
       rcut=9 !RING RADIUS ???
         sstt=2*dm/40.
         do i=1,41 ! loop over mean pe values
         rm0=param(1)-dm+float(i-1)*sstt
         rm_pe(i)=rm0
!c
         do n=1,ndata
            q=qep(n)
            x=ximp(n)-dx
            y=yimp(n)-dy
            if(dsqrt(x**2+y**2).le.rcut) then ! select tracks inside the ring
      qd=convol(q,x,y,rm0)
      vln_lik_pe(i)=vln_lik_pe(i)-dlog(qd)
            end if
         end do

         end do

         vmin=999999.d+5
         pemin=param(1)
         imin=20
         do i=1,41
            if(vln_lik_pe(i).lt.vmin) then
               vmin=vln_lik_pe(i)
               pemin=rm_pe(i)
               imin=i
            end if
         end do
         param(1)=pemin
!c
         v1=999999.D+5
         p1=2
         do i=imin,1,-1
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p1=-(rm_pe(i)-pemin)
               go to 1
            end if
         end do
 1       continue
!c
         v2=999999.d+5
         p2=2
         do i=imin,40
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p2=rm_pe(i)-pemin
               go to 2
            end if
         end do
 2       continue
         dm=p1
         if(dm.lt.p2) dm=p2
         print*, 'LIMITS 0 FIT 1', DM,param(1),p1,p2

         print *, 'FIT 0 Likelihood ************'
         do i=1,40
         print*,(rm_pe(i)),(vln_lik_pe(i))
         end do

!ccccccccccccccccccccccccccccccccccccccc
!    cfine tuning

         dx=param(2)
         dy=param(3)

         n_pe=0
         do i=1,50
            rm_pe(i)=0.
            vln_lik_pe(i)=0
         end do
!c

         sstt=2*dm/40.
         do i=1,41 ! loop over mean pe values
         rm0=param(1)-dm+float(i-1)*sstt
         rm_pe(i)=rm0
!c
         do n=1,ndata
            q=qep(n)
            x=ximp(n)-dx
            y=yimp(n)-dy
            if(dsqrt(x**2+y**2).le.rcut) then ! select tracks inside the intermost ring
      qd=convol(q,x,y,rm0)
      vln_lik_pe(i)=vln_lik_pe(i)-dlog(qd)
            end if
         end do

         end do
!c
         print *, 'FIT 1 Likelihood ************'
         do i=1,40
         print*,sngl(rm_pe(i)),sngl(vln_lik_pe(i))
         end do


         vmin=9999999.d+5
         pemin=param(1)
         imin=20
         do i=1,41
            if(vln_lik_pe(i).lt.vmin) then
               vmin=vln_lik_pe(i)
               pemin=rm_pe(i)
               imin=i
            end if
         end do
         param(1)=pemin

!c
         v1=999999.
         p1=2
         do i=imin,1,-1
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p1=-(rm_pe(i)-pemin)
               go to 11
            end if
         end do
 11      continue
!c
         v2=999999.
         p2=2
         do i=imin,40
            if(vln_lik_pe(i)-vmin-2.gt.0.) then
               p2=rm_pe(i)-pemin
               go to 12
            end if
         end do
 12       continue
          print*,' FIT 1 Results',param(1),p1,p2
          errors(1,1)=p1
         errors(1,2)=p2
         
       return
       end

!c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccc
!c
