//
//  trans_accept.c
//  
//
//  Created by Giannis Maniatis on 23/01/2019.
//

#include "trans_accept.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>

using namespace std;

//////// Functions //////////
double dpoi(int bin, double n_pe, double fact[100]);
double polya(double q, int i, double polya_m, double polya_s);


////////////////////////////
double param_1 = 14.7, param_2 = 2.125, param_3 = -2.5075;


//geometrical parameters
double pico_rad = 5, cone_rad = 3;

//polya parameters
double polya_m = 1.172, polya_s = 0.7876;

double example(double a, double x, double y, double dstep, double mean_number_pe, double rf[1600]);


//data arguments
double ximp[6000], yimp[6000], qep[6000], xi, yi, qepi;
double xx_accept[1600], rf[1600], xx0, rf0;

int counter = 0, ndata = 0;

double n_pe, qd = 99.5;

double convol(double q, double x, double y, double dstep, double rf_c[1600], double mean_number_pe, double fc[100]);

void trans_accept_2(){
    
    double gf;
    
    std::fstream myfile("accept.txt", std::ios_base::in);
    
    while (!myfile.eof()) {
        myfile >> xx0 >> rf0;
        xx_accept[counter] = xx0;
        rf[counter] = rf0;
        counter++;

    }
    
    double step = xx_accept[2] - xx_accept[1];
   

//    open unit69-> track_RR_ALL.txt
    std::fstream myfile2("./tracking/track_RR_ALL.txt", std::ios_base::in);

    
    while (!myfile2.eof()) {
        myfile2 >> xi >> yi >> qepi;
        ximp[ndata] = xi;
        yimp[ndata] = yi;
        qep[ndata] = qepi;
        ndata++;
    }
    
    ndata = ndata - 1;
    int number = 100;
    double factorials[number];
    for (int i = 0; i < number; i++) {
       factorials[i] = TMath::Factorial(i);
        
        if(i < 10) {
            std::cout << "dfac(" << i << ")" << " = " <<std::setprecision(9) << TMath::Log(factorials[i]) << " fac = " <<  factorials[i] << endl;}
    }

        cout << "I found the end of file after " << ndata+1 << " events." << endl;
    
    
    //////////////// FIT1 //////////////////
//    double dx = param_2, dy = param_3;
//
//    double mean_pe[50], lhd[50], mean_pe_val;
//    double dm = 0.3, rcut = 9, sstt = (2*dm)/40;
//    double x, y, q;
//
//
//    for (int i = 0; i < 50; i++) {
//        mean_pe[i] = 0;
//        lhd[i] = 0;
//    }
//
//    for (int i = 0; i < 40; i++) {
//        mean_pe_val = param_1 - dm + i*sstt;
//        mean_pe[i] = mean_pe_val;
//        for (int j = 0; j < ndata + 1; j++) {
//            x = ximp[j] - dx;
//            y = yimp[j] - dy;
//            q = qep[j];
//            if (TMath::Sqrt(x*x + y*y) <= rcut) {
//                if(q > 0){
//                qd = convol(q, x, y, step, rf, mean_pe_val, factorials);
//                lhd[i] = lhd[i] - TMath::Log(qd);}
//            }
//        }
//
//    }
//
//
//                //////////// Find minimum ////////////
//    double vmin = TMath::Power(10000,4), imin = 20, pemin = param_1;
//
//    for (int i = 0; i < 40; i++) {
//        if (lhd[i] < vmin) {
//            vmin = lhd[i];
//            pemin = mean_pe[i];
//            imin = i;  //bin with minimum likelihood
//        }
//    }
//    param_1 = pemin;
//
//    double vmin2 = TMath::Power(10000000,5), p1 = 2, p2 = 2;
//
//    bool cond1 = 0, cond2 = 0;
//    for (int i = imin; i >= 0; i--) {
//        if (lhd[i] - vmin - 2 > 0) {
//            p1 = -(mean_pe[i] - pemin);
//            cond1 = 1;
//        }
//        if (cond1 == 1) break;
//    }
//
//    for (int i = imin; i < 40; i++) {
//        if (lhd[i] - vmin - 2 > 0) {
//            p2 = mean_pe[i] - pemin;
//            cond2 = 1;
//        }
//        if(cond2 == 1) break;
//    }
//
//    dm = p1;
//    if (p1 < p2) {
//        dm = p2;
//    }
//
//    cout << "LIMITS 0 FIT 1 " << dm << ", " << param_1 << ", " << p1 << ", " << p2 <<endl;
//    cout << " --------- FIT 0 LIKELIHOOD -----------" <<  endl;
//    for (int k =0; k < 40; k++) {
//        std::cout << std::setprecision(8) << mean_pe[k] << ", "<< lhd[k]  << endl;
//
//    }
//
//
//
//
//    ////// second fit//////
//    for (int i = 0; i <= 50; i++) {
//        mean_pe[i] = 0;
//        lhd[i] = 0;
//    }
//
//    sstt = 2*dm/40;
//    for (int i = 0; i <= 40; i++) {
//        mean_pe[i] = param_1 - dm + i*sstt;
//
//        for (int n = 0; n < ndata + 1; n++) {
//            q = qep[n];
//            x = ximp[n] - dx;
//            y = yimp[n] - dy;
//            if (TMath::Sqrt(x*x + y*y) <= rcut) {
//                if(q > 0){
//                qd = convol(q, x, y, step, rf, mean_pe[i], factorials);
//                    lhd[i] = lhd[i] - TMath::Log(qd);}
//            }
//            cout << n << " , " << "qd2 = " << qd << " , " <<  TMath::Log(qd) << " , " << lhd[i] << endl;
//
//        }
////        cout << i << " , " << "qd2 = " << qd << " , " <<  TMath::Log(qd) << " , " << lhd[i] << endl;
//
//    }
//
//
//    cout << "FIT 1 Likelihood ****** " << endl;
//    for(int i = 0; i < 40; i++){
//        std::cout << std::setprecision(8) << mean_pe[i] << ", "<< lhd[i]  << endl;
//
//    }
//
//
//    pemin = param_1;
//    imin = 20;
//    for (int i = 0; i < 40; i++) {
//        if (lhd[i] < vmin) {
//            vmin = lhd[i];
//            pemin = mean_pe[i];
//            imin = i;
//        }
//    }
//
//    param_1 = pemin;
//
//
//    cond1 = 0;
//    cond2 = 0;
//    p1 = 2;
//    for (int i = imin; i >= 0; i--) {
//        if (lhd[i] - vmin - 2 > 0) {
//            p1 = - (mean_pe[i] - pemin);
//            cond1 = 1;
//        }
//        if (cond1 == 1) break;
//    }
//
//    p2 = 2;
//    for (int i = imin; i < 40; i++) {
//        if (lhd[i] - vmin - 2 > 0) {
//            p2 =  (mean_pe[i] - pemin);
//            cond2 = 1;
//        }
//        if (cond2 == 1) break;
//    }
//    cout << "FIT 1 Results " << param_1 << " , " << p1 << " , " << p2 << endl;
    /////////////// FIT 2 //////////////////
    
//    param_1 = 14.7;
//    param_2 = 2.125;
//    param_3 = -2.5075;
    
    
    
    
    double meanxy[50][50][2]={{{0}}}, lhd_all[50][50]={0}, dmx = 0.25, dmy = 0.25, ssttx, sstty, vmin,dx, dy,q, x, y;
    double vminX[50][2]={0}, vminY[50][2]={0};
    double rm0;
    double xmin, ymin;
    
    bool cond1 = 0;
    bool cond2 = 0;
    
    int min_x_bin, min_y_bin;
    vmin = TMath::Power(10000,4);

    ssttx = 2*dmx/40;
    sstty = 2*dmy/40;
    
    double ex;
    for (int i = 0; i <= 40; i++) {
        for (int j = 0; j <= 40; j++) {
            dx = param_2 - dmx + i*ssttx;
            dy = param_3 - dmy + j*sstty;
            meanxy[i][j][0] = dx;
            meanxy[i][j][1] = dy;

            rm0 = param_1;

            for (int n = 0; n <= ndata; n++) {
                q = qep[n];
                x = ximp[n] - dx;
                y = yimp[n] - dy;
                qd = convol(q, x, y, step, rf, rm0, factorials);
                if(qd <= 0){
                    cout << "------" << x << y << TMath::Sqrt(x*x+y*y) << q << rf << endl;
                }
                lhd_all[i][j] =  lhd_all[i][j] - TMath::Log(qd);
            }
        }
    }


    
    xmin = param_2;
    ymin = param_3;
    min_x_bin = 20;
    min_y_bin = 20;



    for (int i = 0; i <= 40; i++) {
        for (int j = 0; j <= 40; j++) {
            if (lhd_all[i][j] < vmin) {
                vmin = lhd_all[i][j];
                xmin = meanxy[i][j][0];
                ymin = meanxy[i][j][1];
                min_x_bin = i;
                min_y_bin = j;
            }
        }
    }

    
    param_2 = xmin;
    param_3 = ymin;
    for (int i = 0; i <= 40; i++) {
        vminX[i][0] = meanxy[i][0][0];
        vminX[i][1] = TMath::Power(10,12);
        for (int j = 0; j < 40; j++) {
            if (lhd_all[i][j] < vminX[i][1]) {
                vminX[i][1] = lhd_all[i][j];
                if (i == min_x_bin) {
                }
            }

        }
    }

    for (int j = 0; j <= 40; j++) {
        vminY[j][0] = meanxy[0][j][1];
        vminY[j][1] = TMath::Power(10,12);
        for (int i = 0; i <=40; i++) {
            if (lhd_all[i][j] < vminY[j][1]) {
                vminY[j][1] = lhd_all[i][j];
            }
        }
    }


    /////////  X Profile /////////
    for(int i = 0; i <= 40; i++) {
        //cout << vminX[i][0] << "\t" << vminX[i][1] <<endl;
    }


    cond1 = 0;
    cond2 = 0;
    double p1x = 11111, p2x = 11111;

    for (int i = 0; i <= min_x_bin; i++) {
        if (vminX[i][1] <= vmin+2) { 
            p1x = -vminX[i][0] + xmin;
            cond1 = 1;
        }
        if(cond1 == 1) break;
    }

    for (int i = min_x_bin; i <= 40; i++) {
        if (vminX[i][1] >= vmin+2) {
            p2x = vminX[i][0] - xmin;
            cond2 = 1;
        }
        if(cond2 == 1) break;
    }


    double p1y = 11111, p2y = 11111;
    cond1 = 0; cond2 = 0;

    for (int j = 0; j <= min_y_bin; j++) {
        if (vminY[j][1] <= vmin+2) {
            p1y = -vminY[j][0] + ymin;
            cond1 = 1;
        }
        if(cond1 == 1) break;
    }

    for (int j = min_y_bin; j <= 40; j++) {
        if (vminY[j][1] >= vmin + 2) {
            p2y = vminY[j][0]- ymin;
            cond2 = 1;
        }
        if(cond2 == 1) break;
    }
    
    cout << "XY RESULTS" << min_x_bin << "\t" << min_y_bin << "\t" << xmin << "\t" << ymin << "\t" << vmin << endl;
    cout << "X ERRORS " << p1x << "\t" << p2x << "\t" << p1y << "\t" << p2y << endl;
}



double dpoi(int bin, double n_pe, double fact[100]){
    
    double poi;
    double number = ((bin) * TMath::Log(n_pe) - TMath::Log(fact[bin]) - n_pe);// oxi

    if (number > -18) {                 //GIATI -18!!!!!!
        poi = TMath::Exp(number);
    }
    else poi = 0;
    return poi;
}

double polya(double q, int i, double polya_m, double polya_s){
    
    double pol, over;

    
    double r = 1, par_pol_1, par_pol_2;
    double a;
    par_pol_1 = 1/TMath::Power(polya_s/polya_m,2);
    a = i * par_pol_1;
    pol = a * TMath::Log(par_pol_1) + (a-1)*TMath::Log(q/polya_m) - (par_pol_1 * q)/(polya_m) - ROOT::Math::lgamma(a);
    pol = (TMath::Exp(pol))/polya_m;
  
    return pol;
    
}


double convol(double q, double x, double y, double dstep, double rf_c[1600], double mean_number_pe, double fc[100]){

    double xy, eff,  m_n_pe, q_convol, max_fit, a1, a2;
    int rxy,irxy;

    q_convol = 0;
    xy = TMath::Sqrt(x*x + y*y);
    if(xy <= pico_rad - cone_rad){
        eff = 1.;
    }
    else if(xy > pico_rad + cone_rad){
        eff = 0;
    }
    else{
        rxy = xy/dstep;
        irxy = rxy + 1;
        if (irxy < 1 || irxy > 1601) {
            eff = 0;
        }
        else {eff = rf_c[irxy - 1];
        }
    }
    

    m_n_pe = mean_number_pe * eff; //number of pe
    max_fit = m_n_pe + 5*TMath::Sqrt(m_n_pe) + 1;
    
    if(max_fit > 50) max_fit = 50.;
    for (int i = 0; i <= max_fit; i++) {
        a1 = polya(q, i, polya_m, polya_s);
        a2 = dpoi(i, m_n_pe, fc);
        q_convol = q_convol + a1 * a2;
    }
    if (q_convol <= 0 ) {
        q_convol = TMath::Power(10, -10);
    }
    return q_convol;
}


void predict(double nbins, double bmin, double bmax, int ndata){
    
    double bstep = (bmax - bmin)/nbins, x, y, q, radi;
    double rm0 = param_1, dx = param_2, dy = param_3;
    
    TH1F *radi_hist = new TH1F("radi_hist", "radi_hist",300,0,15);

    
    
    for (int i = 0; i <= ndata; i++) {
        q = qep[i];
        x = ximp[i] - dx;
        y = yimp[i] - dy;
        radi = TMath::Sqrt(x*x + y*y);
        
    }
    
    
  
}


double example(double a, double x, double y, double dstep, double mean_number_pe, double rf[1600]){
    
    double re = 0;
    x  = x + polya_m + 1;
    re  = re +a;
    
    return re;
    
}

