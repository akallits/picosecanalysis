struct MCP {
        double v;
        double q;
        double t;
   };
   
//cut voltage overflow and negative charges
void overflow_cut(vector<MCP> &v, double over_v, double under_q=0.) {
    v.erase(remove_if(v.begin(), v.end(), [over_v, under_q]( auto& i ) {return (i.v>over_v || i.q<=under_q); }), v.end());
}

using u64 = uint64_t;

//########################## MAIN ##############################################

void mcp_slew(){

  //read file and retrieve the tree
  TFile *rootfile = new TFile("./run53_tracks_cut.root","read");
  Tree *data_tree = (TTree*) rootfile->Get("Pico");
  Int_t treentries = data_tree->GetEntries();
  cout <<"num of initial entries/ events: "<<treentries <<endl;
  data_tree->Print();
  
     //      1----> keeps mean of each fit  &&&&&&&&&&&    2----> keeps sigma of each fit
   TGraphAsymmErrors *slewing1  = new TGraphAsymmErrors();
   slewing1->SetMarkerColor(1);
   slewing1->SetMarkerStyle(20);
   slewing1->GetXaxis()->SetTitle("(fitted) charge (pC)");
   slewing1->GetYaxis()->SetTitle("slewing (ps)");

   
   TGraphAsymmErrors *slewing2  = new TGraphAsymmErrors();
   slewing2->SetTitle(" ");
   slewing2->SetMarkerColor(1);
   slewing2->SetMarkerStyle(20);
   slewing2->GetXaxis()->SetTitle("(fitted) charge (pC)");
   slewing2->GetYaxis()->SetTitle("resolution");
  
/********************************************
h 1h methodos h klassiki
*********************************************/
clock_t start, end;
     start = clock();


  MCP mcp1, mcp2;  //MCP 2 is the reference MCP
  vector<MCP> vec_mcp1; 
  double t1, t2, t3;
  double x, y;
  double chi2;
  
  data_tree->SetBranchAddress("mcp1_gpeak",&mcp1.v);
  data_tree->SetBranchAddress("mcp2_gpeak",&mcp2.v);

  data_tree->SetBranchAddress("mcp1_qall",&mcp1.q);
  data_tree->SetBranchAddress("mcp2_qall",&mcp2.q);

  data_tree->SetBranchAddress("mcp1_t", &t1); 
  data_tree->SetBranchAddress("mcp2_t",&t2);

  data_tree->SetBranchAddress("x",&x);
  data_tree->SetBranchAddress("y",&y);
  data_tree->SetBranchAddress("chi2",&chi2);
  
  
  for (int i=0;i<treentries;i++) { 
       data_tree->GetEntry(i); 
       
       //define only "global" cuts
       bool chi2_cut = chi2<2;
       bool xy_cut = (9<x && x<27) && (14<y && y<32);
       bool mcp2_cut = mcp2.v<3.7 && mcp2.q>21.;

       if(chi2_cut && xy_cut && mcp2_cut){
          mcp1.t = (t1-t2)*1000;

          vec_mcp1.push_back(mcp1);
          
       }
       
  }
  

overflow_cut(vec_mcp1, 3.7,3); // cut overflow of 	MCP1: v<3.7 AND q1>3 (apo kei fit gia kaliteri statistiki)

cout <<"data after all the cuts:  "<< vec_mcp1.size() <<endl;
 
 
// first RE-arrange the elements according to charge
   //  gia na xoume apo mikrotero se megalytero
   std::sort(vec_mcp1.begin(), vec_mcp1.end(), 
               [](const auto& i, const auto& j) { return i.q < j.q; } );
       
   
   
   TH1F *qbin_hist = new TH1F("qbin_hist"," ", 70, -460, -360 );
   TF1 *simple_gaus = new TF1 ("simple_gaus","gaus");
   auto* c1 = new TCanvas("c1", "", 500, 500); 
   gStyle->SetOptFit(1111);
 
   TRandom3 *random = new TRandom3();
   int ipoint =0; 
   
   for(u64 i=0; i<vec_mcp1.size(); ++i) {
      u64 i0 = i;
      double mean_q = 0;vec_mcp1;
      
      qbin_hist ->Reset(); 
      
      //evlepa ston kodika tou tzam oti ta bins den itan panta just 300 alla oti epaizan ekei gyro...
      //den kserw an iparxei kapoia pio poliploki skepsi apo ekei pisw alla egw evala apla mia uniform xaxax
      u64 delta_i = random->Uniform(290,320);
      
      while(i<i0+delta_i){
      
           mean_q += vec_mcp1[i].q;
           qbin_hist->Fill(vec_mcp1[i].t);        
           
           if (i==vec_mcp1.size()-1) break;
           ++i;
           }
      mean_q /=(i-i0);     
            
      TFitResultPtr r = qbin_hist->Fit(simple_gaus, "S"); //Q for quiet not showing the fits 
      
      
      c1->cd();
      qbin_hist->Draw();
      
      //gPad->Modified(); gPad->Update();
      //gSystem->ProcessEvents();
      //cin.ignore();      
      
            
      slewing1->SetPoint(ipoint, mean_q, r->Parameter(1));
      slewing2->SetPoint(ipoint, mean_q, r->Parameter(2));
      // error-x-low && error-x-high && error-y-low && error-y-high
      slewing1->SetPointError(ipoint, abs(mean_q- vec_mcp1.at(i0).q), abs(vec_mcp1.at(i-1).q -mean_q), r->ParError(1),r->ParError(1));
      slewing2->SetPointError(ipoint, abs(mean_q- vec_mcp1.at(i0).q), abs(vec_mcp1.at(i-1).q -mean_q), r->ParError(2),r->ParError(2));
      ++ipoint ;
    }

end = clock();
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
    cout << "1st method: sort a vector --> running time : " << fixed 
         << time_taken*1e03 << setprecision(5);
    cout << " msec " << endl;

slewing1->SetTitle(" 1st method: sort a vector");    
auto* c2 = new TCanvas("c2", "", 1000, 700);
     c2->Divide(1,2);
     c2->cd(1);
    slewing1->Draw("ap");
    c2->cd(2);
    slewing2->Draw("ap");

gSystem->ProcessEvents();
    cout <<endl<< "PRESS ENTER TO CONTINUE" <<endl;
    cin.ignore();    

/********************************************
kai h 2h methodos pou sou lega
*********************************************/
      
 //empty the graphs
  /*    for(int i=0;i<slewing1->GetN();i++){
          slewing1->RemovePoint(i);
          slewing2->RemovePoint(i);
      }  
      
   start = clock();
     
  //define histograms
  int nBins = 100;    
 TH2D *deltaT_q1 = new TH2D("deltaT_q1" , "Slewing for MCP1",nBins,0.,35.,nBins,-460, -360); 
 float dq = 35./nBins;
   
 //define cuts (to xeirizomai aneksartita gia auto ta ksanaorizw, oxi aparaitito)
  vector<TCut> v_overflow(3);
  for (int i = 0; i < 3; ++i) {
      v_overflow[i] = Form("0.<mcp%d_gpeak<3.7 && mcp%d_qall>0.",i+1,i+1);
  }

  
  TCut chi2_cutt = "chi2<2";
  TCut xy_cutt = "(9<x && x<27) && (14<y && y<32)";
  
 data_tree->Draw("((mcp1_t-mcp2_t)*1000):mcp1_qall >>deltaT_q1",(v_overflow[0] && v_overflow[1]) && chi2_cutt && xy_cutt &&  "mcp2_qall>21","goff");  



/*************************************************
interesting method--> very very fast but not able to define each q_bin consisted of ~300 events
we can only set a cut and a specific number of bins to project each time..
************************************************     
      deltaT_q1->FitSlicesY(0,0,-1,50,"G3 S");
      canvas->Clear();
      canvas->Divide(1,2);
      
      canvas->cd(1);
      TH1D* mean = (TH1D*)gDirectory->Get(Form("deltaT_q%d_1",1)); 
      mean->SetMaximum(-340);
      mean->Draw();
      
      canvas->cd(2);
      TH1D* sigma = (TH1D*)gDirectory->Get(Form("deltaT_q%d_2",1)); 
      sigma->Draw();
      
      gPad->Modified(); gPad->Update();
      cin.ignore();
*/
/*
     TH1D *proj_hist = new TH1D("proj_hist","projection histogram of q-bins",70,0,0);   
   
      ipoint=0;
      int deltabin=1;
      for(int qBin=9;qBin<nBins; qBin=qBin+deltabin){
         deltabin=0;
         int nentries = 0;
         while(nentries<280){
              proj_hist->Reset();
              proj_hist->GetXaxis()->SetLimits(0,0);
         
              proj_hist =(TH1D*) deltaT_q1->ProjectionY("_py",qBin,qBin+deltabin);
              nentries = proj_hist->GetEntries();
              if(qBin+deltabin==nBins){
                  ++deltabin;
                  break;
              }
              ++deltabin;
         }  
         TFitResultPtr r = proj_hist->Fit("gaus", "QS");//Q for quiet
        
         c1->cd();
         proj_hist->Draw();
         
        
         //gPad->Modified(); gPad->Update();
        // cin.ignore();
                
         double mean_q = 0;
         for(int i=qBin;i<qBin+deltabin; ++i){
             double weight = 0;
             for(int j=1;j<nBins+1;j++){
                 weight += (deltaT_q1->GetBinContent(i,j))/nentries;
             }
             mean_q += weight*(i-1/2)*dq;
         }
        
         double error_low = abs(mean_q-(qBin-1)*dq);
         double error_high = abs((qBin+deltabin-1)*dq-mean_q);
                
         slewing1->SetPoint(ipoint, mean_q, r->Parameter(1));
         slewing2->SetPoint(ipoint, mean_q, r->Parameter(2));
         // error-x-low && error-x-high && error-y-low && error-y-high
         slewing1->SetPointError(ipoint, error_low, error_high, r->ParError(1),r->ParError(1));
         slewing2->SetPointError(ipoint, error_low, error_high, r->ParError(2),r->ParError(2));
         ipoint++; 
    } 
 
  
  end = clock();
    time_taken = double(end - start) / double(CLOCKS_PER_SEC);
    cout << "2nd method: projection of a 2D hist --> running time : " << fixed 
         << time_taken*1e03 << setprecision(5);
    cout << " msec " << endl;
    
  slewing1->SetTitle(" 2nd method: projection of a 2D hist"); 
  auto* c3 = new TCanvas("c3", "", 1000, 700);   
  c3->Divide(1,2);
     c3->cd(1);
    slewing1->Draw("ap");
    c3->cd(2);
    slewing2->Draw("ap");
    
    
    gSystem->ProcessEvents();
    cout <<endl<< "PRESS ENTER TO FINISH THE PROGRAM" <<endl;
    cin.ignore();  
    */
    
    c1->Close();
    //c2->Close();
    //c3->Close();
    rootfile->Delete();
}
