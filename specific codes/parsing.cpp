#include "include/channels.hpp"
#include "include/header.hpp"
#include <cstdio>

#include <cstring>
#include <vector>

#define SV_IMPLEMENTATION
#include "include/sv.hpp"

#include <ostream>
#include <iostream>

void parse_trigtime_array_le(std::vector<f64>& TriggerTime,
                             std::vector<f64>& TriggerOffset,
                             string_view& Sview, const wavedesc* Header) {
  TriggerTime.reserve(Header->subarray_count);
  TriggerOffset.reserve(Header->subarray_count);

  constexpr auto iStep = sizeof(f64);
  for (i32 SubArrayIndex = 0; SubArrayIndex < Header->subarray_count;
       ++SubArrayIndex) {
    f64 val0 = {};
    f64 val1 = {};

    memcpy(&val0, Sview.data, sizeof(f64));
    Sview.data += iStep;
    Sview.size -= iStep;

    memcpy(&val1, Sview.data, sizeof(f64));
    Sview.data += iStep;
    Sview.size -= iStep;

    TriggerTime.push_back(val0);
    TriggerOffset.push_back(val1);
     //fprintf(stdout, "val0=%2.10f, val1=%e\n", val0, val1);

  }
   //fprintf(stdout, "############\n");
}

void parse_data_array1_le(std::vector<i32>& wave, string_view& Sview,
                          const wavedesc* Header) {
  wave.reserve(Header->wave_array_count);

  const auto iStep = Header->comm_type1 + 1;
  for (i32 SubArrayIndex = 0; SubArrayIndex < Header->wave_array_count;
       ++SubArrayIndex) {
    i16 val0;

    memcpy(&val0, Sview.data, iStep);
    Sview.data += iStep;
    Sview.size -= iStep;

     //fprintf(stdout, "val0=%" PRIi16 "\n", val0); 
    wave.push_back(val0);
  }
   //fprintf(stdout, "############\n");
}

void parse_trigtime_array_be(std::vector<f64>& TriggerTime,
                             std::vector<f64>& TriggerOffset,
                             string_view& Sview, const wavedesc* Header) {
  TriggerTime.reserve(Header->subarray_count);
  TriggerOffset.reserve(Header->subarray_count);

  constexpr auto iStep = sizeof(f64);
  u8 Buffer[iStep]     = {};
  for (i32 SubArrayIndex = 0; SubArrayIndex < Header->subarray_count;
       ++SubArrayIndex) {
    f64 val0 = {};
    f64 val1 = {};

    for (u32 i = 0; i < iStep; ++i) {
      Buffer[i] = Sview.data[iStep - i - 1];
    }
    memcpy(&val0, Buffer, sizeof(f64));
    Sview.data += iStep;
    Sview.size -= iStep;

    for (u32 i = 0; i < iStep; ++i) {
      Buffer[i] = Sview.data[iStep - i - 1];
    }
    memcpy(&val1, Buffer, sizeof(f64));
    Sview.data += iStep;
    Sview.size -= iStep;

    TriggerTime.push_back(val0);
    TriggerOffset.push_back(val1);
    // fprintf(stdout, "val0=%2.10f, val1=%e\n", val0, val1);
  }
  fprintf(stdout, "############\n");
}

void parse_data_array1_be(std::vector<i32>& wave, string_view& Sview,
                          const wavedesc* Header) {
  wave.reserve(Header->wave_array_count);
  const u8 nByte = Header->comm_type1 + 1;
  u8* Buffer     = (u8*)malloc(nByte);

  for (i32 SubArrayIndex = 0; SubArrayIndex < Header->wave_array_count;
       ++SubArrayIndex) {
    i64 val;

    for (u32 i = 0; i < nByte; ++i) {
      Buffer[i] = Sview.data[nByte - i - 1];
    }
    memcpy(&val, Buffer, sizeof(i8));
    Sview.data += nByte;
    Sview.size -= nByte;

    wave.push_back(val);
  }
  free(Buffer);
  // fprintf(stdout, "############\n");
}

auto parse_header(const c8* Filename, wavedesc& Header, string_view& Sview) {
  auto FileContent = read_file_as_string_view(Filename);

  constexpr u64 len = 8;
  string_view Desc  = {FileContent.data, len};

  u64 StartIndex = 0;
  while (strncmp(Desc.data, "WAVEDESC", len) != 0) {
    ++Desc.data;
    --Desc.size;
    ++StartIndex;
  }
  Sview = {FileContent.data + StartIndex, FileContent.size - StartIndex};

  void* temp = (void*)(Sview.data);
  Header     = *(wavedesc*)(temp);
  Sview.data += sizeof(wavedesc);
  Sview.size -= sizeof(wavedesc);

   //dump_header(stdout, &Header);
   //fprintf(stdout, "############\n");
   
}

auto create_channel(const c8* Filename) -> channel {
  channel Channel;
  
  string_view Sview;
  parse_header(Filename, Channel.Header, Sview);

  if (Channel.Header.subarray_count > 1) {
    if (Channel.Header.comm_order1 == 1) {
      parse_trigtime_array_le(Channel.TriggerTime, Channel.TriggerOffset, Sview,
                              &Channel.Header);
    } else {
      parse_trigtime_array_be(Channel.TriggerTime, Channel.TriggerOffset, Sview,
                              &Channel.Header);
    }
  }

  Sview.data += Channel.Header.ris_time_array;
  Sview.size -= Channel.Header.ris_time_array;

  if (Channel.Header.wave_array_count > 1) {
    if (Channel.Header.comm_order1 == 1) {
      parse_data_array1_le(Channel.Wave, Sview, &Channel.Header);
    } else {
      parse_data_array1_be(Channel.Wave, Sview, &Channel.Header);
    }
  }

  Sview.data += Channel.Header.wave_array_2;
  Sview.size -= Channel.Header.wave_array_2;

  return Channel;
}

#include <TFile.h>
#include <TTree.h>

auto get_next_event(const channel& Channel, i32 EventNumber)
    -> std::pair<std::vector<f64>, std::vector<f64>> {
  i32 waveform_size =
      Channel.Header.wave_array_count / Channel.Header.subarray_count;
     
  std::vector<f64> X, Y;
  X.reserve(waveform_size);
  Y.reserve(waveform_size);
  for (i32 i = 0; i < waveform_size; ++i) {
    X.push_back(i * Channel.Header.horizontal_interval +
                Channel.TriggerOffset[EventNumber]);
    Y.push_back(-(Channel.Wave[i + EventNumber * waveform_size] *
                    Channel.Header.vertical_gain -
                Channel.Header.vertical_offset));
  }
  // Channel.WaveformX = X;
  // Channel.WaveformY = Y;

  return {X, Y};
}

void trc_to_root(const std::vector<channel>& Channels, i64 nEvents = 1) {
  TFile OutFile("trc.root", "RECREATE");
  c8 TmpBuffer[256] = {};
  for (std::size_t iChannel = 0; iChannel < Channels.size(); ++iChannel) {
    snprintf(TmpBuffer, 256, "channel%zu", iChannel);
    auto OutTree = std::make_shared<TTree>(TmpBuffer, "");
    std::vector<f64> WaveformX;
    std::vector<f64> WaveformY;
    OutTree->Branch("x", &WaveformX);
    OutTree->Branch("y", &WaveformY);
    for (i64 iEvent = 0; iEvent < nEvents; ++iEvent) {
      auto XY   = get_next_event(Channels[iChannel], iEvent);
      WaveformX = XY.first;
      WaveformY = XY.second;

      OutTree->Fill();
    }
    OutTree->Write();
  }
  OutFile.Close();
  
  fprintf(stdout, "trc_to_root\n");
}


// -------------------------------------------------
//------------ MY FUNCTIONS DOKIMASTIKA-------------
#include "TH1.h"
#include "TF1.h"
#include <sys/stat.h>

bool exists ( const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}


auto find_max_pos(std::vector<f64>& wave_y_value) -> u64 {
    u64 i_max = distance(wave_y_value.begin(), max_element(wave_y_value.begin(), wave_y_value.end()));
    return i_max;
}

struct pdf { f64 mean; f64 RMS; };

pdf eliminate_noise(std::vector<f64>& wave_y_value) {
      TH1F *ch_noise_hist = new TH1F("ch_noise_hist","Noise level of channel", 80, 0, 0 );
      pdf noise;
      
      u64 i_noise = 1500; // 75 ns
      u64 binmax = find_max_pos(wave_y_value);
      if (binmax<i_noise) i_noise = 2*binmax/3 +1;
      
      for (u64 i = 0; i < i_noise; ++i) {
    ch_noise_hist->Fill(wave_y_value.at(i));
  }
    noise.mean = ch_noise_hist->GetMean();
    noise.RMS = ch_noise_hist->GetRMS();
    
    //eliminate leme twra noise
    for (u64 i = 0; i < wave_y_value.size() ; ++i) {
    wave_y_value.at(i) -=noise.mean;
  }
  delete ch_noise_hist;
  return noise;
}

auto find_pulse_start_pos(std::vector<f64>& wave_y_value, u64 i_max, f64 noise_RMS) -> u64 {
    u64 i_start = 0 ;
    for (u64 i = i_max; i > 0 ; --i) {
        if(wave_y_value.at(i) < noise_RMS){
           i_start = i;
           break;
          }
    }
    return i_start;
}

auto slope_at_x(std::vector<f64>& wave_x_value, std::vector<f64>& wave_y_value, u64 i1, u64 i2, f64 y, u64& pass) -> f64 {

     //to approximate linear in narrower intervals if possible
    for (u64 i = i2; i > i1-1 ; --i) {
        if(wave_y_value.at(i) < y ){
           i1 = i;
           i2 = i+1;
           break;
          }
    }
    f64 x1 = wave_x_value.at(i1);
    f64 x2 = wave_x_value.at(i2);
    f64 y1 = wave_y_value.at(i1);
    f64 y2 = wave_y_value.at(i2);
    
    f64 slope = (y2-y1)/(x2-x1);
    
   pass = i1; 
   return slope;
}

auto least_sq_for_ion_tail(std::vector<f64>& wave_x_value, std::vector<f64>& wave_y_value, u64 i_max, u64 i_end) -> u64 {
     if(i_end-i_max>60) i_end = i_max+60;
     u64 n=15;
     for(u64 i = i_max + 3; i < i_end; ++i)
        {
            double mean_x = 0, mean_y = 0;
            for(u64 k = 0; k < n; k++)
            {
                mean_y += wave_y_value.at(i+k);
                mean_x += wave_x_value.at(i+k);
            }
            mean_y /= n;
            mean_x /= n;

            double num = 0, den = 0;
            for(u64 k = 0; k < n; k++)
            {
                num += (wave_x_value.at(i+k) - mean_x)*(wave_y_value.at(i+k) - mean_y);
                den += (wave_x_value.at(i+k) - mean_x)*(wave_x_value.at(i+k) - mean_x);
            }

            double slope = num/den;
            if ( abs(slope) < 0.001 ) 
            {
                i_end = i;
                break;
            }
        }
   return i_end;
}

auto Xpoint_of_linear_interpolation(std::vector<f64>& wave_x_value, std::vector<f64>& wave_y_value, u64 i1, u64 i2, f64 y) -> f64 {
     u64 pass = i1;     
     f64 slope = slope_at_x(wave_x_value, wave_y_value, i1, i2, y, pass);
     
    f64 x1 = wave_x_value.at(pass);
    f64 y1 = wave_y_value.at(pass);
    
    f64 x = (y-y1)/slope + x1;
   return x;
}

Double_t leading_edge_function(Double_t *x, Double_t *par){
    return par[0]/(1+exp(-par[2]*(x[0]-par[1])))+par[3];
}
Double_t impulse_function(Double_t *x, Double_t *par){
    return leading_edge_function(x,par) + leading_edge_function(x,&par[4]);
}
//--------------------------------------------------------------------------------

struct peak_sat {
        f64 v ;// voltage --> peak
        f64 q; //charge
        f64 sat ;
        f64 mcp ;
   };
  

Double_t slew_correction_function(Double_t *x, Double_t *par){
    return par[0]/pow(x[0],par[1])+par[2];
}








