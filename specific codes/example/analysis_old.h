//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct  6 18:06:36 2021 by ROOT version 6.12/06
// from TTree Pico/Analysis Output
// found on file: trial.root
//////////////////////////////////////////////////////////

#ifndef analysis_h
#define analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           Baseline_Window;
   Double_t        MCP1_baseline_level;
   Double_t        MCP1_baseline_rms;
   Double_t        MCP1_global_maximum_y;
   Double_t        MCP1_global_maximum_x;
   Double_t        MCP1_start_x;
   Double_t        MCP1_e_peak_end_x;
   Double_t        MCP1_e_charge;
   Double_t        MCP1_all_charge;
   Double_t        MCP1_naive_time;
   Double_t        MCP1_naive_x;
   Double_t        MM1_baseline_level;
   Double_t        MM1_baseline_rms;
   Double_t        MM1_global_maximum_y;
   Double_t        MM1_global_maximum_x;
   Double_t        MM1_start_x;
   Double_t        MM1_e_peak_end_x;
   Double_t        MM1_e_charge;
   Double_t        MM1_all_charge;
   Double_t        MM1_naive_time;
   Double_t        MM1_naive_x;
   Double_t        MM1_sigmoid_parameters[4];
   Double_t        MM1_sigmoid_chi_square;
   Bool_t          MM1_sigmoid_failed;
   Double_t        MM1_sigmoid_timepoint;
   Double_t        MM1_Fullsigmoid_charge;
   Double_t        MM1_Fullsigmoid_chi2;
   Double_t        MM1_Fullsigmoid_parameters[7];
   Int_t           NumberofTracks;
   vector<double>  *POS0_track_hit;
   Double_t        SRSnumber;
   Double_t        TrackChi2;
   Double_t        SlopeXZ;
   Double_t        SlopeYZ;

   // List of branches
   TBranch        *b_Baseline_Window;   //!
   TBranch        *b_MCP1_baseline_level;   //!
   TBranch        *b_MCP1_baseline_rms;   //!
   TBranch        *b_MCP1_global_maximum_y;   //!
   TBranch        *b_MCP1_global_maximum_x;   //!
   TBranch        *b_MCP1_start_x;   //!
   TBranch        *b_MCP1_e_peak_end_x;   //!
   TBranch        *b_MCP1_e_charge;   //!
   TBranch        *b_MCP1_all_charge;   //!
   TBranch        *b_MCP1_naive_time;   //!
   TBranch        *b_MCP1_naive_x;   //!
   TBranch        *b_MM1_baseline_level;   //!
   TBranch        *b_MM1_baseline_rms;   //!
   TBranch        *b_MM1_global_maximum_y;   //!
   TBranch        *b_MM1_global_maximum_x;   //!
   TBranch        *b_MM1_start_x;   //!
   TBranch        *b_MM1_e_peak_end_x;   //!
   TBranch        *b_MM1_e_charge;   //!
   TBranch        *b_MM1_all_charge;   //!
   TBranch        *b_MM1_naive_time;   //!
   TBranch        *b_MM1_naive_x;   //!
   TBranch        *b_MM1_sigmoid_parameters;   //!
   TBranch        *b_MM1_sigmoid_chi_square;   //!
   TBranch        *b_MM1_sigmoid_failed;   //!
   TBranch        *b_MM1_sigmoid_timepoint;   //!
   TBranch        *b_MM1_Fullsigmoid_charge;   //!
   TBranch        *b_MM1_Fullsigmoid_chi2;   //!
   TBranch        *b_MM1_Fullsigmoid_parameters;   //!
   TBranch        *b_NumberofTracks;   //!
   TBranch        *b_POS0_track_hit;   //!
   TBranch        *b_SRSnumber;   //!
   TBranch        *b_TrackChi2;   //!
   TBranch        *b_SlopeXZ;   //!
   TBranch        *b_SlopeYZ;   //!

   analysis(TTree *tree=0);
   virtual ~analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef analysis_cxx
analysis::analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
//      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("trial.root");
//       TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("trial181.root");
       //TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Run228_results.root");
       TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Run130_results.root");

      if (!f || !f->IsOpen()) {
//         f = new TFile("trial181.root");
          f = new TFile("export.root");

      }
      f->GetObject("Pico",tree);

   }
   Init(tree);
}

analysis::~analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   POS0_track_hit = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Baseline_Window", &Baseline_Window, &b_Baseline_Window);
   fChain->SetBranchAddress("MCP1_baseline_level", &MCP1_baseline_level, &b_MCP1_baseline_level);
   fChain->SetBranchAddress("MCP1_baseline_rms", &MCP1_baseline_rms, &b_MCP1_baseline_rms);
   fChain->SetBranchAddress("MCP1_global_maximum_y", &MCP1_global_maximum_y, &b_MCP1_global_maximum_y);
   fChain->SetBranchAddress("MCP1_global_maximum_x", &MCP1_global_maximum_x, &b_MCP1_global_maximum_x);
   fChain->SetBranchAddress("MCP1_start_x", &MCP1_start_x, &b_MCP1_start_x);
   fChain->SetBranchAddress("MCP1_e_peak_end_x", &MCP1_e_peak_end_x, &b_MCP1_e_peak_end_x);
   fChain->SetBranchAddress("MCP1_e_charge", &MCP1_e_charge, &b_MCP1_e_charge);
   fChain->SetBranchAddress("MCP1_all_charge", &MCP1_all_charge, &b_MCP1_all_charge);
   fChain->SetBranchAddress("MCP1_naive_time", &MCP1_naive_time, &b_MCP1_naive_time);
   fChain->SetBranchAddress("MCP1_naive_x", &MCP1_naive_x, &b_MCP1_naive_x);
   fChain->SetBranchAddress("MM1_baseline_level", &MM1_baseline_level, &b_MM1_baseline_level);
   fChain->SetBranchAddress("MM1_baseline_rms", &MM1_baseline_rms, &b_MM1_baseline_rms);
   fChain->SetBranchAddress("MM1_global_maximum_y", &MM1_global_maximum_y, &b_MM1_global_maximum_y);
   fChain->SetBranchAddress("MM1_global_maximum_x", &MM1_global_maximum_x, &b_MM1_global_maximum_x);
   fChain->SetBranchAddress("MM1_start_x", &MM1_start_x, &b_MM1_start_x);
   fChain->SetBranchAddress("MM1_e_peak_end_x", &MM1_e_peak_end_x, &b_MM1_e_peak_end_x);
   fChain->SetBranchAddress("MM1_e_charge", &MM1_e_charge, &b_MM1_e_charge);
   fChain->SetBranchAddress("MM1_all_charge", &MM1_all_charge, &b_MM1_all_charge);
   fChain->SetBranchAddress("MM1_naive_time", &MM1_naive_time, &b_MM1_naive_time);
   fChain->SetBranchAddress("MM1_naive_x", &MM1_naive_x, &b_MM1_naive_x);
   fChain->SetBranchAddress("MM1_sigmoid_parameters", MM1_sigmoid_parameters, &b_MM1_sigmoid_parameters);
   fChain->SetBranchAddress("MM1_sigmoid_chi_square", &MM1_sigmoid_chi_square, &b_MM1_sigmoid_chi_square);
   fChain->SetBranchAddress("MM1_sigmoid_failed", &MM1_sigmoid_failed, &b_MM1_sigmoid_failed);
   fChain->SetBranchAddress("MM1_sigmoid_timepoint", &MM1_sigmoid_timepoint, &b_MM1_sigmoid_timepoint);
   fChain->SetBranchAddress("MM1_Fullsigmoid_charge", &MM1_Fullsigmoid_charge, &b_MM1_Fullsigmoid_charge);
   fChain->SetBranchAddress("MM1_Fullsigmoid_chi2", &MM1_Fullsigmoid_chi2, &b_MM1_Fullsigmoid_chi2);
   fChain->SetBranchAddress("MM1_Fullsigmoid_parameters", MM1_Fullsigmoid_parameters, &b_MM1_Fullsigmoid_parameters);
   fChain->SetBranchAddress("NumberofTracks", &NumberofTracks, &b_NumberofTracks);
   fChain->SetBranchAddress("POS0_track_hit", &POS0_track_hit, &b_POS0_track_hit);
   fChain->SetBranchAddress("SRSnumber", &SRSnumber, &b_SRSnumber);
   fChain->SetBranchAddress("TrackChi2", &TrackChi2, &b_TrackChi2);
   fChain->SetBranchAddress("SlopeXZ", &SlopeXZ, &b_SlopeXZ);
   fChain->SetBranchAddress("SlopeYZ", &SlopeYZ, &b_SlopeYZ);
   Notify();
}

Bool_t analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef analysis_cxx
