#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <fstream>
using namespace std;

void dumpTreeTotxt(){
  TFile *f=new TFile("Run130_results.root"); // opens the root file
  TTree *tr=(TTree*)f->Get("Pico"); // creates the TTree object
  //tr->Scan(); // prints the content on the screen

  int Baseline_Window, NumberofTracks;
  bool MM1_sigmoid_failed;
  double MCP1_baseline_level; // create variables of the same type as the branches you want to access
  double MCP1_baseline_rms, MCP1_global_maximum_y, MCP1_global_maximum_x;
  double MCP1_start_x,MCP1_e_peak_end_x,MCP1_e_charge,MCP1_all_charge,MCP1_naive_time;
  double MCP1_naive_x,MCP1_sigmoid_timepoint,MM1_baseline_level,MM1_baseline_rms;
  double MM1_global_maximum_y,MM1_global_maximum_x,MM1_start_x,MM1_e_peak_end_x;
  double MM1_e_charge,MM1_all_charge,MM1_naive_time,MM1_naive_x;
  double MM1_sigmoid_parameters,MM1_sigmoid_chi_square;
  double MM1_sigmoid_timepoint, MM1_Fullsigmoid_charge,MM1_Fullsigmoid_chi2;
  double MM1_Fullsigmoid_fail,MM1_Fullsigmoid_parameters;
  double SRSnumber, TrackChi2,SlopeXZ,SlopeYZ;

  // vector<double>  *POS0_track_hit;

   tr->SetBranchAddress("Baseline_Window", &Baseline_Window);
   tr->SetBranchAddress("MCP1_baseline_level", &MCP1_baseline_level);
   tr->SetBranchAddress("MCP1_baseline_rms", &MCP1_baseline_rms);
   tr->SetBranchAddress("MCP1_global_maximum_y", &MCP1_global_maximum_y);
   tr->SetBranchAddress("MCP1_global_maximum_x", &MCP1_global_maximum_x);
   tr->SetBranchAddress("MCP1_start_x", &MCP1_start_x);
   tr->SetBranchAddress("MCP1_e_peak_end_x", &MCP1_e_peak_end_x);
   tr->SetBranchAddress("MCP1_e_charge", &MCP1_e_charge);
   tr->SetBranchAddress("MCP1_all_charge", &MCP1_all_charge);
   tr->SetBranchAddress("MCP1_naive_time", &MCP1_naive_time);
   tr->SetBranchAddress("MCP1_naive_x", &MCP1_naive_x);
   tr->SetBranchAddress("MCP1_sigmoid_timepoint", &MCP1_sigmoid_timepoint);
   tr->SetBranchAddress("MM1_baseline_level", &MM1_baseline_level);
   tr->SetBranchAddress("MM1_baseline_rms", &MM1_baseline_rms);
   tr->SetBranchAddress("MM1_global_maximum_y", &MM1_global_maximum_y);
   tr->SetBranchAddress("MM1_global_maximum_x", &MM1_global_maximum_x);
   tr->SetBranchAddress("MM1_start_x", &MM1_start_x);
   tr->SetBranchAddress("MM1_e_peak_end_x", &MM1_e_peak_end_x );
   tr->SetBranchAddress("MM1_e_charge", &MM1_e_charge);
   tr->SetBranchAddress("MM1_all_charge", &MM1_all_charge);
   tr->SetBranchAddress("MM1_naive_time", &MM1_naive_time);
   tr->SetBranchAddress("MM1_naive_x", &MM1_naive_x);
   tr->SetBranchAddress("MM1_sigmoid_parameters", &MM1_sigmoid_parameters);
   tr->SetBranchAddress("MM1_sigmoid_chi_square", &MM1_sigmoid_chi_square);
   tr->SetBranchAddress("MM1_sigmoid_failed", &MM1_sigmoid_failed);
   tr->SetBranchAddress("MM1_sigmoid_timepoint", &MM1_sigmoid_timepoint);
   tr->SetBranchAddress("MM1_Fullsigmoid_charge", &MM1_Fullsigmoid_charge);
   tr->SetBranchAddress("MM1_Fullsigmoid_chi2", &MM1_Fullsigmoid_chi2);
   tr->SetBranchAddress("MM1_Fullsigmoid_fail", &MM1_Fullsigmoid_fail);
   tr->SetBranchAddress("MM1_Fullsigmoid_parameters", &MM1_Fullsigmoid_parameters);
   tr->SetBranchAddress("NumberofTracks", &NumberofTracks);
   //tr->SetBranchAddress("POS0_track_hit", &POS0_track_hit);
   tr->SetBranchAddress("SRSnumber", &SRSnumber);
   tr->SetBranchAddress("TrackChi2", &TrackChi2);
   tr->SetBranchAddress("SlopeXZ", &SlopeXZ);
   tr->SetBranchAddress("SlopeYZ", &SlopeYZ);

  ofstream myfile;
  myfile.open ("extract.txt");
  myfile << "MM1_sigmoid_timepoint MCP1_sigmoid_timepoint\n";

  for (int i=0;i<tr->GetEntries();i++){
    // loop over the tree
    tr->GetEntry(i);
    cout << MM1_sigmoid_timepoint << " " << MCP1_sigmoid_timepoint << endl; //print to the screen
    myfile << MM1_sigmoid_timepoint << " " << MCP1_sigmoid_timepoint <<"\n"; //write to file
  }
  myfile.close();
}