#define analysis_cxx
#include "analysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void analysis::Loop()
{
//   In a ROOT session, you can do:
//      root> .L analysis.C
//      root> analysis t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

    auto sat      = new TH1D("sat","SAT Distribution using naive timing; SAT (ns); Events", 200, -4, 4.);
    auto sat_fit  = new TH1D("sat_fit","SAT Distribution using Sigmoid Fit; SAT (ns); Events", 200, -5, 5.);

    auto sat2 = new TH1D("sat2","SAT Distribution using naive timing with gpeak MCP > 3mV;SAT (ns); Events", 200, -4, +4);
    auto sat2_cut = new TH1D("sat2_cut","SAT Distribution using naive timing with gpeak MM > 100mV;SAT (ns); Events", 200, 0.5, 0.9);

    auto sat2_fit  = new TH1D("sat2_fit","SAT Distribution using Sigmoid Fit with gpeak MCP > 3mV;SAT (ns); Events", 200, -5, 5);
    auto sat2_fit_cut  = new TH1D("sat2_fit_cut","SAT Distribution using Sigmoid Fit with gpeak MM > 100mV;SAT (ns); Events", 200, 0.5, 0.9);


    auto sat3 = new TH1D("sat3","SAT Distribution with MMpeak ampl > 5mV and MCPcharge >3pC;SAT (ns); Events", 100, 0.2, 1) ;
    auto sat3_fit  = new TH1D("sat3_fit","SAT Distribution using Sigmoid Fit with gpeak MM > 3mV and MCP Charge >3pC;SAT (ns); Events", 200, 0, 1);

    auto e_peak_MCP_cut = new TH1D("e_peak_MCP_cut", "Epeak Charge of MCP cutting the noise; Amplitude(V); Events", 100, 0, 0.22);
    auto e_peak_MM_cut1 = new TH1D("e_peak_MM_cut1", "Epeak Charge of MM cutting the noise; Amplitude(V); Events", 100, 0, 0.55);


    auto e_peak_MCP = new TH1D("e_peak_MCP", "Epeak Amplitude of MCP; Amplitude(V); Events", 100, 0, 0.225);
    auto e_charge_MCP = new TH1D("e_charge_MCP", "Epeak Charge of MCP; Charge(pC); Events", 100, 0, 4);

    auto e_charge_MM = new TH1D("e_charge_MM", "Epeak Charge of PICOSEC; Charge(pC); Events", 100, 0, 40);
    auto e_peak_MM = new TH1D("e_peak_MM", "Epeak Amplitude of PICOSEC; Amplitude(V); Events", 100, 0, 0.55);

    auto hscat = new TH2D("hscat","Beam Profile; X track(mm); Y track(mm)",40,50,100,40,10,40);
    auto hscat_MM = new TH2D("hscat_MM","hscat_MM",40,50,100,40,10,40);
    auto hscat_MCP = new TH2D("hscat_MCP","hscat_MCP",40,50,100,40,10,40);


   Long64_t nbytes = 0, nb = 0;
   int cnt=0;
   std::vector<double> Xtrk;
   std::vector<double> Ytrk;


   for (Long64_t jentry=0; jentry<nentries;jentry++) {

      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      

      for (int i = 0; i < 3; ++i){
         if(i==0)
         {
            Xtrk.push_back(POS0_track_hit->at(i));
            //cout <<"Xtracks= "<<POS0_track_hit->at(i)<<endl;

         }
         if (i==1)
         {
            Ytrk.push_back(POS0_track_hit->at(i));
            //cout <<"Ytracks= "<<POS0_track_hit->at(i)<<endl;
            
         }
       
      }

      hscat->Fill(Xtrk.at(jentry), Ytrk.at(jentry));
      //if (Cut(ientry) < 0) continue;
      
      //std::cout<<"Trees events: "<<jentry<< " nentries:  "<<nentries<<std::endl;
         
      sat->Fill(MCP1_naive_time - MM1_naive_time);
      sat_fit->Fill(MCP1_sigmoid_timepoint - MM1_sigmoid_timepoint) ;
       
       e_peak_MCP->Fill(MCP1_global_maximum_y);
       e_peak_MM->Fill(MM1_global_maximum_y);
       e_charge_MCP->Fill(MCP1_e_charge);
       e_charge_MM->Fill(MM1_e_charge);

        if(MCP1_global_maximum_y > 0.03 && MCP1_global_maximum_y < 0.215) 
        {
            e_peak_MCP_cut->Fill(MCP1_global_maximum_y);
            sat2->Fill(MCP1_naive_time - MM1_naive_time);
            sat2_fit->Fill(MCP1_sigmoid_timepoint - MM1_sigmoid_timepoint);
            hscat_MCP->Fill(Xtrk.at(jentry), Ytrk.at(jentry));

        }

        if(MM1_global_maximum_y > 0.1 && MM1_global_maximum_y < 0.5) 
        {
            e_peak_MM_cut1->Fill(MM1_global_maximum_y);
            sat2_cut->Fill(MCP1_naive_time - MM1_naive_time);
            sat2_fit_cut->Fill(MCP1_sigmoid_timepoint - MM1_sigmoid_timepoint);

        }
       
       if(MM1_e_charge > 2 && MCP1_global_maximum_y > 0.03) 
        {
           sat3->Fill(MCP1_naive_time - MM1_naive_time);
           sat3_fit->Fill(MCP1_sigmoid_timepoint - MM1_sigmoid_timepoint); 
           hscat_MM->Fill(Xtrk.at(jentry), Ytrk.at(jentry));
        }
    
   //cout<<"jentry= "<<jentry<<endl;
      
   
   /* Time walk corrections
    * This macro fits_timing  with inputs (var t_type  o1 o2 bins) fits the 
    * time different "t_type"-tfit_3  in bins of the variable "var" (where o1 and o2 are the extreme values of var).
    * The bins in "var" are adjusted in order each bin to have "bins" events.  The results are written
    * out in the file RS_[t_type]_[var].txt*/
   }
    

    //Apply a Gaussian fit 
    TF1 *f1 = new TF1("f1","gaus",0.5,0.9);
    // set initial parameters (not really needed for gaus)
    f1->SetParameters(sat2_cut->GetMaximum(), sat2_cut->GetMean(), sat2_cut->GetRMS() );


    TF1 *f2 = new TF1("f2","gaus",0.5,0.9);
    // set initial parameters (not really needed for gaus)
    f2->SetParameters(sat2_fit_cut->GetMaximum(), sat2_fit_cut ->GetMean(), sat2_fit_cut->GetRMS() );

  
    auto c1 = new TCanvas("sat", "sat", 0, 0, 900, 900);
    c1->Divide(1,2);
    c1->cd(1); sat->Draw();
    c1->cd(2); sat_fit->Draw();

    auto c2 = new TCanvas("sat2", "sat2", 0, 0, 900, 900);
    c2->Divide(1,2);
    c2->cd(1); sat2->Draw();
    c2->cd(2); sat2_fit->Draw(); 

    auto c3 = new TCanvas("MCP_size", "MCP_size", 0, 0, 900, 900);
    e_peak_MM_cut1->Draw();

    auto c4 = new TCanvas("sat21", "sat21", 0, 0, 900, 900);
    c4->Divide(1,2);
    c4->cd(1); sat2_cut->Draw(); sat2_cut->Fit("f1"); sat2_cut->Fit("f1"); sat2_cut->Fit("f1"); sat2_cut->Draw();
    c4->cd(2); sat2_fit_cut->Draw(); sat2_fit_cut->Fit("f2"); sat2_fit_cut->Fit("f2"); sat2_fit_cut->Fit("f2"); sat2_fit_cut->Draw(); 

    auto c5 = new TCanvas("sat3", "sat3", 0, 0, 900, 900);
    c5->Divide(1,2);
    c5->cd(1); sat3->Draw();
    c5->cd(2); sat3_fit->Draw();

    auto c6 = new TCanvas("MCP","MCP", 0, 0, 900, 900);
    c6->Divide(1,2);
    c6->cd(1); e_peak_MCP->Draw(); c6->cd(1)->SetLogy();
    c6->cd(2); e_charge_MCP->Draw(); c6->cd(2)->SetLogy();
    
    auto c8 = new TCanvas("PICOSEC","PICOSEC", 0, 0, 900, 900);
    c8->Divide(1,2);
    c8->cd(1); e_peak_MM->Draw(); c8->cd(1)->SetLogy();
    c8->cd(2); e_charge_MM->Draw(); c8->cd(2)->SetLogy();

    /*TGraph *gr= new TGraph(Xtrk.size(),&Xtrk[0],&Ytrk[0]); 
    gr->SetTitle("Tracking Frame; X track(mm); Y track(mm)");
    gr->SetMarkerStyle(20);
    TCanvas *c9 = new TCanvas("tracking","tracking",1600,1000);
    gr->Draw("ap");*/

    TCanvas *c10 = new TCanvas("trk","trk",1600,1000);
    hscat->SetMarkerStyle(20);
    hscat->Draw("scat=0.5");
    hscat_MCP->SetMarkerColor(3);
    hscat_MCP->SetMarkerStyle(20);
    hscat_MCP->Draw("same");
    hscat_MM->SetMarkerColor(2); 
    hscat_MM->SetMarkerStyle(20);
    hscat_MM->Draw("same");

    
   
}