#include "include/channels.hpp"
#include "include/header.hpp"
#include "include/types.hpp"

#include "./parsing.cpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>

// basic file operations
#include <iostream>
#include <fstream>

//root includes
#include <TRint.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TGraph.h>
#include "TGraphAsymmErrors.h"
#include <TFitResult.h>
#include "TH1.h"
#include "TF1.h"
#include <TSystem.h>
#include <TStyle.h>
#include <TPaveText.h>
#include "TLatex.h"
//#include "TRandom.h"

// for math functions
#include <TMath.h>
#include <math.h>

#include <bits/stdc++.h>
using namespace std;

//------------1---------------------

void overflow_cut(vector<peak_sat> &v, f64 over_mcp, f64 over_v) {
    v.erase(remove_if(v.begin(), v.end(), [over_mcp, over_v]( auto& i ) {return (i.mcp>over_mcp || i.v>over_v); }), v.end());
}

void erase_sat(vector<peak_sat> &v, f64 mean, f64 rms) {
    v.erase(remove_if(v.begin(), v.end(), [mean, rms]( auto& i ) {return abs(i.sat-mean) > 10*rms; }), v.end());
}   

//-------------2--------------------
bool reject;
f64 mean = 0;       
f64 rms = 0;
 double fline(double *x, double *param)
{
    if (reject && x[0] > (mean -rms) && x[0] < (mean + rms)) {
      TF1::RejectPoint();
      return 0;
   }
   return param[0]*exp(-0.5*pow( ((x[0]-param[1])/param[2]),2 ) );
}
//-------------------------------

int main() {

   i32 pad_num =31;
   char filename_format[] = "info_pad_%d.txt";
   char filename[sizeof(filename_format)+2]; // up to 3 %d number digits
   snprintf(filename, sizeof(filename),filename_format, pad_num); // must !!!
   ifstream info_file;
   info_file.open(filename, ios:: in);
   
   
   if (!info_file) {
    fprintf(stderr, "ERROR in file reading: %s !! Check if exists!! \n",filename);
    exit(1);
  }

/*
   // new lines will be skipped unless we stop it from happening:    
    info_file.unsetf(std::ios_base::skipws);

    // count the newlines with an algorithm specialized for counting:
    u64 nLines = std::count(
        istream_iterator<char>(info_file),
        istream_iterator<char>(), 
        '\n');

    cout << "Lines: " << nLines << "\n";
    
   // re-read the file
   info_file.clear();
   info_file.seekg(0);
*/

   TRint App("app", nullptr, nullptr);
   auto* Canvas = new TCanvas("c1", "", 1000, 700);
      gStyle->SetOptFit(1);
      gStyle->SetOptStat(1);


    // skip the first line
   std::string str;
   getline(info_file, str); 
   cout << str <<endl;
   
   peak_sat vt; // stores peak of pico + fit charge + SAT_fit + MCP peak
   
   vector <peak_sat> vec_vt;
   
   f64 SAT;
   f64 p2; // or pl3
   
   while(!(info_file >> vt.mcp )==0)
     { 
         // reads a string (to avoid reads 0 and terminate)    
         info_file >> str;
         //string to double
         SAT = -std::stod(str);
         
         info_file >> vt.v; 
         
         info_file >> str;
         SAT = std::stod(str) +SAT;
         vt.sat = SAT;
         
         info_file >> p2;      
         info_file >> str;
         vt.q= std::stod(str);
                  
         if(vt.q>0){
           
           vec_vt.push_back(vt);
         }
         f64 dummy;
         for(u64 i=0; i<14; ++i)
         info_file >> dummy;
     } 
     
     //-----------------------arrange according to Epeak and dumb overflow pulses + "fake" ones------------------------
     clock_t start, end;
     start = clock();
     
     f64 over_mcp = 0.21;
     f64 over_v = 0.48;
     cout << "size of vector before cuts    " << vec_vt.size() << endl;
      
     // first arrange the elements according to pico amplitude
   std::sort(vec_vt.begin(), vec_vt.end(), 
               [](const auto& i, const auto& j) { return i.v < j.v; } );
               
     overflow_cut(vec_vt, over_mcp, over_v); // cut over
     
     for(u64 i=200; i<250; ++i) {
     mean = mean + vec_vt.rbegin()[i].sat/50;
    }
    
     for(u64 i=200; i<250; ++i) {
     rms = rms + sqrt((vec_vt.rbegin()[i].sat- mean)*(vec_vt.rbegin()[i].sat-mean)/49);
    }
    fprintf(stdout,"mean = %f +/- rms = %e\n",mean, rms);

    cout << "size of vector before cut of fake pulses  " << vec_vt.size() << endl;
    erase_sat(vec_vt, mean, rms);		 
    
    cout << "size of vector after cut of fake pulses  " << vec_vt.size() << endl;
    
    /*// if you want to display the vector
    for(u64 i=0; i<vec_vt.size(); ++i) {
     cout << vec_vt.at(i).v <<" "<< vec_vt.at(i).q <<" "<< vec_vt.at(i).sat << endl;
    }
    */
    end = clock();
    double time_taken = double(end - start) / double(CLOCKS_PER_SEC);
    cout << "Time taken by program is : " << fixed 
         << time_taken*1e03 << setprecision(5);
    cout << " msec " << endl;
    
     //----------------------------------------------------
     
   // Create file on which hists/graphs can be saved.
   TFile* outFile = new TFile("graph.root", "RECREATE");
   
   //i32 nBins =sqrt(vec_vt.size());
   i32 nBins = 100;

   // hist to store SAT  
   TH1F *SAT_total = new TH1F("SAT_total","Signal arrival time (all pulses)", nBins, 0,0);  
   SAT_total->GetXaxis()->SetTitle("time (ns)");
   SAT_total->GetYaxis()->SetTitle("number of events");
      
 
   // hist to store amplitudes
   TH1F *peak_total = new TH1F("peak_total","PICOSEC: e-peak amplitude (all pulses)", nBins, 0, 0.8);  
   peak_total->GetXaxis()->SetTitle("E-peak voltage (V)");
   peak_total->GetYaxis()->SetTitle("number of events"); 

   // hist to store charge
   TH1F *charge_total = new TH1F("charge_total","fitted charge multiplicity (all pulses)", nBins,0,30);
   charge_total->GetXaxis()->SetTitle("fitted charge (pC)");
   charge_total->GetYaxis()->SetTitle("number of events");
   
   // hist to store MCP amplitudes
   TH1F *peak_mcp = new TH1F("peak_total","MCP: e-peak amplitude (all pulses)", nBins, 0, 0);  
   peak_mcp->GetXaxis()->SetTitle("E-peak voltage (V)");
   peak_mcp->GetYaxis()->SetTitle("number of events"); 
   
  // create TGraph for charge-peak diagram
  TGraph *peak_charge  = new TGraph(vec_vt.size());
  peak_charge->SetName("peak_charge");
  peak_charge->SetTitle("Peak vs fitted Charge for all pulses");
  peak_charge->GetXaxis()->SetTitle("fitted Charge (pC)");
  peak_charge->GetYaxis()->SetTitle("Peak voltage (V)");
  peak_charge->SetMarkerColor(1);
  peak_charge->SetMarkerStyle(20);
  peak_charge->SetMarkerSize(0.5);
   
   
  // fill  
  for(u64 i=0; i<vec_vt.size(); ++i) {
     SAT_total->Fill( vec_vt.at(i).sat );
     charge_total-> Fill(vec_vt.at(i).q);
     peak_total->Fill(vec_vt.at(i).v);
     peak_mcp->Fill(vec_vt.at(i).mcp);
     peak_charge ->SetPoint(i, vec_vt.at(i).q, vec_vt.at(i).v);
    }   
     
  Canvas->Divide(2, 2);
  Canvas->cd(1);
  peak_mcp->Draw();
  Canvas->cd(2);
  charge_total->Draw();
  Canvas->cd(3);
  peak_total->Draw();
  Canvas->cd(4);
  peak_charge->Draw("ap");
  
  App.Run(kTRUE);
  
   //exit(1);
  //---------------------------------------------------------------------------------
  // we now attempt to get bins of charge or amplitude (every 500 events) so as to get slewing
  //--------------------------------------------------------------------
   
   
   // first RE-arrange the elements according to pico charge
   //  gia na xoume apo mikrotero se megalytero
   std::sort(vec_vt.begin(), vec_vt.end(), 
               [](const auto& i, const auto& j) { return i.q < j.q; } );
   
   //      1----> keeps mean of each fit  &&&&&&&&&&&    2----> keeps sigma of each fit
   TGraphAsymmErrors *slewing1  = new TGraphAsymmErrors();
   slewing1->SetTitle("mean SAT in bins of charge");
   slewing1->SetMarkerColor(1);
   slewing1->SetMarkerStyle(20);
   slewing1->GetXaxis()->SetTitle("(fitted) charge (pC)");
   slewing1->GetYaxis()->SetTitle("mean SAT");

   
   TGraphAsymmErrors *slewing2  = new TGraphAsymmErrors();
   slewing2->SetTitle("SAT resolution in bins of charge");
   slewing2->SetMarkerColor(1);
   slewing2->SetMarkerStyle(20);
   slewing2->GetXaxis()->SetTitle("(fitted) charge (pC)");
   slewing2->GetYaxis()->SetTitle("resolution");
   
   TH1F *qbin_hist = new TH1F("qbin_hist"," ", 50, 0,0 );
   TF1 *simple_gaus = new TF1 ("simple_gaus","gaus");
   auto* c3 = new TCanvas("c3", "", 1000, 700);
   
   const u64 delta_i = 300;
   
   i32 ipoint =0; 
   for(u64 i=0; i<vec_vt.size(); ++i) {
      u64 i0 = i;
      f64 mean_q = 0;
      
      qbin_hist ->Reset(); 
      qbin_hist->GetXaxis()->SetLimits(0,0);
      
      while(i<i0+delta_i){
      
           mean_q += vec_vt.at(i).q;
           qbin_hist->Fill(vec_vt.at(i).sat);        
           
           if (i==vec_vt.size()-1) break;
           ++i;
           }
      mean_q /=(i-i0);     
            
      TFitResultPtr r = qbin_hist->Fit(simple_gaus, "S");
      
      c3->cd();
      qbin_hist->Draw();
      
      App.Run(kTRUE);

      
      slewing1->SetPoint(ipoint, mean_q, r->Parameter(1));
      slewing2->SetPoint(ipoint, mean_q, r->Parameter(2));
      // error-x-low && error-x-high && error-y-low && error-y-high
      slewing1->SetPointError(ipoint, abs(mean_q- vec_vt.at(i0).q), abs(vec_vt.at(i-1).q -mean_q), r->ParError(1),r->ParError(1));
      slewing2->SetPointError(ipoint, abs(mean_q- vec_vt.at(i0).q), abs(vec_vt.at(i-1).q -mean_q), r->ParError(2),r->ParError(2));
      ++ipoint ;
    }
    
    Double_t par[6]; /* parameter array */
    TF1 *slew_correction_fit = new TF1("slew_correction_fit",slew_correction_function, vec_vt.front().q , vec_vt.back().q,3);   // IMPORTANT: NUM OF PARS
    slew_correction_fit -> SetParNames("b","w","a");
    slew_correction_fit -> SetParameters(0.1, 0.5, 0 );
    
    slewing1->Fit("slew_correction_fit","RS");
    slew_correction_fit->GetParameters(&par[0]);
    
    
     auto* c4 = new TCanvas("c4", "", 1000, 700);
     c4->Divide(1,2);
     c4->cd(1);
    slewing1->Draw("ap");
    c4->cd(2);
    slewing2->Draw("ap");
    
    App.Run(kTRUE);
    //for(i32 i=0; i<slewing2->GetN(); ++i) cout <<slewing2->GetPointX(i)<< "   "<<slewing2->GetPointY(i) <<endl;
    
  /*----------------------------------------------------------------------------------
   Double gaussian plot for signal arrival time over ALL pulses
  -------------------------------------------------------------------------------------*/
  
  SAT_total->Reset();
  
  for(u64 i=0; i<vec_vt.size(); ++i) {
     SAT_total->Fill( vec_vt.at(i).sat -(par[0]/pow(vec_vt.at(i).q,par[1])+par[2])  );
    }  
  
  auto* c2 = new TCanvas("c2", "", 1000, 700);
  c2->cd();
  SAT_total->SetMarkerStyle(20);
  SAT_total->SetMarkerSize(1);
  SAT_total->SetMarkerColor(1);
  SAT_total->SetLineColor(1);
  
  mean = SAT_total->GetMean();
  rms = SAT_total-> GetStdDev();
  
  /* creating 2 gaussian curves */
  
  TF1 *g1= new TF1 ("gauss1","gaus", mean-5*rms, mean+5*rms);
  g1->SetParameter(1,mean);
  g1->SetParameter(2,rms);
  
  g1->SetLineColor(2);
  g1->SetLineStyle(10);
  
  SAT_total-> Fit (g1,"","",mean-rms, mean+rms);
  // get parameters from the fit
  g1-> GetParameters (&par[0]);
  
  //--------------------------------
  TF1 *g2 = new TF1("g2",fline,mean-5*rms, mean+5*rms,3);
  g2->SetParameter(2,par[2]); // approx rms
  g2->FixParameter(1,par[1]); // fix mean
  //---------------------------------------
  TF1 *double_gaus= new TF1("double_gaus","gaus(0)+gaus(3)",mean-5*rms, mean+5*rms); /* define combined function*/
  
  
  g2->SetLineColor(3);
  g2->SetLineStyle(6);
  double_gaus->SetLineColor(4);
  
     //fit only the edge area excluding the center
   reject = true;
   SAT_total->Fit(g2,"+");
   reject = false;
  //-----------------------------------
  
  // get parameters from the fit
  g2-> GetParameters (&par[3]);
  
  // use the parameters on the combined function 
  double_gaus-> SetParameters (par);

  SAT_total->Fit("double_gaus","R+");
  //SAT_total->Fit("double_gaus","+","",0,0); //fitting gaussian curve on the histogram 
  
  
  //---------------- fill hist to get stats----------------
  TH1F *dumb_hist = new TH1F("dumb_hist"," ", 1000, 0, 0 );
    
  for (i32 i = 0; i < 100000; ++i) {
    dumb_hist->Fill( double_gaus->GetRandom() );
  }
  f64 meanfit = dumb_hist->GetMean();
  f64 sigmafit = dumb_hist-> GetStdDev();
  f64 sigmafit_error = dumb_hist->GetStdDevError();
  std::cout << "Mean = " << meanfit << std::endl;
  std::cout << "Sigma = " << sigmafit << std::endl;
  //-----------------
  
  // to display fit-params of interest
  TPaveText *t = new TPaveText(0.78, 0.6, 0.98, 0.75, "NDC"); // left-up
        t->AddText(Form("#sigma_{1} =    %.1f #pm %.1f ps", par[2]*1e03, g1->GetParError(2)*1e03));
        t->AddText(Form("#sigma_{2} =    %.1f #pm %.1f ps", par[5]*1e03, g2->GetParError(2)*1e03 ));
        t->AddText(Form("#sigma_{tot} =   %.1f #pm %.1f ps", sigmafit*1e03, sigmafit_error*1e03));
        t->SetBorderSize(1);
        t->SetFillColor(gStyle->GetTitleFillColor());
  
  gStyle->SetOptFit(0);  
  SAT_total->Draw("E1");
  double_gaus->Draw("SAME");
  g1->Draw("SAME");
  g2->Draw("SAME");  
  t->Draw("SAME");


  App.Run(kTRUE);
  gStyle->SetOptFit(1);  
  
   // store and close 
   SAT_total -> Write();
   charge_total -> Write();
   peak_total->Write();
   peak_charge->Write();
  delete outFile; 
  return 0;
}  
