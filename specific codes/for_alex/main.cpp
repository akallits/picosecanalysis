#include "include/channels.hpp"
#include "include/header.hpp"
#include "include/types.hpp"

#include "./parsing.cpp"

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <TApplication.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TGraph.h>
#include <TMultiGraph.h>

auto main(i32 Argc, c8* Argv[]) -> i32 {
  if (Argc != 5) {
    fprintf(stderr, "Usage:");
    fprintf(stderr, "./main C1--Trace--00000.trc C2--Trace--00000.trc "
                    "C3--Trace--00000.trc C4--Trace--00000.trc");
    fprintf(stderr, "No filenames given!");
    exit(1);
  }

  TApplication App("app", nullptr, nullptr);
  constexpr u64 nChannels = 4;
  std::vector<channel> Channels(nChannels);
    
  for (u64 i = 0; i < nChannels; ++i) {
    Channels[i] = create_channel(Argv[i + 1]);
  }

  constexpr i32 nEvents   = 15;
  Color_t Colors[nEvents] = {kWhite,  kBlack,  kGray,    kRed,    kGreen,
                             kBlue,   kYellow, kMagenta, kCyan,   kOrange,
                             kSpring, kTeal,   kAzure,   kViolet,  kPink};


  /* for (i32 iChannel = 0; iChannel < 4; ++iChannel) { */
  /*   pft::println(stdout, "Channel ", iChannel); */
  /*   for (i32 iEvent = 0; iEvent < nEvents; ++iEvent) { */
  /*     pft::println(stdout, "trigtime ", Channels[iChannel].TriggerTime[iEvent], */
  /*                  ", trigoffset ", Channels[iChannel].TriggerOffset[iEvent]); */
  /*   } */
  /*   pft::println(stdout); */
  /* } */

  std::vector<std::vector<TGraph*>> Graphs(4, std::vector<TGraph*>(nEvents));
  for (i32 iChannel = 0; iChannel < 4; ++iChannel) {
    for (i32 iEvent = 100; iEvent < 100+nEvents; ++iEvent) {
      auto XY = get_next_event(Channels[iChannel], iEvent);
      Graphs[iChannel][iEvent] =
          new TGraph(XY.first.size(), XY.first.data(), XY.second.data());

      Graphs[iChannel][iEvent]->SetName(Form("gg_%d", iEvent));
      Graphs[iChannel][iEvent]->SetUniqueID(0);

      Graphs[iChannel][iEvent]->SetMarkerColor(Colors[iEvent]);
      Graphs[iChannel][iEvent]->SetMarkerStyle(20);
      Graphs[iChannel][iEvent]->SetMarkerSize(0.6);
    }
  }

  std::vector<TMultiGraph*> MultiGraphs(4);
  for (i32 iChannel = 0; iChannel < 4; ++iChannel) {
    MultiGraphs[iChannel] = new TMultiGraph(Form("tm%d", iChannel), "");
    for (i32 iEvent = 0; iEvent < nEvents; ++iEvent) {
      MultiGraphs[iChannel]->Add(Graphs[iChannel][iEvent], "p");
      MultiGraphs[iChannel]->SetTitle(Form("Channel %d", iChannel + 1));
    }
  }
  auto* Canvas = new TCanvas("c1", "", 1366, 768);
  Canvas->Divide(2, 2);
  Canvas->cd(1);
  MultiGraphs[0]->Draw("a");
  Canvas->cd(2);
  MultiGraphs[2]->Draw("a");
  Canvas->cd(3);
  MultiGraphs[1]->Draw("a");
  Canvas->cd(4);
  MultiGraphs[3]->Draw("a");
  Canvas->Draw();

  App.Run();
  return 0;
}
