#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>
#include <sstream>

// basic file operations
#include <iostream>
#include <fstream>

//root includes
#include <TApplication.h>
#include <TCanvas.h>
#include <TColor.h>
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include "TF1.h"
#include "TH2.h"
#include "TFile.h"
#include <TSystem.h>
#include <TTree.h>
#include <TStyle.h>
#include <TCut.h>
#include "TRootCanvas.h"
#include <TGraph.h>
#include <TFitResult.h>
#include "TH1.h"

// for math functions
#include <TMath.h>
#include <math.h>

using namespace std;
using u64 = uint64_t;

#include "./functions_mcp.cpp"
//########################## MAIN ################################
int main(){
   
   //INITIAL SETTINGS
   u64 scale = 0;

  //for initial values of slew and resol fit OF EACH MCP 
  double fit_par[4][5]={ {-0.851, 6.866, -0.106, 4.933, 8.899},{-0.1,5,-0.5,5,20}, {-0.163,2.267,-0.1,5.270,9.723},{-0.1,3,-0.1,5,20} }; // parameter array
  // for delta time range OF EACH MCP
  double range[4]={500,1300,-3900,-3200};
  
  int data_run[3]={284,0,314};

  // root app
   TApplication App("app", nullptr, nullptr);
   auto* canvas = new TCanvas("canvas", "", 900, 900);
      gStyle->SetOptFit(1111);
   
   TFile *rootfile = new TFile("./electronics_tree.root","read");
  
  //read file and retrieve the tree
for(u64 i =0;i<3-scale*1;i=i+2){
  int run=data_run[i];
  TTree *data_tree = (TTree*) rootfile->Get(Form("tree_run%d",run));
  Int_t treentries = data_tree->GetEntries();
  cout <<"num of initial entries/ events: "<<treentries <<endl;
  data_tree->Print();   
   
  
  info pico, mcp2;
  vector<info> vec_pico, vec_mcp2;
  double t2, t;
  double x, y;
  double chi2;
  double p2;
  
  data_tree->SetBranchAddress("x",&x);
  data_tree->SetBranchAddress("y",&y);
  data_tree->SetBranchAddress("chi2",&chi2);
  
  data_tree->SetBranchAddress("mcp2_gpeak",&mcp2.v);
  data_tree->SetBranchAddress("mcp2_qall",&mcp2.q); 
  data_tree->SetBranchAddress("mcp2_t",&t2); 
   
  data_tree->SetBranchAddress("pico_gpeak",&pico.v);
  data_tree->SetBranchAddress("pico_qall",&pico.q);
  data_tree->SetBranchAddress("pico_t", &t); 
  data_tree->SetBranchAddress("param_p3", &p2); 
  
  for (int j=0;j<treentries;j++) { 
       data_tree->GetEntry(j); 
       
       //define only "global" cuts

       bool mcp2_cut = mcp2.v>0.5 && mcp2.v<10.;

       if(mcp2_cut){
          pico.t = (t-t2)*1000;
          pico.rp2 = 1/p2;

          vec_pico.push_back(pico);   
       }   
  }

  overflow_cut(vec_pico,0.36,0.01); // cut overflow 
  charge_cut(vec_pico,200,0.5); // cut overflow
  dt_cut(vec_pico, range[i],range[i+1]); // cut times out of range
  
  if(scale){
     double pp2 = 1.35413439;
     double pp3 = -0.98508452;
     q_scale(vec_pico,1/pp2, -pp3/pp2);
  }
  
  cout <<"data after all the cuts:  "<< vec_pico.size() <<endl;
  
  q_vector_sort(vec_pico);    // first RE-arrange the elements according to charges

  // fill the slewing and resol graphs according to
  //gauss fits of q-bins 
  string titlescale = scale? " SCALED ":" "; // auto einai mono gia na prosarmozei tous titlous sta graphs
  string str =Form("./electronics_run%d_qall_%s.txt",run,titlescale.c_str());
  
  //an thes na apothikeuseis se txt to graph vale : auto graphs = slew_resol_gr(vec_pico,"q",str); .... kai svise tin katw grammi
  auto graphs = slew_resol_gr(vec_pico,"q");
  TGraphAsymmErrors *slewing = graphs.first;
  TGraphAsymmErrors *resol = graphs.second;
  
  slewing->SetTitle( Form("RUN %d ; Total Row charge %s (pC)",run,titlescale.c_str()) );
  resol->SetTitle( Form("; Total Row charge %s (pC)",titlescale.c_str()) );

  double dt_delay =slewing->GetPointY(slewing->GetN()-1)-10;
  manual_move( slewing, -dt_delay );
  
  //get fitting parameters
  //FIT
  int first_fit_point =0;
  int last_fit_point = slewing->GetN()-1;
  auto q_fit_range = fit_range(slewing, first_fit_point, last_fit_point);
  
  fit(slewing, q_fit_range.first, q_fit_range.second, fit_par[i]);

  
  canvas->Clear(); 
  canvas->Divide(1,2);
  canvas->cd(1);
  slewing->Draw("ap");
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  
  canvas->cd(2);
  gPad->SetLogy();
  slewing->SetTitle("logarithmic scale");
  slewing->Draw("ap");
    
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore();
  
    
  // correct for slew
  fit_par[i][4]=fit_par[i][4]+dt_delay; // reset time delay
  vec_pico = correct_for_slew(vec_pico,fit_par[i]); 
  
  // fill the slewing and resol graphs according to
  //gauss fits of q-bins   
 
   graphs = slew_resol_gr(vec_pico, "q" );
   slewing = graphs.first;
   resol = graphs.second;
   
  slewing->SetTitle(Form("RUN %d ; Total Row charge %s (pC); Corrected slewing (ps)",run,titlescale.c_str() ));
  resol->SetTitle(Form("; Total Row charge %s (pC)",titlescale.c_str()) );
   
  //FIT
  first_fit_point=0;
  last_fit_point= resol->GetN()-1;
  q_fit_range = fit_range(resol, first_fit_point, last_fit_point);
   
  fit(resol, q_fit_range.first, q_fit_range.second, fit_par[i+1]);

  canvas->Clear(); 
  canvas->Divide(1,2);
  canvas->cd(1);
  slewing->Draw("ap");
  canvas->cd(2);
  resol->Draw("ap");
    
  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore(); 
  
  //***********************************************
  //        2nd ORDER CORRECTION FOR p2 PARAMETER
  //***********************************************
  
  p2_vector_sort(vec_pico);    // then RE-arrange the elements according to reversed p2 param

  // fill the slewing and resol graphs according to
  //gauss fits of rp2-bins   
   str = Form("./electronics_run%d_rp2_%s.txt",run,titlescale.c_str()); 
   
   //an thes na apothikeuseis se txt to graph vale : graphs = slew_resol_gr(vec_pico, "rp2",str );
   graphs = slew_resol_gr(vec_pico, "rp2");
   slewing = graphs.first;
   resol = graphs.second;
   
  slewing->SetTitle(Form("2nd order correction for slope: RUN %d ; 1/p2 parameter; slewing corrected for %s charge (arbitrary)",run,titlescale.c_str() ));
  resol->SetTitle("; 1/p2 parameter");
  
  //FIT
  first_fit_point=1;
  last_fit_point= resol->GetN()-1;
  auto p2_fit_range = fit_range(resol, first_fit_point, last_fit_point);
   
  double p2_par[5] ={-5,5,-5,-5,-50};
  fit(slewing, p2_fit_range.first, p2_fit_range.second, p2_par );

  TMultiGraph *multi  = new TMultiGraph();
  multi = mark_not_fitted_pnts(slewing, first_fit_point, last_fit_point);
  multi->SetTitle(Form(" 2nd order correction for slope: RUN %d ; 1/p2 parameter; slewing corrected for %s charge (ps)",run,titlescale.c_str()));
  
  canvas->Clear(); 
  canvas->Divide(1,2);
  canvas->cd(1);
  multi->Draw("ap");
  canvas->cd(2);
  resol->Draw("ap");     

  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore();  
  
  TH1F *pull_hist = new TH1F("pull_hist","Pull Histogram ", 15, -5,5);
  
  for(int j=0; j<slewing->GetN(); j++){
          double fuck = slewing->GetPointX(j);
          double pull = (slewing->GetPointY(j) -  double_expo_function(&fuck ,p2_par))/(slewing->GetErrorY(j));
          pull_hist->Fill(pull);
    }

  TFitResultPtr r = pull_hist->Fit("gaus","S"); 
  double scale_to_pull = r->Parameter(2);

  
  // adjust to log
  dt_delay =slewing->GetPointY(slewing->GetN()-1)-100;
  manual_move( slewing, -dt_delay );
  p2_par[4]=p2_par[4]-dt_delay; 
  //fit(slewing, p2_fit_range.first, p2_fit_range.second, p2_par );
  TF1 *moved_fit = new TF1("moved_fit",double_expo_function, p2_fit_range.first, p2_fit_range.second,5);
  moved_fit -> SetParameters(p2_par[0],p2_par[1],p2_par[2],p2_par[3],p2_par[4]);
  //moved_fit->SetLineColor(kRed);

  slewing->GetXaxis()->SetLimits(p2_fit_range.first-0.01, p2_fit_range.second+0.01);
  auto bandwidth = error_bandwidth(slewing, p2_fit_range.first, p2_fit_range.second, p2_par,scale_to_pull);
  
    
  canvas->Clear();
  canvas->Divide(1,2); 
  canvas->cd(1);
  gPad->SetLogy();
  slewing->Draw("ap");
  moved_fit->Draw("same");
  bandwidth.first->Draw("same");
  bandwidth.second->Draw("same");
  canvas->cd(2);
  pull_hist->Draw(); 

  gPad->Modified(); gPad->Update();
  gSystem->ProcessEvents();
  cin.ignore(); 
  
}

  ///////////////////////////
  cout <<"END OF PROGRAM, CLOSE GRAPH WINDOW TO EXIT"<<endl;  
        
   TRootCanvas *rc = (TRootCanvas *)canvas->GetCanvasImp();
   rc->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()"); 
   //gApplication->Terminate();
   
  App.Run(kTRUE);
  
   
   canvas->Close();
   rootfile->Delete();
   return 0;
}
