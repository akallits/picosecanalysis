#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>
#include <algorithm>
#include <vector>

// basic file operations
#include <iostream>
#include <fstream>

//root includes
#include "TGraphAsymmErrors.h"
#include "TMultiGraph.h"
#include <TFitResult.h>
#include "TH1.h"
#include "TF1.h"
#include <TTree.h>
#include <TRandom3.h>

// for math functions
#include <TMath.h>
#include <math.h>

using namespace std;
using u64 = uint64_t;

//#################################################
struct info {
        double v;
        double q;
        double t;
        double rp2 = 0;
   };

// q scale
void q_scale(vector<info> &v, double mult, double add) {
    for(u64 i=0;i<v.size();i++)
        v.at(i).q = v.at(i).q*mult+add;
}
   
//cut voltage overflow and voltage low pulses
void overflow_cut(vector<info> &v, double over_v, double under_v=0.) {
    v.erase(remove_if(v.begin(), v.end(), [over_v, under_v]( auto& i ) {return (i.v>over_v || i.v<=under_v); }), v.end());
}

//cut charges
void charge_cut(vector<info> &v, double over_q=1000., double under_q=0.) {
    v.erase(remove_if(v.begin(), v.end(), [over_q, under_q]( auto& i ) {return (i.q>over_q || i.q<=under_q); }), v.end());
}

//cut times out of range
void dt_cut(vector<info> &v, double under_t, double over_t) {
    v.erase(remove_if(v.begin(), v.end(), [under_t, over_t]( auto& i ) {return (i.t<under_t || i.t>over_t); }), v.end());
}

void q_vector_sort(vector<info> &v){
    std::sort(v.begin(), v.end(), 
               []( auto& i, auto& j) { return i.q < j.q; } );// lower to greater
}

void p2_vector_sort(vector<info> &v){
    std::sort(v.begin(), v.end(), 
               []( auto& i, auto& j) { return i.rp2 < j.rp2; } );// lower to greater
}


//double expo for fits
Double_t double_expo_function(Double_t *x, Double_t *par){

    return exp(par[0]*x[0]+par[1])+exp(par[2]*x[0]+par[3])+par[4];
}
/////////////////////////////////////////////////////////
//split the vector
auto split_the_vector(vector<info> &vec,string c)->pair<vector<double> ,vector<double> > {
  
  vector<double> y_values;
  y_values.reserve(vec.size());

  transform(cbegin(vec), cend(vec),
      back_inserter(y_values),
      [](const auto& data) {
        return data.t;
       }
    );
    
  vector<double> x_values;
  x_values.reserve(vec.size());

  transform(cbegin(vec), cend(vec),
      back_inserter(x_values),
      [c](const auto& data) {
        if(c=="rp2"){
            return data.rp2;}
        else
            return data.q;
       }
    );
  
  return {x_values,y_values};
}

//////////////////////////////////////////////////////
//to make slew and resol graphs
auto slew_resol_gr(vector<info> &vec, string component="q", string str="./dumb_file.txt", double sigma_ref =0,double sigma_error_ref = 0)
 -> pair<TGraphAsymmErrors*, TGraphAsymmErrors*> {
  
  auto XY = split_the_vector(vec, component);
  vector<double> Xvector = XY.first;
  vector<double> Yvector = XY.second;
  
  //ascii file for Tzamaria
  ofstream outfile;
  if(str!="./dumb_file.txt")
     outfile.open(str, ios:: out);
  //to display
  //auto* c = new TCanvas("c", "", 900, 900);
 
  TGraphAsymmErrors *mean_gr  = new TGraphAsymmErrors();
  mean_gr->SetMarkerColor(1);
  mean_gr->SetMarkerStyle(20);
  mean_gr->SetTitle(Form(" ; %s ; slewing (ps)",component.c_str() ));
  TGraphAsymmErrors *sigma_gr  = new TGraphAsymmErrors();
  sigma_gr->SetMarkerColor(1);
  sigma_gr->SetMarkerStyle(20);
  sigma_gr->SetTitle(Form(" ; %s ; resolution (ps)",component.c_str() ));
 
  TH1F *xbin_hist = new TH1F("xbin_hist"," ", 50, 0,0);
  TF1 *simple_gaus = new TF1 ("simple_gaus","gaus");
  TRandom3 *random = new TRandom3();
  int ipoint =0; 
    
  for(u64 i=0; i<Xvector.size(); ++i) {
      u64 i0 = i;
      double mean_x = 0;
      
      xbin_hist ->Reset(); 
      xbin_hist->GetXaxis()->SetLimits(0,0);

      // to set a suitable binning
      int intpart= (int) ((Xvector.size()/45 +50)/100);
      //int intpart = 9;
      u64 delta_i = random->Uniform(intpart*100-5,intpart*100+5); // +/- 10 points around intpart*100... to have approximately 45 points
      
      while(i<i0+delta_i){
      
           mean_x += Xvector[i];
           xbin_hist->Fill(Yvector[i]);        
           
           if (i==Xvector.size()-1) break;
           ++i;
           }
        mean_x /=(i-i0);     
        if(xbin_hist->GetEntries()<(intpart-0.5)*100) continue;
        TFitResultPtr r = xbin_hist->Fit(simple_gaus, "QS"); //Q for quiet...
 /*         
        c->cd();
        xbin_hist->Draw(); 
        gPad->Modified(); gPad->Update();
        gSystem->ProcessEvents();
        //cin.ignore();
 */      
       double sigmaMCP = sqrt(pow(r->Parameter(2),2)-pow(sigma_ref,2));
       double d_sigmaMCP = (r->Parameter(2)*r->ParError(2) - sigma_ref*sigma_error_ref)/sigmaMCP;
           
       mean_gr->SetPoint(ipoint, mean_x, r->Parameter(1));
       sigma_gr->SetPoint(ipoint, mean_x, sigmaMCP);
      // error-x-low && error-x-high && error-y-low && error-y-high
      mean_gr->SetPointError(ipoint, abs(mean_x- Xvector.at(i0)), abs(Xvector.at(i-1) -mean_x), r->ParError(1),r->ParError(1));
      sigma_gr->SetPointError(ipoint, abs(mean_x- Xvector.at(i0)), abs(Xvector.at(i-1) -mean_x), d_sigmaMCP,d_sigmaMCP);
            
      
      outfile <<mean_x <<" " <<abs(mean_x- Xvector.at(i0)) <<" " <<abs(Xvector.at(i-1) -mean_x) <<" " <<r->Parameter(1) <<" " <<r->ParError(1) <<" "<<sigmaMCP <<" "<<d_sigmaMCP <<endl;
      
      ++ipoint ;
   }
  xbin_hist->Delete(); 
  return {mean_gr,sigma_gr};
}

/////////////////////////////////////////////
vector<info> correct_for_slew(vector<info> &vec,Double_t *par, string component="q"){
    auto XY = split_the_vector(vec, component);
    vector<double> Xvector = XY.first;
    
    for(u64 i=0; i<vec.size(); ++i) {
         vec[i].t=vec[i].t - double_expo_function(&Xvector.at(i) ,par); 
    }
   return vec;
}


//multi-graph to emphasise non-fitted points
auto mark_not_fitted_pnts(TGraphAsymmErrors * main_graph, int first_fit_point, int last_fit_point)->TMultiGraph* {
   TGraphAsymmErrors *emphasise_point  = new TGraphAsymmErrors();  
   emphasise_point->SetMarkerColor(8);
   emphasise_point->SetLineColor(8);
   emphasise_point->SetMarkerStyle(20);
   
   TMultiGraph *multigr = new TMultiGraph();
   multigr->Add(main_graph);
   multigr->Add(emphasise_point);
   
   for(int i=0;i<first_fit_point+(main_graph->GetN()-1-last_fit_point);i++){
   int dumb = ((int) i<first_fit_point)*i+((int) i>=first_fit_point)*(last_fit_point+1);
       emphasise_point->SetPoint(i,main_graph->GetPointX( dumb ),main_graph->GetPointY(dumb));
       emphasise_point->SetPointError(i, main_graph->GetErrorXlow(dumb), main_graph->GetErrorXhigh(dumb), main_graph->GetErrorYlow(dumb ),main_graph->GetErrorYhigh(dumb ));
  }
  return multigr;
}

////////////////////////////////////
auto fit_range(TGraphAsymmErrors * graph, int first_fit_point, int last_fit_point)->pair<double,double>{
    double xlow_fit=graph->GetPointX(first_fit_point)-graph->GetErrorXlow(first_fit_point);
    double xhigh_fit=graph->GetPointX(last_fit_point)+graph->GetErrorXhigh(first_fit_point);
    
    return {xlow_fit,xhigh_fit};
}

/////////////////////////////////////
void fit(auto graph, double xlow, double xhigh ,Double_t *par, int color =2){

  TF1 *fit_function = new TF1("fit_function",double_expo_function, xlow , xhigh,5);   // IMPORTANT: NUM OF PARS

  fit_function -> SetParameters(par[0],par[1],par[2],par[3],par[4]);
  fit_function->SetLineColor(color);
    
  graph->Fit("fit_function","RS");
  fit_function->GetParameters(&par[0]);
}

////////////////////////////////////
void manual_move(TGraphAsymmErrors * graph, float dy){
    for(int i=0;i<graph->GetN(); i++){
          graph->SetPoint(i,graph->GetPointX(i) , graph->GetPointY(i)+dy);
    }
}

////////////////////////////////////
//error bandwidth
auto error_bandwidth(TGraphAsymmErrors * graph,double xlow, double xhigh ,Double_t *par, double error_scale )
 -> pair<TF1*, TF1*> {

  TGraph *low_gr  = new TGraph();  
  TGraph *high_gr  = new TGraph();

  
  for(int j=0; j< graph->GetN(); j++){
          double fuck = graph->GetPointY(j);
          low_gr->SetPoint(j,graph->GetPointX(j) , fuck-error_scale*abs(graph->GetErrorY(j)) );
          high_gr->SetPoint(j,graph->GetPointX(j) , fuck+error_scale*abs(graph->GetErrorY(j)) );
    }  

  fit(high_gr, xlow, xhigh, par );
  TF1 *high_band = new TF1("high_band",double_expo_function, xlow,xhigh,5);
  high_band -> SetParameters(par[0],par[1],par[2],par[3],par[4]);
  high_band->SetLineColor(kBlue);

  fit(low_gr, xlow,xhigh, par );
  TF1 *low_band = new TF1("low_band",double_expo_function, xlow,xhigh,5);
  low_band -> SetParameters(par[0],par[1],par[2],par[3],par[4]);
  low_band->SetLineColor(kBlue);
  
  return {low_band,high_band};
}
  
  
