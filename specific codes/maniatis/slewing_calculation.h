#include <TF1.h>
#include <TH1.h>
#include "TSystem.h"

float minimum_charge, maximum_charge, mimimum_ampl, maximum_ampl;

int intervals = 1300;
float minimum_ampl_bin, maximum_ampl_bin, minimum_charge_bin, maximum_charge_bin;

vector<float> ampl_vec, ampl_tmp_vec, charge_vec, counter_vec, mcp_vec, mm_vec;
//TH1F *v_sum_dist = new TH1F("v_sum_dist", "v_sum_dist",intervals, minimum_ampl_bin, maximum_ampl_bin);
//TH1F *q_sum_dist = new TH1F("q_sum_dist", "q_sum_dist", intervals, minimum_charge_bin, maximum_charge_bin);

TH1F *v_sum_dist = new TH1F("v_sum_dist", "v_sum_dist", 13000, 0.67, 1.44);
TH1F *q_sum_dist = new TH1F("q_sum_dist", "q_sum_dist", 13000, 43, 100);

float v_sum, q_sum;
int v_counter, q_counter;

//load data from the main programm
void Get_data(float ampl_val, float charge_val, float mcp_time, float mm_time){
    ampl_vec.push_back(ampl_val);
    charge_vec.push_back(charge_val);
    mcp_vec.push_back(mcp_time);
    mm_vec.push_back(mm_time);
}

template <typename A, typename B> void zip(const std::vector<A> &a, const std::vector<B> &b, std::vector<std::pair<A,B>> &zipped)
{
    for(size_t i=0; i<a.size(); ++i)
    {
        zipped.push_back(std::make_pair(a[i], b[i]));
    }
}

template <typename A, typename B> void unzip(const std::vector<std::pair<A, B>> &zipped, std::vector<A> &a, std::vector<B> &b)
{
    for(size_t i=0; i<a.size(); i++)
    {
        a[i] = zipped[i].first;
        b[i] = zipped[i].second;
    }
}

void Cummulatives(){
    
    float pos = 0, v_sum, q_sum;
        for(int i = 0; i < ampl_vec.size(); i++){
            v_sum = v_sum + ampl_vec.at(i);
            q_sum = q_sum + charge_vec.at(i);
            v_counter++;
            counter_vec.push_back(v_counter);
        }
    
    ampl_tmp_vec = ampl_vec;
    sort(ampl_tmp_vec.begin(), ampl_tmp_vec.end());
    auto cum_graph = new TGraph(ampl_vec.size(), &ampl_vec[0], &counter_vec[0]);
    cum_graph->SetMarkerStyle(22);
    cum_graph->SetMarkerSize(1.2);
    auto cum_can = new TCanvas("cum_can", "cum_can", 0, 0, 900, 900);
    cum_graph->Draw("AP");
    
    
    // sorting all vectors //
    vector<int> index(ampl_vec.size(),0);
    for(int i = 0; i != index.size(); i++){
        index[i] = i;
    }
    
    sort(index.begin(), index.end(),[&](const int& a, const int& b) {
             return (ampl_vec[a] < ampl_vec[b]);
         });

    
    // fitting distrs //
    int slewing_binning = 500;
    vector<float> slewing_timing, slewing_timing_error, slewing_x, slewing_x_dummy, slewing_x_error, slewing_x_error_dummy, slewing_timing_dummy, slewing_timing_error_dummy; // v_res vectors
    vector<float> sat_slewing_timing, sat_slewing_timing_error; // v_sat vactors
    float mean_x, sigma_sat, sigma_sat_error, mean_sat, mean_sat_error;
    
    TFile *fout = new TFile("distrs.root","RECREATE");
    fout->mkdir("wide");
    fout->mkdir("zoom");
    fout->mkdir("x_axis");
    for(int i = 1; i < index.size(); i++){
        if(i%slewing_binning == 0){
            TH1F *v_slewing = new TH1F("v_slewing", "v_slewing", 100, -1, 1); // distribution limits depends on run numbber

            for(int j = i; j > i - slewing_binning; j--){
                v_slewing->Fill(mm_vec.at(index.at(j)) - mcp_vec.at(index.at(j))+112);
                mean_x = mean_x + ampl_vec.at(index.at(j));
            }
            
            TF1 *fit_slew = new TF1("fit_slew","gaus");
            v_slewing->Fit("fit_slew","Q");
            sigma_sat = fit_slew->GetParameter(2);
            sigma_sat_error = fit_slew->GetParError(2);

            mean_sat = fit_slew->GetParameter(1);
            mean_sat_error = fit_slew->GetParError(1);

            fout->cd("wide");
            v_slewing->Write();
            
            float up_limit, down_limit;
            TH1F *v_slewing_zoom = new TH1F("v_slewing_zoom", "v_slewing_zoom", 50, mean_sat - 5*sigma_sat, mean_sat + 5*sigma_sat);
            TH1F *x_values = new TH1F("x_values", "x_values", 200, 0, 1); // distribution limits depends on run numbber

            for(int j = i; j > i - slewing_binning; j--){
                v_slewing_zoom->Fill(mm_vec.at(index.at(j)) - mcp_vec.at(index.at(j)) + 112);
                x_values->Fill(ampl_vec.at(index.at(j)));
            }
            // mean value of x axis
            TF1 *fit_x = new TF1("fit_x", "gaus");
            x_values->Fit("fit_x","Q");
            fout->cd("x_axis");
            x_values->Write();
            TF1 *fit_slew2 = new TF1("fit_slew2","gaus");
            v_slewing_zoom->Fit("fit_slew2","Q");
            sigma_sat = fit_slew2->GetParameter(2);

            mean_sat = fit_slew2->GetParameter(1);

            //multiple gaussian fits
            for(int k = 0; k < 3; k++){
                fit_slew2->SetParameter(1, mean_sat);
                fit_slew2->SetParameter(2, sigma_sat);
                v_slewing_zoom->Fit("fit_slew2","Q");
                
                sigma_sat = fit_slew2->GetParameter(2);
                mean_sat = fit_slew2->GetParameter(1);
            }
            fout->cd("zoom");
            v_slewing_zoom->Write();
            
            mean_sat_error = fit_slew2->GetParError(1);
            sigma_sat_error = fit_slew2->GetParError(2);
            
            sat_slewing_timing.push_back(mean_sat);
            sat_slewing_timing_error.push_back(mean_sat_error);
            slewing_timing.push_back(sigma_sat);
            slewing_timing_error.push_back(sigma_sat_error*1.25);

            slewing_x.push_back(mean_x/slewing_binning);
//            slewing_x.push_back(fit_x->GetParameter(1));
            slewing_x_error_dummy.push_back(0.001);
            float bin_width = (ampl_vec.at(index.at(i)) - ampl_vec.at(index.at(i - slewing_binning)))/2;
            slewing_x_error.push_back(bin_width);
            slewing_x_dummy.push_back((ampl_vec.at(index.at(i - slewing_binning)) + ampl_vec.at(index.at(i)))/2);
            slewing_timing_error_dummy.push_back(0.000001);
//            cout  << mean_x/slewing_binning << " " << fit_x->GetParameter(1) << endl;
            mean_x = 0; // for mean value of amplitudes
        }
    }
    
    fout->Close();
    
//    for(int i = 0; i < slewing_timing_error.size(); i++) cout << slewing_timing_error.at(i) << endl;

    TMultiGraph *mg = new TMultiGraph();
    TGraphErrors *gr_slewing_errors = new TGraphErrors(slewing_timing.size(), &slewing_x_dummy[0], &slewing_timing[0], &slewing_x_error[0], &slewing_timing_error_dummy[0]);
    //    gr_slewing_errors->Draw("same");
    mg->Add(gr_slewing_errors);
    
    TF1 *fit_power = new TF1("fit_power","[0]*x^(-[1]) + [2]", slewing_x.front(), slewing_x.back());
//    TGraph *gr_slewing = new TGraph(slewing_timing.size(), &slewing_x[0], &slewing_timing[0]);
    TGraphErrors *gr_slewing = new TGraphErrors(slewing_timing.size(), &slewing_x[0], &slewing_timing[0], &slewing_x_error_dummy[0], &slewing_timing_error[0]);
    gr_slewing->Fit("fit_power");
    gr_slewing->SetMarkerStyle(20);
    gr_slewing->GetXaxis()->SetTitle("Peak Voltage (V)");
    gr_slewing->GetYaxis()->SetTitle("Resolution (ns)");
    gr_slewing->SetTitle("Slewing effect on 8.4 GS/s");
//    gr_slewing->SetTitle("Slewing effect on 6.5 GS/s");
    TLatex *text = new TLatex();
    text->SetTextSize(0.03);
    mg->Add(gr_slewing);
    //text->DrawLatex(0.4, 0.1, Form("f(x) = %f * x^{-%f}", fit_power->GetParameter(0),fit_power->GetParameter(1)));
    TCanvas *can_slewing = new TCanvas("can_slewing", "can_slwing", 0, 0, 900, 900);
    mg->Draw("AP");
    
    //SAT fit
    TF1 *sat_fit_power = new TF1("sat_fit_power","[0]*x^(-[1])", slewing_x.front(), slewing_x.back());

    TCanvas *can_sat_slewing = new TCanvas("can_sat_slewing", "can_sat_slwing", 0, 0, 900, 900);
    TGraphErrors *gr_slewing_sat = new TGraphErrors(sat_slewing_timing.size(), &slewing_x[0], &sat_slewing_timing[0], &slewing_x_error[0], &sat_slewing_timing_error[0]);
    
    gr_slewing_sat->Fit("sat_fit_power");
    gr_slewing_sat->SetMarkerStyle(20);
    gr_slewing_sat->GetXaxis()->SetTitle("Peak Voltage (V)");
    gr_slewing_sat->GetYaxis()->SetTitle("SAT (ns)");
    gr_slewing_sat->SetTitle("Slewing effect on 8.4 GS/s (SAT)");
//    gr_slewing_sat->SetTitle("Slewing effect on 6.5 GS/s (SAT)");
    gr_slewing_sat->Draw("AP");
    
    
    
//    ofstream export_fit;
//    export_fit.open("./fit_data.txt");
//    for(int i = 0; i < slewing_x.size(); i++){
//        export_fit << slewing_x.at(i) << " " << slewing_timing.at(i) << " " << slewing_timing_error.at(i) << endl;
//    }

}


