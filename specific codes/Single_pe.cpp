#define Single_pe_cxx
#include "Single_pe.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void Single_pe::Loop()
{
//   In a ROOT session, you can do:
//      root> .L Single_pe.C
//      root> Single_pe t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

    auto amplitude = new TH1D("amplitude", ";Amplitude (V) ;Events", 150, 0, 0.06);
    auto charge = new TH1D("charge", ";Charge (pC) ;Events", 150, 0, 3);
    
    ofstream points;
    points.open("single_pe.txt");
   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
       amplitude->Fill(MM1_global_maximum_y);
       charge->Fill(MM1_e_charge);
      // if (Cut(ientry) < 0) continue;
   }
    
    float bin_center = 0, n_events = 0;
    for(int i = 0; i < 150; i++){
        bin_center = amplitude->GetBinCenter(i);
        n_events = amplitude->GetBinContent(i);
        points << bin_center << " " << n_events << endl;
    }
    
//    float
//    auto pol = new TF1("pol", "[0]*TMath::Power(([1] + 1),([1] + 1)) * TMath::Power(x/[2],[1])  * TMath::Exp(-([1] + 1)*(x/[2]))/TMath::Gamma([1]+1)", 0.01, 0.04);
    
    auto pol = new TF1("pol", "[0]*TMath::Power(([1] + 1),([1] + 1)) * TMath::Power(x/[2],[1])  * TMath::Exp(-([1] + 1)*(x/[2]))/(TMath::Exp(ROOT::Math::lgamma([1]+1)))", 0.012, 0.04);
    float theta = (pow(amplitude->GetMean(),2)/pow(amplitude->GetRMS(),2)) - 1;
//  float theta = (pow(0.01568,2)/pow(0.00387,2)) - 1;
    auto pol2 = new TF1("pol2", "[0]*TMath::Power(([1] + 1),([1] + 1)) * TMath::Power(x/[2],[1])  * TMath::Exp(-([1] + 1)*(x/[2]))/(TMath::Exp(ROOT::Math::lgamma([1]+1)))", 0., 0.012);
    pol2->SetParameters(432, 4.37068, 0.0115425);
    
    
    float mean = amplitude->GetMean();
//  pol->SetParameters(400, theta, mean);
    pol->SetParameters(400, theta, mean);
//  pol->SetParameters(400, 0.005, mean);
//  pol->FixParameter(0,480);
    amplitude->Fit("pol","", "",0.012,0.04);
    cout << pol->GetChisquare() << endl;
    

    auto c1 = new TCanvas("c1", "c1", 0 ,0, 900,900);
    c1->SetLogy();
    amplitude->Draw();
    pol2->SetLineColor(1);
    pol2->Draw("same");
    auto c2 = new TCanvas("c2", "c2", 0, 0, 900, 900);
    charge->Draw();
}
