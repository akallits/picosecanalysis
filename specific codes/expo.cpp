

void expo() {

  // the falling edge
  int nxy = 0;
  // get  the points of the tail
  auto* graph_falling_edge = new TGraphErrors();
  for (std::size_t i = local_max_id + 5; i < pulse_width; ++i) {
    graph_falling_edge->SetPoint(nxy, xs[i], ys[i]);
    graph_falling_edge->SetPointError(nxy, 0.0, error_ys[i]);
    nxy++;
  }
  // find normalisation
  constexpr double slp = -0.0173; // example slope 
  double rnorm        = 0.0;
  for (int i = 0; i < 5; ++i) {
    rnorm += TMath::Log(graph_falling_edge->GetPointY(i)) -
             (slp * graph_falling_edge->GetPointX(i));
  }
  rnorm /= 5.0;

  // find and exclude secondary bumps
  for (int i = 0; i < nxy; ++i) {
    const double ddi = graph_falling_edge->GetPointY(i) -
                    TMath::Exp(rnorm + slp * graph_falling_edge->GetPointX(i));
    if (ddi > 10.0 * noise_rms) {
      graph_falling_edge->SetPointError(i, 0.0, 10.0 * ddi);
    }
  }
  auto* exp_fun = new TF1("expfun", analysis::exponential_func,
                          xs[local_max_id], xs[local_max_id + nxy], 2);
  exp_fun->SetParameter(0, rnorm);
  exp_fun->SetParameter(1, slp);
  graph_falling_edge->Fit(exp_fun, "R");
  graph_falling_edge->Fit(exp_fun, "R");
  graph_falling_edge->Fit(exp_fun, "R");


}
