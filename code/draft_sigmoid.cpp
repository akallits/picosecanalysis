  double tfit[4]={0.,0.,0.,0.};
        tfit[ci] = TimeSigmoid(ARRAYSIZE,amplC[ci],ptime, baseline_rms, evNo);
        cout<<BLUE<<"Channel "<< ci <<" has sigmoid timepoint ="<< tfit[ci]<<endl;
        cout<<"sigmoid timepoint ="<< tfit[ci]<<endlr;
        FullSigmoid(ARRAYSIZE, amplC[ci], ptime, baseline_rms, evNo); 

        cout<<MAGENTA<<"FullSigmoidFuncion processed"<<endlr;


        //tfit[ci] = TimeSigmoidDraw(ARRAYSIZE, amplC[ci], ptime, baseline_rms, evNo, clr[ci], sigcanv[ci],ci, cname, draw);
     //cout<<RED<< " sigmoid timepoint"<<tfit[ci]<<endlr;


 sigcanv[ci]->cd(1);
     cout<<MAGENTA<<"Preparing for Sigmoind Fit" <<endlr;
        int sig_start = start_point.pos - 10; //points
        while(sig_start<0) sig_start++;
        int sig_end = global_maximum.pos + 20; //points
        while(sig_end>=ARRAYSIZE) sig_end--;
        int Npoints = sig_end - sig_start+1; 
        if(Npoints >100 || Npoints<=0)
        {
          cout<<RED<<"Attention : Sigmoid fit has failed ==> Number of points on sig_waveform ==>"<< Npoints <<endlr;
        }

       //cout<<"global maximum position = "<<" "<<global_maximum.pos<<" " <<"sigmoid starting point"<<" "<< sig_start <<" "<< sig_start_t<<endl;
       double x[Npoints], y[Npoints], erx[Npoints], ery[Npoints];
       //double *x = new double[Npoints];
       //double *y = new double[Npoints];
       //double *erx = new double[Npoints];
       //double *ery = new double[Npoints];
       int points;
       for (int i = sig_start; i <= sig_end; ++i)
       {
         y[i-sig_start] = amplC[ci][i];
         x[i-sig_start] = ptime[i];
         erx[i-sig_start] = 0;
         ery[i-sig_start] = baseline_rms;
         points++;
         //ery[i-sig_start] = bslC;
         //cout<<amplC[ci][i]<<" "<< ptime[i] <<"  "<<baseline_rms<<endl;

        }

        sig_waveform = new TGraphErrors(Npoints, x, y, erx, ery);
        sprintf(cname,"Event %d Sigmoid Fit C%d\n",evNo,ci+1);
        sig_waveform->SetTitle(cname);
        sig_waveform->SetLineColor(clr[ci]);
        sig_waveform->SetMarkerColor(clr[ci]);
        sig_waveform->SetFillColor(0);
        //continue;


        TF1 *sig_fit =  new TF1("sig_fit",fermi_dirac,x[0],x[global_maximum.pos - sig_start], 4);
        double sig_pars[10];
     
        double y_half_point = 0.5*global_maximum.ampl;
        //double den = amplC[ci][global_maximum.pos]-amplC[ci][sig_start]; 
        double  x_mid_left = (x[sig_end - sig_start]+ x[1])/2.;
        //slope_at_x(amplC[ci], ptime, sig_start, global_maximum.pos, y_half_point);//*4/(den);
        
        //double steepness_left = (x[sig_start - sig_end] + x[1])/2;
        //cout<<"Steepness result: "<<steepness_left <<endl;        
        double steepness_left = 5./(x[sig_end-sig_start] - x[1]);
        //Xpoint_linear_interpolation(amplC[ci], ptime, sig_start, global_maximum.pos, y_half_point);


        sig_pars[0] = amplC[ci][global_maximum.pos]; // - amplC[ci][sig_start];
        sig_pars[1] = x_mid_left; 
        sig_pars[2] = steepness_left;
        sig_pars[3] = 0.;//amplC[ci][sig_start];

        /*
        sig_fit->SetParLimits(0,amplC[ci][global_maximum.pos]*0.75 , amplC[ci][global_maximum.pos]*1.25);
        sig_fit->SetParLimits(1,x[1], x[sig_end - sig_start]+x[1]);
        sig_fit->SetParLimits(2, steepness_left*0.5, steepness_left*0.8);
        sig_fit->SetParLimits(3, -baseline_rms, baseline_rms);
        */

        //sig_fit->SetParameters(amplC[ci][global_maximum.pos]-amplC[ci][sig_start], x_mid_left, steepness_left, amplC[ci][sig_start]);
        sig_fit->SetParameters(sig_pars[0],sig_pars[1],sig_pars[2],sig_pars[3]);
        sig_fit->SetLineColor(kGreen);
        
        sig_fit->SetParName(0,"max_amplitude");
        sig_fit->SetParName(1,"x_mid_point");
        sig_fit->SetParName(2,"steepness_left");
        sig_fit->SetParName(3,"baseline");


        //sig_pars[0] = amplC[ci][global_maximum.pos]; //- amplC[ci][sig_start];
        //cout<<MAGENTA<<"SigmoidP0 = "<<" "<< sig_pars[0]<<endlr;
        //sig_pars[1] = 1;
        //sig_pars[2] = steepness_left;
        //cout<<BLUE<<"SigmoidP1 = "<<" "<< sig_pars[1]<<endlr;
        //sig_pars[3] = x_mid_left;
        //cout<<YELLOW<<"SigmoidP2 = "<<" "<< sig_pars[2] <<endlr;
        //sig_pars[4] = amplC[ci][sig_start];
        //cout<<"SigmoidP3 = "<<" "<< sig_pars[3]<<endlr;

        sig_waveform->Draw("ap");
        sig_waveform->Fit("sig_fit", "QR0");
        sig_waveform->Fit("sig_fit", "QR0");
        sig_waveform->Fit("sig_fit", "QR0");
        sig_waveform->Fit("sig_fit", "QR0");
        sig_waveform->Fit("sig_fit","R0");
        sig_waveform->GetHistogram()->GetXaxis()->SetTitle("ns");
        sig_waveform->GetHistogram()->GetYaxis()->SetTitle("V");
        sig_fit->Draw("same");

       
        sig_fit->GetParameters(&sig_pars[0]);
              
        //double tfit = sig_pars[3]-(1/sig_pars[2])*(TMath::Log(sig_pars[0]/((0.2*global_maximum.ampl)-sig_pars[4])-sig_pars[1]));
        double tfit[4];
        tfit[ci] = sig_pars[1] - (1/sig_pars[2])*(TMath::Log(sig_pars[0]/((0.2*global_maximum.ampl-sig_pars[3])-1)));

        cout<<RED<<"sigmoid timepoint ="<< tfit[ci]<<endlr;
        sigcanv[ci]->Update();
        sigcanv[ci]->Modified();



        sigcanv[ci]->cd(2);

      int start = start_point.pos -60;
   

      while(start < 0) start++;

      int end = global_maximum.pos +10*(global_maximum.pos - start_point.pos);

      if(end<=start)
        end = start +200;
      int Npointsd = end-start+1;

      double x_d[Npointsd], y_d[Npointsd], erx_d[Npointsd], ery_d[Npointsd];

      for (int i = start; i < end; ++i)
      {
        x_d[i-start] = ptime[i];
        y_d[i-start] = amplC[ci][i];
        erx_d[i-start] = 0;
        ery_d[i-start] =baseline_rms;
      }

      double sig_lim_min = ptime[start_point.pos];
      double sig_lim_max = ptime[global_maximum.pos+3];
      double sig_pars_d[5];

      TF1 *sig_fitd =  new TF1("sig_fitd",fermi_dirac,sig_lim_min,sig_lim_max, 4);
      //double DT = ptime[t90point] - ptime[t10point];
      double t_half_point =  x_mid_left;
      double steepness_left_d = steepness_left;

      sig_pars_d[0] = amplC[ci][global_maximum.pos];
      sig_pars_d[1] = steepness_left;
      sig_pars_d[2] = t_half_point; 
      sig_pars_d[3] = 0.;
  
      sig_fitd->SetParameters(sig_pars_d);

//   sig_fit->SetParLimits(0,data[mpoint]*0.75 , data[mpoint]*1.25);
//   sig_fit->SetParLimits(1,steepness_left*0.5, steepness_left*1.5);
//   sig_fit->SetParLimits(2, ptime[t10point], ptime[t90point]);
//   sig_fit->SetParLimits(3, -yerr[0], yerr[0]);
  
      for (int i = global_maximum.pos+10;i<maxpoints && i<global_maximum.pos+10;i++)
      { 
        ery_d[i]*=10.;
        //cout<<BLUE<<"error = "<<ery_d[i]<<endlr;
      }


      TGraphErrors* sig_waveformd = new TGraphErrors(Npointsd, x_d, y_d, erx_d, ery_d);
      //TGraphErrors* sig_waveformd = new TGraphErrors(maxpoints, x, y, erx, ery);
      sig_waveformd->Draw("ap");
      sig_waveformd->SetMaximum(1.1);
      sig_waveformd->SetMinimum(-1.1);

      //sig_fitd->SetLineColor(kMagenta);
  
  //   sprintf(cname,"Event %d Sigmoid Fit C%d\n",evNo,ci+1);
//   sig_waveform->SetTitle(cname);
//   sig_waveform->SetLineColor(clr[ci]);
//   sig_waveform->SetMarkerColor(clr[ci]);
//   sig_waveform->SetFillColor(0);
      sig_waveformd->Fit("sig_fit", "QR");
      sig_waveformd->Fit("sig_fit", "QR");
      sig_waveformd->Fit("sig_fit","QR+");
      //sig_fitd->Draw("same");
  
      for (int i=0;i<4;i++)
        sig_fitd->GetParameter(i);

      t_half_point =  ptime[global_maximum.pos] + 3. ;
 /// double sigmoid fit here   
      double sig_lim_min2 = ptime[global_maximum.pos-1];
      double sig_lim_max2 = ptime[global_maximum.pos]+1.5;
      sig_pars[0] = amplC[ci][global_maximum.pos];
      sig_pars[1] = -steepness_left;
      sig_pars[2] = t_half_point; 
      sig_pars[3] = amplC[ci][global_maximum.pos]*0.1;
  
      TF1 *sig_fit2 =  new TF1("sig_fit2",fermi_dirac,sig_lim_min2,sig_lim_max2, 4);
      sig_fit2->SetParameters(sig_pars);
      sig_fit2->FixParameter(0,sig_fitd->GetParameter(0));
      sig_fit2->FixParameter(3,sig_fitd->GetParameter(3));
  
      sig_waveformd->Fit("sig_fit2", "QMR");
      sig_waveformd->Fit("sig_fit2", "QMR");
      sig_waveformd->Fit("sig_fit2","QR+");

      sig_fit2->SetRange(sig_lim_min-3.,sig_lim_max2+10.);
      sig_fit2->SetLineColor(kCyan);
      //sig_fit2->Draw("same");
      sigcanv[ci]->Update();
  
      TF1 *sig_fit1 =  new TF1("sig_fit1",fermi_dirac_sym_1,sig_lim_min,sig_lim_max2+10., 3);
      sig_fit1->SetParameter(1,sig_fit2->GetParameter(1));
      sig_fit1->SetParameter(2,sig_fit2->GetParameter(2));
      sig_fit1->SetLineColor(kGreen+2);
      sig_fit1->Draw("same");
      sigcanv[ci]->Update();

      TF1 *sig_fittot =  new TF1("sig_fittot",fermi_dirac_sym_double,sig_lim_min,sig_lim_max2+10., 6);
      for (int i=0;i<4;i++)
        sig_fittot->SetParameter(i,sig_fitd->GetParameter(i));

      sig_fittot->SetParameter(3+1,sig_fit2->GetParameter(1));
      sig_fittot->SetParameter(3+2,sig_fit2->GetParameter(2));
      sig_fittot->SetLineColor(kBlue);
      sig_fittot->Draw("same");
  
      sig_waveformd->Fit("sig_fittot","M+","",sig_lim_min,sig_lim_max2+2.6);
      sig_waveformd->GetHistogram()->GetXaxis()->SetRangeUser(sig_lim_min-2.,sig_lim_max2+10.);
      sig_waveformd->GetHistogram()->GetXaxis()->SetTitle("ns");
      sig_waveformd->GetHistogram()->GetYaxis()->SetTitle("V");
      gStyle->SetOptFit(1111);
      sigcanv[ci]->Update();
      sigcanv[ci]->Modified();





/// Sigmoid Fit Draw
     tfit[ci] = TimeSigmoidDraw(ARRAYSIZE, amplC[ci], ptime, baseline_rms, evNo, clr[ci], sigcanv[ci],ci, cname, draw);
     cout<<RED<< " sigmoid timepoint"<<tfit[ci]<<endlr;

     