//  return 0;   
        double global_maximum_ampl = 111111;
        double global_maximum_pos = 5; 
/// Find Global Maximum of the pulse 
        int baseline_region_end = maxpoints;
        struct FindGlobalMaximum global_maximum;
        global_maximum = GlobalMaximum(amplC[ci], ARRAYSIZE, ptime);

///Find baseline level
        if(global_maximum.pos < baseline_region_end)
        {
           baseline_region_end = global_maximum.pos;
           cout<<"baseline_region_end_point = " << baseline_region_end*dt << " Max position = " << global_maximum.x<<endl;
        }
        
        baseline_region_end -= 200; //200 points back of the peak
        //cout<< "baseline_region_end after subtracting 200 " <<baseline_region_end <<endl;
        if(baseline_region_end < 0)
        baseline_region_end = 100.;
        double baseline_level, baseline_rms, baselineRMS;
        baseline_rms = FindBaselineLevel(amplC[ci], baseline_region_end);

        int max_region_end;
        max_region_end = 2000+baseline_region_end;
    //cout<<RED<<"baseline rms = "<< baseline_rms<<endlr;

/// Find the start point 
        int start;
        struct FindStartPoint start_point;
        start_point = StartPoint(amplC[ci], ptime, baseline_rms, baseline_region_end, dt); //global_maximum.pos, baseline_rms)
        cout<<RED<<"Starting point is : "<< start_point.x <<endlr;

/// Find the end point 
        int end;
        struct FindEndPoint end_point;
        end_point = EndPoint(amplC[ci], ptime, baseline_rms, max_region_end, dt, ARRAYSIZE);
        cout<<BLUE<<"Ending point is : "<< end_point.x <<endlr;
        struct SearchEpeakEndPoint e_peak_end;
        e_peak_end = SearchEpeakEndPoint(amplC[ci], ptime, global_maximum, end_point, ARRAYSIZE);
        if (ci+1==1)
        {
          e_peak_end.x = end_point.x;
         cout<<RED<<"electron peak end point MCP @ "<< e_peak_end.x <<endlr;

        }

        double tfit[4]={0.,0.,0.,0.};
        tfit[ci] = TimeSigmoid(ARRAYSIZE,amplC[ci],ptime, baseline_rms, evNo);
        cout<<BLUE<<"Channel "<< ci <<" has sigmoid timepoint ="<< tfit[ci]<<endl;
        //cout<<"sigmoid timepoint ="<< tfit[ci]<<endlr;
        FullSigmoid(ARRAYSIZE, amplC[ci], ptime, baseline_rms, evNo); //this to be used for charge calculation 
    //continue;
    //return 11;