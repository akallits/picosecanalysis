  if (draw)
    {

        cout<<endl<<"Event "<< eventNo<<" Channel "<<ci+1<<"\t fit1 "<<fitstatus1[ci]<<" fit2 "<<fitstatus2[ci]<< " bsl "<<bslC[ci]<<" rms "<<rmsC[ci]<< " totcharge "<<totcharge<< endl; 
        cout<<"Pulse length = "<<maxpoints<<endl;
        long double epochX = (1.*epoch + nn*1e-9);
    //  TTimeStamp *tstamp = new TTimeStamp((time_t)epoch+(rootConv-unixConv),(Int_t)nn);
        TTimeStamp *tstamp = new TTimeStamp(epochX+(rootConv-unixConv),0);
        printf("Event time : %s = %10.9Lf  DT = %10.9LF s = %10.7LF ms\n",tstamp->AsString("l"),epochX,epochX-drawdt,1000.*(epochX-drawdt));
        drawdt = epochX;
        cout <<"Default derivation for "<<DT<<" ns , with sampling of "<<dt<<" ns ==> derivation points npt = "<<npt<< endl;

        
        evdcanv[ci]->cd(1);gPad->SetGrid(1,1);
        waveform = new TGraph(maxpoints,ptime,amplC[ci]);
        waveform2 = new TGraph(maxpoints,ptime,amplC[ci]);
        maxc[ci]=TMath::MaxElement(maxpoints,amplC[ci]);
        minc[ci]=TMath::MinElement(maxpoints,amplC[ci]); //the peak amplitude of the pulse
	      sprintf(cname,"Event %d Waveform C%d\n",evNo,ci+1);
        waveform->SetTitle(cname);
        waveform->SetLineColor(clr[ci]);
        waveform->SetMarkerColor(clr[ci]);
        waveform->SetFillColor(0);

        double fmin = frmin[ci]-frmax[ci];
        waveform->GetHistogram()->SetMinimum(fmin);
        cout<<"Setting minimum at "<<frmin[ci]-frmax[ci]<<endl;

        waveform->Draw("apl");
        
        waveform->GetHistogram()->GetYaxis()->SetRangeUser(fmin,-fmin/8.);
        evdcanv[ci]->Modified();
        evdcanv[ci]->Update();

/// Sigmoid Fit Draw
     //tfit[ci] = TimeSigmoidDraw(ARRAYSIZE, amplC[ci], ptime, baseline_rms, evNo, clr[ci], sigcanv[ci],ci, cname, draw);
     //cout<<RED<< " sigmoid timepoint"<<tfit[ci]<<endlr;
///Full sigmoid Here 
     FullSigmoidDraw(maxpoints, amplC[ci], ptime, evNo, baseline_rms, clr[ci], sigcanv[ci],ci, cname);
///drawing both Simple sigmoid and double sigmoid fit 

///calculate charges here

      //using double sigmoind fit 
      double sigmcharge = 0.;
      double Ohm = 50.;
      double step = ptime[1]- ptime[0];
      double convfact = step/Ohm *1000; //to convert in pCb
// using charge over leading edge 
      double charge_leading_edge =0; 

      for (int i = start_point.pos; i < global_maximum.pos; ++i)
        charge_leading_edge+= amplC[ci][i];
      charge_leading_edge *=convfact; 
      cout<<BLUE<<"Leading edge charge "<<charge_leading_edge<<" pCb"<<endlr; 


      double charge_e_peak = 0;
      for(int i = start_point.pos; i<=e_peak_end.pos; ++i)
        charge_e_peak +=amplC[ci][i];
      charge_e_peak*=convfact;
      cout<<RED<<"Epeak charge "<<charge_e_peak<<" pCb"<<endlr; 


      double charge_all=0; 
      for (int i = start_point.pos; i <end_point.pos; ++i)
        charge_all+=amplC[ci][i];
      charge_all*=convfact;
      cout<<BLUE<<"Total charge "<<charge_all<<" pCb"<<endlr; 

      double fit_charge =0;
      //sigmcharge = sig_fittot->Integral(ptime[start],ptime[end]);
      //fit_charge = sigmcharge/Ohm *1000; //pCb

      //cout<<RED<<"DOUBLE Sigmoid Charge "<<fit_charge<<" pCb"<<endlr; 

      /*double sigmcharge = 0.;
      double binNo;
      int intstart = sigstart+start;

      while(intstart > start)
      {
        if(fermi_dirac_sym_double_charge(ptime[intstart])<0||fermi_dirac_sym_double_charge(ptime[intstart]==0.))
          intstart--;
      }

      double Ohms = 50.;
      double step = ptime[intstart+1] - ptime[intstart];
      double convfact = step/Ohms*1000.;

      for (int i = 0; i < (Npointsd+start-intstart); i++)
      {
        binNo = ptime[intstart+1];
        if (fermi_dirac_sym_double_charge(binNo)>0)
        {
          sigmcharge+=fermi_dirac_sym_double_charge(binNo)*convfact;
        }
      }

      cout<<RED<<"DOUBLE Sigmoid Charge "<<sigmcharge<<endlr; 
*/

///  Smoothing array when no "bit filter" !!!!

      evdcanv[ci]->cd(2); gPad->SetGrid(1,1);
      DerivateArray(amplC[ci],dampl[ci],maxpoints,dt,npt,1); ///with the number of points 
      double maxelement_dampl[4]= {0,0,0,0};
      maxelement_dampl[ci] = TMath::MaxElement(maxpoints, dampl[ci]); 
      double minelement_dampl[4] = {0,0,0,0};
      minelement_dampl[ci] = TMath::MinElement(maxpoints, dampl[ci]);
      derivative = new TGraph(maxpoints,ptime,dampl[ci]); // the dapl arr has changed 
      derivative2 = new TGraph(maxpoints,ptime,dampl[ci]); 
      sprintf(cname,"Derivative C%d\n",ci+1);
      derivative->SetMarkerColor(clr[ci+8]);
      derivative->SetLineColor(clr[ci+8]);
      derivative->SetFillColor(0);
      derivative->SetTitle(cname);
      derivative->Draw("apl");
      //derivative->SetMaximum(0.02);
      //derivative->SetMinimum(-0.02);
      derivative->SetMaximum(maxelement_dampl[ci]+0.1);
      derivative->SetMinimum(minelement_dampl[ci]-0.1);
      waveform2->SetMarkerColor(clr[ci+2]);
      waveform2->SetLineColor(clr[ci+2]);
      waveform2->SetFillColor(0);
      waveform2->Draw("pl");
      evdcanv[ci]->Modified();
      evdcanv[ci]->Update();
      //continue;

///  Derivate smoothed signals for analysis  (may not be used...)
      cout<<BLUE<< "Smooting start"<<endlr;
      int nsmooth = 3;
    //if (oscsetup->AmplifierNo[ci]==1) nsmooth = 3;
      SmoothArray(amplC[ci], samplC, maxpoints, nsmooth, 1);
      //cout<<RED<<"Ready to derivate smoothed array"<<endlr;
      DerivateArray(samplC,dsampl,maxpoints,dt,npt,1);
      //cout<<YELLOW<<"Ready to plot Smoothed derivative"<<endlr;
      TGraph *graph22 = new TGraph(maxpoints,ptime,dsampl);
      sprintf(cname,"Smoothed Derivative C%d\n",ci+1);
      graph22->SetMarkerColor(clr[ci+4]);
      graph22->SetLineColor(clr[ci+4]);
      graph22->SetLineWidth(2);
      graph22->SetFillColor(0);
      graph22->SetTitle(cname);
      graph22->Draw("pl");
      //graph22->Draw("same");
      evdcanv[ci]->Modified();
      evdcanv[ci]->Update();

      evdcanv[ci]->cd(3); gPad->SetGrid(1,1);
      double intgr = IntegrateA(maxpoints,dsampl,idamplC,dt);
      TGraph *iwaveform = new TGraph(maxpoints,ptime,iamplC);
      //sprintf(cname,"Event %d Int-Waveform C%d\n",evNo,ci+1);
      iwaveform->SetTitle(cname);
      iwaveform->SetLineColor(clr[ci+4]);
      iwaveform->SetMarkerColor(clr[ci+4]);
      iwaveform->SetFillColor(0);
      //iwaveform->Draw("pl");
      //evdcanv[ci]->Modified();
      //evdcanv[ci]->Update();
      
  ///Integration of Pulse here  

      int nint = 20; //default
      nint = N_INTEGRATION_POINTS;
      //cout<<"N integration points = "<<N_INTEGRATION_POINTS<<endl;
      //double DTI2 = 2.;  ///default
      double DTI2 = 2.; 
      nint = TMath::FloorNint(DTI2/dt)+1;
      cout<<"Integration points = "<<nint<<endl;    
      intgr = IntegratePulse(maxpoints,idamplC,iampl,dt,nint*dt);
     // continue;
      maxd[ci]=TMath::MaxElement(maxpoints,iampl);
      mind[ci]=TMath::MinElement(maxpoints,iampl);
      integralh = new TGraph(maxpoints,ptime,iampl);
      integralh2 = new TGraph(maxpoints,ptime,iampl);
      sprintf(cname,"Event %d - Integral %g of C%d\n",evNo, nint*dt,ci+1);
      integralh->SetMarkerColor(clr[ci+4]);
      integralh->SetLineColor(clr[ci+4]);
      integralh->SetFillColor(0);
      integralh->SetTitle(cname);
      integralh->SetMaximum(maxelement_dampl[ci]+0.1);
      integralh->SetMinimum(mind[ci]-0.1);
      integralh->Draw("apl");
      waveform2->SetMarkerColor(clr[ci+2]);
      waveform2->SetLineColor(clr[ci+2]);
      waveform2->SetFillColor(0);
      waveform2->Draw("pl");
      //derivative2->SetMarkerColor(clr[ci+8]);
      //derivative2->SetLineColor(clr[ci+8]);
      //derivative2->SetFillColor(0);
      //derivative2->Draw("pl");
      evdcanv[ci]->Modified();
      evdcanv[ci]->Update();
  
      //intgr = IntegratePulse(maxpoints,amplC[ci],iampl,dt,50.);
      DTI2 = 1.5;
      nint = TMath::FloorNint(DTI2/dt)+1;;
      intgr = IntegratePulse(maxpoints,idamplC,iampl,dt,nint*dt);
      graph22 = new TGraph(maxpoints,ptime,iampl);
      sprintf(cname,"Event %d - Integral %g of C%d\n",evNo, nint*dt,ci+1);
      graph22->SetMarkerColor(clr[ci+10]);
      graph22->SetLineColor(clr[ci+10]);
      graph22->SetFillColor(0);
      graph22->SetTitle(cname);
      //graph22->Draw("pl");

      DTI2 = 1.;
      nint = TMath::FloorNint(DTI2/dt)+1;
      intgr = IntegratePulse(maxpoints,idamplC,iampl,dt,nint*dt);
      graph22 = new TGraph(maxpoints,ptime,iampl);
      sprintf(cname,"Event %d - Integral %g of C%d\n",evNo,nint*dt,ci+1);
      graph22->SetMarkerColor(clr[ci+15]);
      graph22->SetLineColor(clr[ci+15]);
      graph22->SetFillColor(0);
      graph22->SetTitle(cname);
      //graph22->Draw("pl");
      evdcanv[ci]->Modified();
      evdcanv[ci]->Update();
      
      cout<<RED<<"OLA KALA"<<endlr;
      continue;
        
  }///end (if(draw))
      