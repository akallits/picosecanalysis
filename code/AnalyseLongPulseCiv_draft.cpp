int AnalyseLongPulseCiv(int ci, int points, int evNo, double* data, double* drv, double *tdata, PEAKPARAM *par, double threshold, double dt, int tshift)
{
  /// use the integrated+filtered pulse to define a region where a trigger occured. (integral above threshold) 
  ///pulses are considered negative!!!

        double global_maximum_ampl = 111111;
        double global_maximum_pos = 5; 

/// Find Global Maximum of the pulse 
        int baseline_region_end = points;
        struct FindGlobalMaximum global_maximum;
        global_maximum = GlobalMaximum(data, points, tdata);

///Find baseline level
        if(global_maximum.pos < baseline_region_end)
        {
           baseline_region_end = global_maximum.pos;
           cout<<"baseline_region_end_point = " << baseline_region_end*dt << " Max position = " << global_maximum.x<<endl;
        }
        
        baseline_region_end -= 200; //200 points back of the peak
        //cout<< "baseline_region_end after subtracting 200 " <<baseline_region_end <<endl;
        if(baseline_region_end < 0)
        baseline_region_end = 100.;
        double baseline_level, baseline_rms, baselineRMS;
        baseline_rms = FindBaselineLevel(data, baseline_region_end);

        int max_region_end;
        max_region_end = 2000+baseline_region_end;
    //cout<<RED<<"baseline rms = "<< baseline_rms<<endlr;

/// Find the start point 
        int start;
        struct FindStartPoint start_point;
        start_point = StartPoint(data, tdata, baseline_rms, baseline_region_end, dt); //global_maximum.pos, baseline_rms)
        cout<<RED<<"Starting point is : "<< start_point.x <<endlr;

/// Find the end point 
        int end;
        struct FindEndPoint end_point;
        end_point = EndPoint(data, tdata, baseline_rms, max_region_end, dt, points);
        cout<<BLUE<<"Ending point is : "<< end_point.x <<endlr;
        struct SearchEpeakEndPoint e_peak_end;
        e_peak_end = SearchEpeakEndPoint(data, tdata, global_maximum, end_point, points);
        if (ci+1==1)
        {
          e_peak_end.x = end_point.x;
         cout<<RED<<"electron peak end point MCP @ "<< e_peak_end.x <<endlr;

        }

        //double tfit[4]={0.,0.,0.,0.};
        double tfit=0;;
        //tfit[ci] = TimeSigmoid(points,data,tdata, baseline_rms, evNo);
        tfit = TimeSigmoid(points,data,tdata, baseline_rms, evNo);
        cout<<BLUE<<"Channel "<< ci <<" has sigmoid timepoint ="<< tfit<<endl;
        //cout<<"sigmoid timepoint ="<< tfit[ci]<<endlr;
        FullSigmoid(points, data, tdata, baseline_rms, evNo); //this to be used for charge calculation 
    //continue;
    //return 11;  
  if (points - tshift < 50) return -1;
  int ntrig=0;
  int tpoint=0;
  double drvtrig = 0.00002;
  if (threshold <0.0025)
    drvtrig = 0.000005;
  par->tot[0]=0;
  
  for (int i=tshift; i<points; i++)   {
    if (data[i]<=threshold) {
      tpoint = i;
      ntrig=1;
//       par->tot[0]=1;
      break;
    }
    else 
      tpoint=i;
  }
  if (ntrig<=0) return (-1); // cout<<"No trigger in event!"<<endl;
  
//    cout<<"tpoint = "<<tpoint*dt<<endl;
  if (tpoint>=points-10) return (-1);

  double miny = data[tpoint];
  par->charge=0.;
  par->maxtime=tpoint;
  par->ampl=data[tpoint];

  for (int i=tpoint; i<points; i++)
  {
    if (data[i]<miny)
    {
      par->ampl=data[i];
      par->maxtime=i;
      miny=data[i];
    }
    if (data[i]<=threshold)
    {
      par->tot[0]++;
      par->ftime=i; /// this is added to avoid a pulse at the end of the data that does not return to 0!!!
    }
    else //if (data[i]>threshold)
    {
      par->ftime=i;
      par->tot[0]--;
      break;
    }
    /// note down the point the signal has gone above the threshold
  }
  /// fast scan for risetime, risecharge and t_start
  par->t90=tpoint;
  par->t10=tpoint;
  par->stime=tpoint;
  par->ttrig=tpoint; 
  for (int i=par->maxtime; i>0; i--)
  {
     if (data[i]>=par->ampl*0.9)
     {
       par->t90=i;
       break;
     }
  }
  par->risecharge=0.;
  for (int i=par->t90; i>0; i--)
  {
    par->risecharge+=data[i]; 
    if (data[i]>=par->ampl*0.1 || (data[i]>0.5*threshold && fabs(drv[i])<=drvtrig ))
    {
      par->t10=i;
      break;
    }
  }

  for (int i=par->maxtime; i<points; i++)
  {
    if (data[i]>=par->ampl*0.1 || (data[i]>0.5*threshold && fabs(drv[i])<=drvtrig ))
    {
      par->tb10=i;
      break;
    }
  }
  
  for (int i=(int) par->t10; i>0; i--)
  {
//     cout<<data[i]<<"  "<<fabs(drv[i])<<"   "<<threshold<<endl;
    par->stime=i;
//     if (i<tpoint)
//     {
//       par->charge+=data[i];
//     }
    if (data[i]>threshold/5. || (fabs(drv[i])<=drvtrig && data[i]>threshold*0.8 ) )
    {
      par->stime=i;
      break;
    }
  }
  for (int i=(int)par->ftime; i<points; i++)
  {
    par->ftime=i;
    if (data[i]>threshold/5. || (fabs(drv[i])<=drvtrig && data[i]>threshold*0.8 ) )
    {
      break;
    }
  }
//   cout<<"ftime = "<<par->ftime<<endl;
/// extend to CIVIDEC_PULSE_DURATION ns in order to get the ion tail
  for (int i=par->ftime; i<points && i<par->stime + CIVIDEC_PULSE_DURATION/dt; i++)
  {
    par->ftime=i;
  }
//   cout<<"extended to = "<<par->ftime<<endl;
  
  par->charge=0.;
  for (int i=par->stime;i<=par->ftime;i++)
    par->charge+=data[i];

  par->risecharge=0.;
  for (int i=par->stime;i<points && i<par->stime + CIVIDEC_PEAK_DURATION/dt ;i++)
    par->risecharge+=data[i];

  par->sampl = data[par->stime];
  par->fampl = data[par->ftime];
  par->bslch = -0.5 * (data[par->stime] + data[par->ftime])*(par->ftime - par->stime +1.);
  par->width = par->ftime-par->stime;
  par->ampl*=-1.;
  par->charge*=-1.*dt;   ///charge is calculated in V * ns. 
//   cout<<"tstart = "<<par->stime*dt<<endl;
//   cout<<"t10 = "<<par->t10*dt<<endl;
//   cout<<"t90 = "<<par->t90*dt<<endl;
//   cout<<"tmax = "<<par->maxtime*dt<<endl;
//   cout<<"tb10 = "<<par->tb10*dt<<endl;
//   cout<<"tend = "<<par->ftime*dt<<endl;
//   
//   cout<<"rt = "<<(par->t90-par->t10)*dt<<endl;
//   cout<<"tot = "<<(par->tot)*dt<<endl;
//   cout<<"DT = "<<(par->tb10-par->t10)*dt<<endl;
//   cout<<"DTall = "<<(par->ftime-par->stime)*dt<<endl;
//   
//   cout<<"ampl = "<<par->ampl<<endl;
//   cout<<"charge = "<<par->charge*dt/N_INTEGRATION_POINTS<<endl;
//   cout<<"risecharge = "<<par->risecharge*dt/N_INTEGRATION_POINTS<<endl;
  return (par->ftime);
}