#ifndef MYFUNCTIONS_H
#define MYFUNCTIONS_H 1

#include <TROOT.h>
#include <stdlib.h>
#include <stdio.h>

#include <iostream>

#include <fstream>
#include <cmath>

#include <TFile.h>
#include <TH1F.h>
#include <TPostScript.h>
#include <TPDF.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TStyle.h>
#include <TCutG.h>
#include <TCanvas.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TF1.h>
#include <TMath.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TTree.h>
#include <TSpline.h>
#include <TPaveStats.h>
#include <TLatex.h>

#include <RQ_OBJECT.h>
// #include <TGTransientFrame.h>

#include <thread>             // std::thread, std::this_thread::yield
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable

// // 	
#define DEBUG 1
#undef DEBUG
#define DEBUGMSG 1
#undef DEBUGMSG
#define SLOWFILES 1
// #undef SLOWFILES

#define PeakparamDef(name,id)
#define FlowControlMainFrameDef(name,id)
#define FlowControlFrameDef(name,id)
#define AnalyseLongMMPulseSigmoidClassDef(name,id);

std::mutex myMutex;
std::condition_variable myConditionVariable;
bool globalPause;


class PEAKPARAM : public TObject {
public:
   int maxtime;
   int stime;
   int ftime;
   double ampl;
   double sampl;
   double fampl;
   double t20;
   double st20;
   double tfit20;
   double sechargefixed;
   double secharge;
   double echargefixed;
   double echarge;
   double echargefit;
   double totchargefixed;
   double risetime;  ///10% - 90%
   double risecharge;
   double width;
   double tot[10]; ///
   double sigmoidR[5];
   double sigmoidF[5];
   double sigmoidTot[6];

   double charge;
   double scharge;
   double t10;
   double tb10;
   double t90;

   double ttrig;
   double bslch;
   double rms;
   double bsl;
 
  PEAKPARAM() {}
 
  PeakparamDef(PEAKPARAM,1)
};
 
typedef struct {
//    float corr;
   int npeaks;
   int maxtime;
   int stime;
   int ftime;
   double *ampl;
   double *sampl;
   double *fampl;
   double *t20;
   double *st20;
   double *tfit20;
   double *sechargefixed;
   double *secharge;
   double *echargefixed;
   double *echarge;
   double *echargefit;
   double *totchargefixed;
   double *risetime;  ///10% - 90%
   double *risecharge;
   double *width;
   double *tot[10]; ///
   double *sigmoidR[3];
   double *sigmoidF[3];

   double *charge;
   double *scharge;
   double *t10;
   double *tb10;
   double *t90;

   double *ttrig;
   double *bslch;
   } PICPARAM;


typedef struct {
//    float corr;
   double bsl;
   double rms;
   double ftime;
   double stime;
   double maxtime;
   double maxtimeS;
   double time50;
   double ampl;
   double sampl;
   double intg;
   double charge;
   double chargeIon;
   double chi2;
   double t1;
   double t2;
   double t3;
   double a;  /// fit function f = a*x + b
   double b;
   double ttrig;
   } DPARAM;

typedef struct {
//    float corr;
   double bsl;
   double rms;
   int stime;
   int ftime;
   int maxtime;
   double tot;
   double ampl;
   double charge;
   double risecharge;
   double t10;
   double tb10;
   double t90;
   double tmax;
   double ttrig;
   double width;
   double sampl;
   double fampl;
   double bslch;
   } IPARAM;

typedef struct {
//    float corr;
   int npeaks;
   double *tot;
   double *ampl;
   double *charge;
   double *risecharge;
   double *t10;
   double *tb10;
   double *t90;
   double *ttrig;
   double *width;
   double *sampl;
   double *fampl;
   double *bslch;
   } PPARAM;

typedef struct {
//    float corr;
   int detNo;
   int preamNo;
   int runNo;
   double ampl;
   double sampl;
   double charge;
   double scharge;
   double rate;
   double srate;
   double grate;
   double sgrate;
   double tot;
   double stot;
   double risetime;
   double srisetime;
   double width;
   double swidth;
   double chovampl;
   double schovampl;
   } RUNPAR;

typedef struct {
//    float corr;
   int runNo;
   int poolNo;
   int preamNo[4];
   double tsigma[4];
   double trms[4];
   double amplpolya[4][3];
   double echargepolya[4][3];
   double chargepolya[4][3];
   double risetime[4];
   double width[4];
   double chovampl[4];
   } RUNPAR4;

typedef struct {
//    float corr;
   int runNo;
   int poolNo;
   int srsCh;
   float V1[4];
   float V2[4]; 
   float Z[4]; 
   char DetName[4][10];
   char Photocathode[4][12];
   char Amplifier[4][20];
   int AmplifierNo[4];
} OSCSETUP;

typedef struct {
//    float corr;
   int srstriggerctr[20];   /// [i] = trackNumber
   int srstimestamp[20];    /// [i] = trackNumber
   int ntracks;
   float trackchi2[20];    /// [i] = trackNumber
   float hits[20][20][3];   /// [i] = trackNumber  [j] = position  [k] = x,y,z
   float distnextcluster[20][6];  /// [i] = trackNumber [j] = GEM xy
   float totchanextcluster[20][6];  /// [i] = trackNumber [j] = GEM xy
} TRACKDATA;

typedef struct {
   double ampl; 
   int pos; 
   float x;
} GLOBALMAXIMUM;



const double rootConv = 2871763200.;
const double unixConv = 2082844800.;

const int MAX_N_FILES=11000;

const char *CODEDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/code";
const char *BASEDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4";
const char *WORKDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4/wdir";
const char *PLOTDIR="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4/plots";
const char *DATADIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4/dataTrees";
const char *TRACKDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis";
const char *OUTDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4/processedTrees";
const char *PARAMDIRNAME="/sw/akallits/PicoAnalysis/Saclay_Analysis/Saclay_drf/data/2022_October_h4/processedTrees/ParameterTrees";
const char *RTYPE="TESTBEAM";
const int MINRUN = 1;
const int MAXRUN = 1000;
const int TOTRUNS = MAXRUN-MINRUN+1;

const double Threshold = 0.0073;

//const int N_INTEGRATION_POINTS = 10; default
const int N_INTEGRATION_POINTS = 200;

const double CIVIDEC_PULSE_DURATION = 75.; // [ns]
const double CIVIDEC_PEAK_DURATION = 6.; // [ns]
const double MM_BSL_DURATION = 1.; // [ns]
const double MCP_BSL_DURATION = 0.5; // [ns]

/// definitions for the bin2tree.cxx ___________
#ifndef PATH_NAMES_DATA_CODE
#define PATH_NAMES_DATA_CODE 1
    const char *DATA_PATH_NAME="/drf/projets/picosecond/data/testbeam/2022_October_h4";
//     const char *OUT_DIR_NAME="/drf/projets/nblm/analysisSaraf/wdir/Fast/dataTrees";
    const char *OUT_DIR_NAME=BASEDIRNAME;  /// Must be the same with above
    const char *WORK_DIR_NAME=WORKDIR;   /// Must be the same with above
    const char *RUN_TYPE="TESTBEAMraw";

    const int RUNMIN=MINRUN;
    const int RUNMAX=MAXRUN;

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define KRESET "\x1B[0m"

#endif
///_______________________________________________



const std::string RED("\033[0;31m"); 
const std::string RED_U_GBKG("\033[4;31;42m"); 
const std::string GREEN("\033[0;32m"); 
const std::string YELLOW("\033[0;33m"); 
const std::string BLUE("\033[0;34m"); 
const std::string MAGENTA("\033[0;35m"); 
const std::string CYAN("\033[0;36m"); 
const std::string INVERSE_ON("\033[7m"); 
const std::string INVERSE_OFF("\033[27m"); 
const std::string RESET_COLOR("\033[0m");	
const std::string endlr("\n\033[0m");	


int FilterHisto(TH1* , double );

int SubtractBaseline(int , double* , double* , double );

int GetOsciloscopeSetup(int , int , const char* , OSCSETUP*);

double IntegrateA(int , double* , double* ,double );

//void FindGlobalMaximum(double, int, float);
double FindBaselineLevel(double*, int, int);
//double fermi_dirac_general(double*, double* );
//double fermi_dirac(double*, double *);

double slope_at_x(double*, int , int , double);

inline double fermi_dirac_general(double *x, double *par)
{
    double fdreturn = par[0]/TMath::Power((1+TMath::Exp((x[0]-par[1])*par[2])),par[3]);
    //double fdreturn = par[0]/TMath::Power((1+TMath::Exp(-(x[0]-par[1])*par[2])),par[3]); 
    //double fdreturn = par[0]/(1+par[3]*TMath::Exp(-(x[0]-par[1])*par[2]));
    return fdreturn;
}

inline double fermi_dirac(double *x, double *par)
{
    double fdreturn = par[0]/(par[4]+TMath::Exp(-par[1]*(x[0]-par[2])))+par[3];
    //double fdreturn = par[0]/(1+TMath::Exp((x[0]-par[1])*par[2]))+par[3];
    return fdreturn;
}

inline double fermi_dirac_sym(double *x, double *par)
{
    double fdreturn = par[0]/(1.+TMath::Exp(-par[1]*(x[0]-par[2])))+par[3];
    //double fdreturn = par[0]/(1+TMath::Exp((x[0]-par[1])*par[2]))+par[3];
    return fdreturn;
}

inline double fermi_dirac_sym_1(double *x, double *par)
{
    double fdreturn = (1./(1.+TMath::Exp(-par[1]*(x[0]-par[2]))))+0.*par[0];
    //double fdreturn = par[0]/(1+TMath::Exp((x[0]-par[1])*par[2]))+par[3];
    return fdreturn;
}

inline double fermi_dirac_sym_double(double *x, double *par)
{
    double fdreturn = (par[0]/(1.+TMath::Exp(-par[1]*(x[0]-par[2])))+par[3])*(1./(1.+TMath::Exp(-par[4]*(x[0]-par[5]))));
    //double fdreturn = par[0]/(1+TMath::Exp((x[0]-par[1])*par[2]))+par[3];
    return fdreturn;
}


class FlowControlMainFrame : public TGMainFrame {

private:
   TGCompositeFrame    *fButtonsFrame;
   TGTextButton        *fExit, *fPause;
   TGGroupFrame        *fGframe;
   TGNumberEntry       *fNumber;
   TGLabel             *fLabel;
   Bool_t            start, processing;
   std::thread wait_thread();

public:
   FlowControlMainFrame(const TGWindow *p, UInt_t w, UInt_t h, Int_t maxNo);
   virtual ~FlowControlMainFrame();
   void DoSetlabel();
   int GetEventNo();
   void SetEventNo(int );
   
   void ChangePauseLabel();
   bool ContinueProcessing();
   void WaitNextEvent();
   void Resume();
//    ClassDef(FlowControlMainFrame, 0)
   FlowControlMainFrameDef(FlowControlMainFrame,0)

};


class AnalyseLongMMPulseSigmoidClass /*: public TObject*/ {

private:
   int                  points;
   double               *data;
   double               *drv;
   double               *ptime;
   double               *yerr;
   PEAKPARAM            *par;
   double               threshold;
   double               dt;
   int                  tshift;
   int                  channel;
   int                  evID;
   int                  amplifier;
   char                 detector[100];
   TCanvas              *flowcanv;
   TCanvas              *canv;
   bool                 eventDisplay;
   
   TF1                  *sig_fit;
   TF1                  *sig_fit2;
   TF1                  *sig_fit1;
   TF1                  *sig_fittot;
   
   TGraphErrors         *sig_waveform;
  
  double sig_lim_min;
  double sig_lim_max;
  double sig_pars[5];

  double DT;
  double t_half_point;
  double steepness_left;
   

public:
   AnalyseLongMMPulseSigmoidClass(int draw);
   virtual ~AnalyseLongMMPulseSigmoidClass();
   
   int Analyse();  /// main analysis


/// data set methodes
   void SetNpoints(int i){points=i;};
   void SetData(double *arr){data=arr;};
   void SetTimeArray(double *arr){ptime=arr;};
   void SetDerivative(double *arr){drv=arr;};
   void SetYerr(double *arr){yerr=arr;};
   void SetThreshold(double thr){threshold=thr;};
   void SetDT(double t){t=dt;};
   void SetSearchStartPoint(int i){tshift=i;};

/// set functions for event display
   void SetDraw(int on){eventDisplay= on?kTRUE:kFALSE;};
   void SetCanvas(TCanvas *tc){canv=tc;};
   void SetFlowCanvas(TCanvas *tc){flowcanv=tc;};;
   
   void SetEventNo(int ev){evID=ev;};
   void SetDetector(const char * detect){strncpy(detector,detect,100);};
   void SetAmplifier(int ampl){amplifier = ampl;};
   void SetChannel(int ch){channel=ch;};

   PEAKPARAM* GetPeakParameters(){return par;};
   int GetSearchEndPoint(){return tshift;};
   
   
//    ClassDef(FlowControlMainFrame, 0)
   AnalyseLongMMPulseSigmoidClassDef(AnalyseLongMMPulseSigmoidClass,0)

};







#endif
